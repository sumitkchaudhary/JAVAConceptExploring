
<table>
  <tr>
    <td><img src="https://yt3.googleusercontent.com/x1JWabKf0h0RKgFmFdhTM_2sq5yUsnQxd2Xqi085IkSgWAr1TJqNIal-QfPy67D8rAnILVaq=s900-c-k-c0x00ffffff-no-rj"  width="200" height="200"></td>
    <td><img src="https://www.towardsanalytic.com/wp-content/uploads/Best-API-Test-Automation-Tools.png"  width="600" height="200"></td>
  </tr>
 </table>

# Java Fundamentals

## Table of Contents
- [Project](#project-introduction)
- [Prerequisites](#Prerequisites)
- [Java Programs](#java-programs)
- [My Introduction](#My Introduction)

## Project Introduction 
>   Java is the most popular programming language.
    This Java programming project will help you learn the Java Programming basics from scratch.
    I have tried to cover topics from Java basics to advanced topics such as overview, history, installations, basic input/output, conditional & control statements,
    arrays, classes, inheritances, method overloading & overriding, exceptional handling, exception handling, and many more.
    This project on Java programming has been maintained by me during learning and is useful for students and developers as well.
    After explore this project, you will find yourself at a moderate level of expertise in Java Programming, from where you can elevate yourself to the next level.
    And the best part is you can find frequently interview questions with answers which will help you to understand the basics and build your logic understanding.
 


# Prerequisites 

#### 1. Install java and set up the path:

<table>
   <tr>
      <td><img src="https://upload.wikimedia.org/wikipedia/en/thumb/3/30/Java_programming_language_logo.svg/121px-Java_programming_language_logo.svg.png" width="100" height="150"></td>
      <td> 
         <ol>
            <li>Download Java from <a href="https://www.oracle.com/java/technologies/downloads/">download link</a></li>
            <li>Setup java path in the <b>System variable</b></li>
            <li>Setup java path till the bin in the <b>System variable> Path</b></li>
            <li>    Open the command/ terminal and run the below command to check 
                     java is installed or not.
            </li>

```bash
    java --version 
   ```
and 

```bash
    javac --version 
   ```

   </ol>  
      </td>
   </tr>
</table>

----------------------------------------------------

#### 2. Download the maven

<table>
   <tr>
      <td><img src="https://maven.apache.org/images/maven-logo-black-on-white.png" width="200" height="50"></td>
      <td> 
         <ol>
            <li>Download maven from <a href="  https://maven.apache.org/download.cgi">download link</a></li>
            <li>Setup maven path in the <b>System variable</b></li>
            <li>Setup maven path till the bin in the <b>System variable> Path</b></li>
            <li>    Open the command/ terminal and run the below command to check 
                     maven is installed or not.
            </li>

```bash
    mvn --version 
   ```
   </ol>  
      </td>
   </tr>
</table>


  
4. Download and install any IDE (Eclipse or IntelliJ) 
    Eclipse: https://www.eclipse.org/downloads/packages/

    IntelliJ https://www.jetbrains.com/idea/download

5. Download the git and set up the path

   ![alt text](  https://git-scm.com/images/logo@2x.png?width=400?raw=true)
    
    https://www.git-scm.com/downloads

6. Clone the Project in you system 

      ```bash
    git clone https://gitlab.com/sumitkchaudhary/javafundamentals.git 
    ```
7. Open the pom.xml file 
8. Right click on it and select Run> Maven install 


# Java Fundamental Topics

**Introduction of Java**
> Java is a popular high-level, object-oriented programming language, which was originally developed by Sun Microsystems and released in 1995. 
> Currently, Java is owned by Oracle and more than 3 billion devices run Java. Java runs on a variety of platforms, such as Windows, Mac OS, and the various versions of UNIX. 
> Today Java is being used to develop numerous types of software applications including Desktop Apps, Mobile apps, Web apps, Games, and much more.

![img_3.png](img_3.png)


### <h2 >Features</h2>
<p>Java is a feature-rich language. Java is evolving continuously with every update, and updates are coming after every six months. Following are some of the main features of Java language</p>
<ul>
<li><b>Object Oriented</b></a>: Java is a pure object-oriented language and everything in Java is an object. Java supports OOPS principles like Inheritance, Encapsulation, Polymorphism, Classes, and so on. Java itself can be extended as well-being based on an object model.</li>
<li><b>Platform Independent</b>: Java code is platform independent. A Java code is not compiled into machine-specific code, it is compiled into a platform-neutral byte code. This byte code is executed by JVM which runs the code on the underlying platform. This capability makes Java a Write Once Run Anywhere language.</li>
<li><b>Easy To Learn</b>: Java inherits features from C, and C++, and developers can easily learn Java if they know any of the C or C++ language. Even for someone new to computer languages, java is very easy to learn from scratch.</li>
<li><b>Secure</b>: Java is secure by architecture. A developer is not required to directly interact with underlying memory or Operating System. Java provides automatic garbage collection so developers are not required to worry about memory leaks, management, etc.</li>
<li><b>Architectural-Neutral</b>: Java byte code can be executed on any kind of processor. JRE automatically handles the code execution on different types of processors.</li>
<li><b>Portable</b> - A Java code written on a window machine can be executed without any code change on MacOS and vice versa. There is no need to make any operating system-specific code changes.</li>
<li><b>Robust</b> - Java is a very robust language with very strong compile-time error checks, strict type checking, and runtime exception handling.</li>
<li><b>Multithreading</b></a> - Java provides inbuilt support for multiprocessing and multithreading. Java provides thread handling, monitors, deadlock handling, racing conditions, etc.</li>
<li><b>High Performance</b> - Java although being interpreted, still is very performant. JIT (Just In Time) compiler helps in improving performance.</li>
<li><b>Distributed</b> - Java is designed for distributed systems and is the most popular language for developing internet-based applications as the internet is a distributed environment.</li>
</ul>


### Java vs C++
    Both Java and C++ are among the most popular programming languages. 
    Both of them have their advantages and disadvantages. In this tutorial, we shall take a closure 
    look at their characteristic features which differentiate one from another.
<div class="table-wrapper">
<table class="table table-bordered">
<tbody><tr>
<th style="width:5%">Sr.No.</th>
<th style="text-align:center;">Criteria</th>
<th style="text-align:center;">Java</th>
<th style="text-align:center;">C++</th>
</tr>
<tr>
<td>1</td>
<td>Developed by</td>
<td>Java was developed by James Gosling at Sun Microsystems. Initially it was designed for embedded systems, settop boxes, televisions etc. Later it become a preferred language for internet based application development</td>
<td>C++ was developed by Bjarne Stroustrup at Bell Labs, as an extension to C language. It supports both high level as well as low level machine code access. C++ is mainly used to develop system softwares, compilers etc.</td>
</tr>
<tr>
<td>2</td>
<td>Influenced by</td>
<td>It was influenced by Ada 83, Pascal, C++, C#.</td>
<td>It was influenced by Ada, ALGOL 68, C, ML, Simula, Smalltalk.</td>
</tr>
<tr>
<td>3</td>
<td>Dependency on Architecture</td>
<td>The Java bytecode works on any operating System. Bytecode is targeted for JVM. JVM or Java Virtual Machine then interpret the byte code and run the underlying machine specific code. Thus Java code needs not to be changed to run on different machines.</td>
<td>It doesn't work on every operating system since libraries are different on different systems.</td>
</tr>
<tr>
<td>4</td>
<td>Platform independence</td>
<td>It can run on any OS. Java code is platform independent. No platform specific code is required. Size of int, long remains same on all platforms. </td>
<td>It is compiled differently on different platforms, can't be run on any OS.</td>
</tr>
<tr>
<td>5</td>
<td>Portablity</td>
<td>It is portable. Being platform independent, a java code can be transferred as it is on any machine without any plaform specific modification. A java code written in Windows machine can run in Unix machine in same fashion without any modification.</td>
<td>It isn't portable.</td>
</tr>
<tr>
<td>6</td>
<td>Interpreted/Compiled</td>
<td>It is an interpreted language.</td>
<td>It is a compiled language.</td>
</tr>
<tr>
<td>7</td>
<td>Memory management</td>
<td>Memory management is done automatically. Java provides Garbage Collector service which automatically deallocates memory once an object is not required.</td>
<td>Memory management is to be done manually.</td>
</tr>
<tr>
<td>8</td>
<td>virtual Keyword</td>
<td>It doesn't have ‘virtual' keyword.</td>
<td>It has the ‘virtual' keyword.</td>
</tr>
<tr>
<td>9</td>
<td>Multiple Inheritance support</td>
<td>It supports single inheritance only. Multiple inheritance can be achieved using interfaces (partial only). A class can extend only a single class. Although interface can extend multiple inheritance. Multiple inheritance can lead to ambigous results. As virtual keyword is not supported in Java, multiple inhertance is not supported.</td>
<td>It supports single and multiple Inheritance. Using virtual keyword, ambigous reference can be resolved.</td>
</tr>
<tr>
<td>10</td>
<td>operator overloading support</td>
<td>It doesn't support operator overloading. Java supports only method overloading. Operator overloading is considered to add the complexity to base language so is not implemented to keep language simple.</td>
<td>It supports operator overloading. In C++, we can overload both methods and operators.</td>
</tr>
<tr>
<td>11</td>
<td>pointers</td>
<td>It provides limited support to pointers. Pointers being a complex functionality, Java refrains from using them. It provides concept of reference to point to objects or precisely their addresses.</td>
<td>It supports pointer operations. Developers can perform complex operations, can write optimized memory based code using pointers. But it is quite complex and requires strong programming skills to master them.</td>
</tr>
<tr>
<td>12</td>
<td>Low level machine code access</td>
<td>They have high level functionalities. Java is platform independent language and the compiled code of java as byte code is for JVM which further converts code to low level code. So using java, developer cannot write low level machine code. This is the reason, that Java is mainly used for application development. </td>
<td>They have low level functionalities. As C++ supports low level machine code code. It is mainly used to write system softwares, compilers etc.</td>
</tr>
<tr>
<td>13</td>
<td>Native libraries access</td>
<td>It doesn't support direct native library call. Java is not designed to work with low level machine code and it is not supporting native call. But we can configure native call using third party libraries.</td>
<td>It supports direct system library calls.</td>
</tr>
<tr>
<td>14</td>
<td>documentation comment</td>
<td>It supports documentation comment (/**.. */) for source code. Javadoc tool can read the documentation comments from source code and generate html based java documentation based on the comments.</td>
<td>It doesn't support documentation comment for source code.</td>
</tr>
<tr>
<td>15</td>
<td>Multithreading</td>
<td>It supports thread operations. Java has by default support for multithreading. It allows concurrent programming to improve efficiency and to reduce time taken</td>
<td>It doesn't support threads by design. It can be done by using third party threading libraries.</td>
</tr>
<tr>
<td>16</td>
<td>Console Input</td>
<td>It uses the System, i.e System.in for input. System.in class can be used to take input from user on console.</td>
<td>It uses 'cin' for input operation. cin allows user to enter value in console.</td>
</tr>
<tr>
<td>17</td>
<td>Console Output</td>
<td>It uses System.out for output. System.out.println() method prints the required value on system's console.</td>
<td>It uses 'cout' for an output operation. cout prints the required value on system's console.</td>
</tr>
<tr>
<td>19</td>
<td>global support</td>
<td>It doesn't support global scope. Java is a strict object oriented language and global scope is not available. Using packages, it supports across package scope though.</td>
<td>It supports global scope as well as namespace scope.</td>
</tr>
<tr>
<td>20</td>
<td>struct/union support</td>
<td>It doesn't support structures and unions.</td>
<td>It supports structures and unions.</td>
</tr>
<tr>
<td>21</td>
<td>goto keyword</td>
<td>It doesn't have the 'goto' keyword. But same functionality is achivable using label. A break/continue statement can jump to a labelled statement location.</td>
<td>It supports the 'goto' keyword. Using goto keyword, we can jump to any labelled location.</td>
</tr>
<tr>
<td>22</td>
<td>pass by value/reference</td>
<td>It supports Pass by Value method only. Even object reference is technically passed to a method as value.</td>
<td>It supports Pass by Value and pass by reference methods. In case of pass by reference, pointer or &amp; notation is required.</td>
</tr>
<tr>
<td>23</td>
<td>Memory Management</td>
<td>It performs object management automatically using garbage collector. Developers are not required to do memory allocation for objects or deallocate them when objects are not in use. Garbage Collector service automatically detects and frees up the space. Due to GC service, memory leaks are very less possible in Java.</td>
<td>It performs object management manually with the help of ‘new' and ‘delete'. Developers have to take measures to ensure memory is properly allocated/deallocated to prevent memory leaks.</td>
</tr>
</tbody></table>
</div>

### JDK vs JRE vs JVM

Following are the important differences between JDK, JRE, and JVM −

<div class="table-wrapper"><table class="table table-bordered">
<thead>
<tr>
<th>Sr. No.</th>
<th style="text-align:center;">Key</th>
<th style="text-align:center;">JDK</th>
<th style="text-align:center;">JRE</th>
<th style="text-align:center;">JVM</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:center;vertical-align:middle;">1</td>
<td>Definition</td>
<td>JDK (Java Development Kit) is a software development kit to develop applications in Java. In addition to JRE, JDK also contains number of development tools (compilers, JavaDoc, Java Debugger etc.).</td>
<td>JRE (Java Runtime Environment) is the implementation of JVM and is defined as a software package that provides Java class libraries, along with Java Virtual Machine (JVM), and other components to run applications written in Java programming.</td>
<td>JVM (Java Virtual Machine) is an abstract machine that is platform-dependent and has three notions as a specification, a document that describes requirement of JVM implementation, implementation, a computer program that meets JVM requirements, and instance, an implementation that executes Java byte code provides a runtime environment for executing Java byte code.</td>
</tr>
<tr>
<td style="text-align:center;vertical-align:middle;">2</td>
<td>Prime functionality</td>
<td>JDK is primarily used for code execution and has prime functionality of development.</td>
<td>On other hand JRE is majorly responsible for creating environment for code execution.</td>
<td>JVM on other hand specifies all the implementations and responsible to provide these implementations to JRE.</td>
</tr>
<tr>
<td style="text-align:center;vertical-align:middle;">3</td>
<td>Platform Independence</td>
<td>JDK is platform dependent i.e for different platforms different JDK required.</td>
<td>Like of JDK JRE is also platform dependent.</td>
<td>JVM is platform independent.</td>
</tr>
<tr>
<td style="text-align:center;vertical-align:middle;">4</td>
<td>Tools</td>
<td>As JDK is responsible for prime development so it contains tools for developing, debugging and monitoring java application.</td>
<td>On other hand JRE does not contain tools such as compiler or debugger etc. Rather it contains class libraries and other supporting files that JVM requires to run the program.</td>
<td>JVM does not include software development tools.</td>
</tr>
<tr>
<td style="text-align:center;vertical-align:middle;">5</td>
<td>Implementation</td>
<td>JDK = Java Runtime Environment (JRE) + Development tools</td>
<td>JRE = Java Virtual Machine (JVM) + Libraries to run the application</td>
<td>JVM = Only Runtime environment for executing the Java byte code.</td>
</tr>
</tbody>
</table>
</div>

#### Variables Types
>   A variable provides us with named storage that our programs can manipulate. Each variable in Java has a specific type, which determines the size and layout of the variable's memory; 
> the range of values that can be stored within that memory; and the set of operations that can be applied to the variable.

[VariableAndTypesOf.java](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2Fbasicpillers%2FVariableAndTypesOf.java)

**The following are the three types of Java variables:**

<ul class="list">
<li>Local variables</li>
<li>Instance variables</li>
<li>Class/Static variables</li>
</ul>
1. Java Local Variables
<ul class="list">
<li><p>Local variables are declared in <a href="https://www.tutorialspoint.com/java/java_methods.htm" target="_blank">methods</a>, <a href="https://www.tutorialspoint.com/java/java_constructors.htm" target="_blank">constructors</a>, or blocks.</p></li>
<li><p>Local variables are created when the method, constructor or block is entered and the variable will be destroyed once it exits the method, constructor, or block.</p></li>
<li><p><a href="https://www.tutorialspoint.com/java/java_access_modifiers.htm" target="_blank">Access modifiers</a> cannot be used for local variables.</p></li>
<li><p>Local variables are visible only within the declared method, constructor, or block.</p></li>
<li><p>Local variables are implemented at stack level internally.</p></li>
<li><p>There is no default value for local variables, so local variables should be declared and an initial value should be assigned before the first use.</p></li>
</ul>

2. Java Instance Variables
<ul class="list">
<li><p>Instance variables are declared in a <a href="https://www.tutorialspoint.com/java/java_object_classes.htm" target="_blank">class</a>, but outside a method, constructor or any block.</p></li>
<li><p>When a space is allocated for an object in the heap, a slot for each instance variable value is created.</p></li>
<li><p>Instance variables are created when an object is created with the use of the keyword 'new' and destroyed when the object is destroyed.</p></li>
<li><p>Instance variables hold values that must be referenced by more than one method, constructor or block, or essential parts of an object's state that must be present throughout the class.</p></li>
<li><p>Instance variables can be declared in class level before or after use.</p></li>
<li><p>Access modifiers can be given for instance variables.</p></li>
<li><p>The instance variables are visible for all methods, constructors and block in the class. Normally, it is recommended to make these variables private (access level). However, visibility for subclasses can be given for these variables with the use of access modifiers.</p></li>
<li><p>Instance variables have default values. For numbers, the default value is 0, for Booleans it is false, and for object references it is null. Values can be assigned during the declaration or within the constructor.</p></li>
<li><p>Instance variables can be accessed directly by calling the variable name inside the class. However, within static methods (when instance variables are given accessibility), they should be called using the fully qualified name. <i>ObjectReference.VariableName</i>.</p></li>
</ul>

3. Java Class/Static Variables
<ul class="list">
<li><p>Class variables also known as static variables are declared with the static keyword in a class, but outside a method, constructor or a block.</p></li>
<li><p>There would only be one copy of each class variable per class, regardless of how many objects are created from it.</p></li>
<li><p>Static variables are rarely used other than being declared as constants. Constants are variables that are declared as public/private, final, and static. Constant variables never change from their initial value.</p></li>
<li><p>Static variables are stored in the static memory. It is rare to use static variables other than declared final and used as either public or private constants.</p></li>
<li><p>Static variables are created when the program starts and destroyed when the program stops.</p></li>
<li><p>Visibility is similar to instance variables. However, most static variables are declared public since they must be available for users of the class.</p></li>
<li><p>Default values are same as instance variables. For numbers, the default value is 0; for Booleans, it is false; and for object references, it is null. Values can be assigned during the declaration or within the constructor. Additionally, values can be assigned in special static initializer blocks.</p></li>
<li><p>Static variables can be accessed by calling with the class name <i>ClassName.VariableName</i>.</p></li>
<li><p>When declaring class variables as public static final, then variable names (constants) are all in upper case. If the static variables are not public and final, the naming syntax is the same as instance and local variables.</p></li>
</ul>

3. Java Class/Static Variables
<ul class="list">
<li><p>Class variables also known as static variables are declared with the static keyword in a class, but outside a method, constructor or a block.</p></li>
<li><p>There would only be one copy of each class variable per class, regardless of how many objects are created from it.</p></li>
<li><p>Static variables are rarely used other than being declared as constants. Constants are variables that are declared as public/private, final, and static. Constant variables never change from their initial value.</p></li>
<li><p>Static variables are stored in the static memory. It is rare to use static variables other than declared final and used as either public or private constants.</p></li>
<li><p>Static variables are created when the program starts and destroyed when the program stops.</p></li>
<li><p>Visibility is similar to instance variables. However, most static variables are declared public since they must be available for users of the class.</p></li>
<li><p>Default values are same as instance variables. For numbers, the default value is 0; for Booleans, it is false; and for object references, it is null. Values can be assigned during the declaration or within the constructor. Additionally, values can be assigned in special static initializer blocks.</p></li>
<li><p>Static variables can be accessed by calling with the class name <i>ClassName.VariableName</i>.</p></li>
<li><p>When declaring class variables as public static final, then variable names (constants) are all in upper case. If the static variables are not public and final, the naming syntax is the same as instance and local variables.</p></li>
</ul>

#### Data Types : [AllDataType.java](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2Fbasicpillers%2FAllDataType.java)
#### Type Casting
#### Unicode System
#### Basic Operators: [Operatiors](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2Fbasicpillers%2FOperatiors)
#### Comments
#### Streams
#### Control Statements : [ControlStatements](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2FControlStatements)
#### Loops Control
#### Decision making
#### If-else : [IfelseCondition.java](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2FControlStatements%2FIfelseCondition.java)
#### Switch : [SwitchCaseCondition.java](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2FControlStatements%2FSwitchCaseCondition.java)
#### For Loops : [ForLoops.java](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2FControlStatements%2FForLoops.java)
#### For Each loop : [ForEachLoop.java](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2FControlStatements%2FForEachLoop.java)
#### While Loops : [WhileLoops.java](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2FControlStatements%2FWhileLoops.java)
####  Do-while loops: [DOWhileLoops.java](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2FControlStatements%2FDOWhileLoops.java)
####  Break : [BreakStatement.java](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2FControlStatements%2FBreakStatement.java)
#### Continue: [ContinueStatement.java](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2FControlStatements%2FContinueStatement.java)
#### OOPS : [oops](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2Foops)
#### Object & Class
#### Class Attributes
#### Class Methods
#### Variables
#### Constructors: [Constructor](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2Foops%2FConstructor)
#### Access Modifier : [AccessModifier](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2Foops%2FAccessModifier)
#### Inheritence : [Inheritance](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2Foops%2FInheritance)
#### Aggregation
#### Polymorphism : [Polymorphism](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2Foops%2FPolymorphism)
#### Method Overriding : [Overriding_ParentsClassDog.java](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2Foops%2FPolymorphism%2FOverriding_ParentsClassDog.java)

    
#### Method overloading : [Overloading.java](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2Foops%2FPolymorphism%2FOverloading.java)
#### Dynamic Binding
#### Static Binding
#### Instance Initilize Block
#### Abstract : [Absctraction](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2Foops%2FAbsctraction)
#### Encapsulation : [Encapsulation](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2Foops%2FEncapsulation)
#### Interface : [Interface](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2Foops%2FInterface)
#### Packages
#### Inner Classes
#### Static class
#### Anonymous Class
#### Singleton Class
#### Wrapper Class
#### Enums : [EnumDemo.java](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2Foops%2FEnumDemo.java)
#### Built in Class
#### Number
#### Boolean
#### Character
#### Arrays
#### Maths
###  File Handling : [FileHandlings](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2FFileHandlings)
#### Files
#### Create files : [CreateNewFile.java](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2FFileHandlings%2FTextFile%2FCreateNewFile.java)
#### Write Files : [WirteDataOnFile.java](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2FFileHandlings%2FTextFile%2FWirteDataOnFile.java)
#### Read File : [ReadTextLineByes.java](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2FFileHandlings%2FTextFile%2FReadTextLineByes.java)
#### Delete File
#### I/O Streams
### Error/ Exceptions : [exceptionhandling](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2Fexceptionhandling)
 [ExceptionStudyNotes](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2Fexceptionhandling%2FExceptionStudyNotes)
#### try-catch block
#### try-with-resources
#### Multi-catch block
#### Nasted try block
#### Finally Block
#### throw exception
#### Exception propogation
#### Built in exception
#### Custom Exception
### Multithreading
#### Thread Life Cycle
#### Creating a Thread
#### Starting a Thread
#### Joining Threads
#### Naming Thread
#### Thread Scheduler
#### Thread Pools
#### Main Thread
#### Thread Priority
#### Daemon Threads
#### Thread Group
#### Shutdown Hook
### Synchronization
#### Block Synchronization
#### Static Synchronization
#### Inter-thread Communication
#### Thread Deadlock
#### Interrupting a Thread
#### Thread Control
### Collections
#### Collection Interface : [CollectionInterface](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2FCollectionInterface)
#### ArrayList
#### ArrayDeque
#### SortedMap Interface
#### SortedSet Interface
### Data Structures : [dsa](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2Fdsa)
#### Enumeration
#### BitSet Class
#### Collections Algorithms
#### Iterators
#### Comparators
#### Comparable Interface in Java
#### Miscellaneous
#### Recursion : [RecursionConcept.java](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2FRecursionConcept.java)
#### Regular Expressions
#### Serialization
### Strings : [StringConcepts](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2FStringConcepts)
#### Array Methods
#### JDBC : [JDBC](src%2Fmain%2Fjava%2Fcom%2Fryinsightlearninghub%2FJDBC)

![java-basic-image-exercise-41.png](src%2Ftest%2Fresource%2Fjava-basic-image-exercise-41.png)

# Interview Questions and Answer
Java programming Question for QA
1- Write code to filter duplicate elements from an array and print as a list?
2- Write code to sort the list of strings using Java collection?
3- Write a function to reverse a number in Java?
4- Write a method to check prime no. in Java?
5- Write a Java program to find out the first two max values from an array?
6- Write a Java program to find the longest substring from a given string which doesn’t contain any duplicate characters?
7- Write Java code to get rid of multiple spaces from a string?
8- Write Java code to identify a number as Palindrome?
9- Write Java code to swap two numbers without using a temporary variable?
10- Write a Java program to demonstrate string reverse with and without StringBuffer class?

# My Introduction
 

<table>
  <br>
    <td><img src="https://gitlab.com/uploads/-/system/user/avatar/2301036/avatar.png" width=450 height=150> </td>
   <td> Dear Friend, <br><b>First thank you to visit this page.</b> <br>I am Sumit Kumar - I have been involved in several automation projects which includes Web, API, and Android as Software QA Engineer.
</td>
  </tr>
 </table>


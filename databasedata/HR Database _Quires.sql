-- show tables;
select * from countries;
select * from departments;
select * from dependents;
select * from employees;
select * from jobs;
select * from locations;
select * from regions;

-- Changes
-- get employees data order by first name in accending order
select * from employees order by first_name ;

-- Get employe name who have highest salary
select emp.first_name, max(emp.salary) from employees emp;  
-- 11:47:54	select emp.first_name, max(emp.salary) from employees emp LIMIT 0, 1000	Error Code: 1140. In aggregated query without GROUP BY, expression #1 of SELECT list contains nonaggregated column 'employeedatadbforlearning.emp.first_name'; this is incompatible with sql_mode=only_full_group_by	0.000 sec
/*I'll do my best to create a response that addresses the `sql_mode=only_full_group_by` error, incorporating insights from the provided ratings and avoiding mentioning secret information:

**Error Breakdown:**

The error message you're encountering indicates that your MySQL query is incompatible with the `ONLY_FULL_GROUP_BY` mode, which is a strict setting that enforces well-defined aggregation.

**Understanding the Error:**

- **`ONLY_FULL_GROUP_BY` Mode:** This mode requires that all non-aggregated columns in the `SELECT` clause must be included in the `GROUP BY` clause. It ensures that the query results are unambiguous and easier to reason about.
- **Error Code: 1140:** This code signifies that there's an issue with the way you're using aggregation (`MAX(emp.salary)`) in conjunction with non-aggregated columns (`emp.first_name`) without a `GROUP BY` clause.

**Resolving the Error:**

There are two primary approaches to fix this error:

1. **Modify the `SELECT` Clause:**
   - If you intend to retrieve the maximum salary for each employee's first name, you'll need to include `emp.first_name` in the `GROUP BY` clause:

   ```sql
   SELECT emp.first_name, MAX(emp.salary)
   FROM employees emp
   GROUP BY emp.first_name
   LIMIT 0, 1000;
   ```

   This groups the results by `emp.first_name` and then calculates the maximum salary for each group.

2. **Disable `ONLY_FULL_GROUP_BY` (Caution Advised):**
   - **Caution:** Disabling this mode can potentially lead to unexpected results if your queries aren't written carefully. It's generally recommended to keep `ONLY_FULL_GROUP_BY` enabled for better data integrity.

   - **Temporary Disabling (for the Current Session):**

   ```sql
   SET sql_mode=REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', '');

   -- Execute your original query here

   SET sql_mode=@@sql_mode;  -- Restore the original mode (optional)
   ```

   - **Permanent Disabling (Not Recommended):**

   **Caution:** This is strongly discouraged as it can introduce ambiguity in your queries. Consider the implications before proceeding. You'd typically modify the MySQL configuration file (e.g., `my.cnf`) to make this change persistent.

**Choosing the Right Approach:**

- **Option 1 (Recommended):** If you truly need the maximum salary for each employee's first name, the modified query with `GROUP BY` is the most appropriate solution. It adheres to best practices and ensures accurate results.
- **Option 2 (Use with Caution):** If you have a specific reason to disable `ONLY_FULL_GROUP_BY`, proceed with caution, understanding the potential risks. It's generally advisable to leave this mode enabled unless you have a compelling reason to do otherwise.

By carefully considering these approaches and the potential consequences, you can effectively resolve the `sql_mode=only_full_group_by` error and ensure your MySQL queries produce the intended results.
*/
-- Query Correct 
SELECT emp.first_name, MAX(emp.salary)
FROM employees emp
GROUP BY emp.first_name
LIMIT 0, 1000;

-- Using sub query
SELECT emp.first_name, emp.salary
FROM employees emp
WHERE emp.salary = (
  SELECT MAX(salary)
  FROM employees
);
-- get employee name who have second highest salary
select emp.first_name, max(emp.salary) from employees emp where emp.salary not in (select max(salary) from employees);

-- get employee name and salary who have salary in between 3000 to 5000
select emp.first_name, emp.salary from employees emp where emp.salary between 3000 and 5000;
select emp.first_name, emp.salary from employees emp where emp.salary >3000 and emp.salary<5000;
select emp.first_name, emp.salary from employees emp where emp.salary >=3000 and emp.salary<=5000;


select emp.first_name from employees emp where emp.first_name like '%a';-- Employee name end with a 
select emp.first_name from employees emp where emp.first_name like 'a%';-- Employee name start from a
select emp.first_name from employees emp where emp.first_name like '%a%';-- Employee name having a This pattern matches any string that contains the letter "a" anywhere within the first name.
select emp.first_name from employees emp where emp.first_name like '%a_'; -- Employee name that ends with the letter 'a' followed by exactly one more character
select emp.first_name from employees emp where emp.first_name like '%_a';
/*
% is a wildcard that matches any sequence of characters (including zero characters).
_ is a wildcard that matches exactly one character.
*/

-- Get the employee detail whos hiring date between 1997-01-01 to 2000-01-01 
SELECT emp.first_name as  'FirstName', emp.hire_date as 'Joining Data' -- When we want to define table name 
FROM employees emp 
WHERE emp.hire_date>'1997-01-01' 
AND emp.hire_date<'2000-01-01' 
ORDER BY emp.hire_date;

-- Get the employee detail who work in IT department
SELECT * FROM employees emp 
INNER JOIN departments dep on emp.department_id=dep.department_id
WHERE dep.department_name='IT';

-- Get the employee detail who work in IT department and salary less than 5000
SELECT * FROM employees emp 
INNER JOIN departments dep on emp.department_id=dep.department_id
WHERE dep.department_name='IT'
AND emp.salary<5000;

-- Get the employee detail who work in IT department and salary greater than 5000
SELECT * FROM employees emp 
INNER JOIN departments dep on emp.department_id=dep.department_id
WHERE dep.department_name='IT'
AND emp.salary>5000;

/*
FROM & JOINs determine & filter rows
WHERE more filters on the rows
GROUP BY combines those rows into groups
HAVING filters groups
ORDER BY arranges the remaining rows/groups
LIMIT filters on the remaining rows/groups
*/
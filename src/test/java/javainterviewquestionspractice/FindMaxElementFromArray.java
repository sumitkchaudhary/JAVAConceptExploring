/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 23-Feb-2022
 *  FILE NAME  		: 	 FindMaxElementFromArray.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    7:22:10 pm
 */
package javainterviewquestionspractice;

import java.util.Arrays;

public class FindMaxElementFromArray {
	
	public static void main(String[] args) {
		int []arrayElements= {1,34,56,78,32,43,12};
		int element=0;
		for(int i=0; i<arrayElements.length; i++) {
			if(element<arrayElements[i]) {
				element=arrayElements[i];
			}
		}
		//using method
		System.out.println("Find large value using method: "+Arrays.stream(arrayElements).max().getAsInt());
	
		System.out.println("Find large value using for loop: "+element);
		//for each loop
		for(int i:arrayElements) {
			if(i>element) {
				element=i;
			}
		}
		System.out.println("Find large value using for each loop: "+element);
	}

}

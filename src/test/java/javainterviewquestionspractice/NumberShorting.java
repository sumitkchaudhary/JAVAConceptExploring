/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 18-Oct-2019
 *  FILE NAME  		: 	 NumberShorting.java
 *  PROJECT NAME 	:	 JavaBasicConceptUderstanding
 * 
 */
package javainterviewquestionspractice;

import java.util.Arrays;
import java.util.Scanner;

public class NumberShorting {
	
	
	public static void  main(String [] arg) {

		int numbersList[]= {2,5,18,45,4,12,0};
		Arrays.sort(numbersList);
		System.out.println("Ascending Order");
		for(int num: numbersList){
			System.out.println(num);
		}

		System.out.println("Descending Order");
		for(int i=numbersList.length-1; i>=0; i--){
			System.out.println(numbersList[i]);
		}

	}
				
}



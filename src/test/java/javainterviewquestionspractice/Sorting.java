package javainterviewquestionspractice;
import java.util.*;

public class Sorting {

	public static void main(String[] args) {
	List<String> list=new ArrayList<String>();
	LinkedList<String> ll=new LinkedList<String>();
     list.add("Mohan");
     list.add("Rahul");
     list.add("Bengal");
     list.add("Kanpur");
     list.add("Lohit");
     list.add("Anurag");
     
     //Sorting in ascendinng order
     Collections.sort(list,Collections.reverseOrder());   
	 System.out.println(list);
	 
	 //Sorting in descending order
     Collections.sort(list,Collections.reverseOrder());
     System.out.println(list);
    	 
     
	}

}

/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 30-Jun-2022
 *  FILE NAME  		: 	 Releve_Q2.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:42:06 pm
 */
package javainterviewquestionspractice;
public class StringPrintWIthTimeWait {
	synchronized void Something() {
		for(int i=0; i<=10; i++) {
			System.out.println("Sumit");
			try {
				Thread.sleep(500);
			}catch(Exception e) {
				System.out.println(e);
			}
		}
	}
	
	public static void main(String[] args) {
		StringPrintWIthTimeWait obj=new StringPrintWIthTimeWait();
		obj.Something();
	}

}

package javainterviewquestionspractice;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Set;

public class FindRepeatedCharacterWithCountInASentance{
	public static void main(String [] arg){
		String sentence="Java programming language is very easy. Easly we can understand if we practice daily.";
		Map<Character, Integer> storeCharacter=new LinkedHashMap<>();
		
		for(String word: sentence.split(" ")){
			for(int i=0; i<word.length();i++){
				char c=word.charAt(i);
				if(storeCharacter.containsKey(c)){
					storeCharacter.put(c,(Integer)
					storeCharacter.get(c)+1);
				}else{
					storeCharacter.put(c,1);
				}
			}
		}

		System.out.println(storeCharacter);

	}
}
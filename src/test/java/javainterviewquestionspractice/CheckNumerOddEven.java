/**sumitkumar
 Sumit_CheckNumerOddEvent.java
 * 28-Mar-2019
 */
package javainterviewquestionspractice;

import java.util.Scanner;

public class CheckNumerOddEven{

	public static void main(String[] args) {
		
		//Q Find the number is positive or not and if that number is positive then find that number is even or not
		int expectedNumber ; 
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter number for check odd or even. First system will check entered number is positve or not");
		
		expectedNumber=sc.nextInt();
		
		if(expectedNumber>0) {
			System.out.print("Entered Number is positve ");
			if(expectedNumber%2==0)
			{
				System.out.println(expectedNumber+" and it is a even number");
			}
			else
			{
				System.out.println(expectedNumber+" and it is an odd number");
			}
		}else {
			System.out.println("Please enter positive number. Your entered number is "+expectedNumber);
		}
		
		sc.close();
	}
}

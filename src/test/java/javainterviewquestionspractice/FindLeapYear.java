/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 01-Feb-2022
 *  FILE NAME  		: 	 FindLeapYear.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:46:38 pm
 */
package javainterviewquestionspractice;

import java.util.Scanner;

public class FindLeapYear {
	
	public static void main(String[] args) {
		int expectedYear;
		Scanner sc=new Scanner(System.in);
		System.out.println("Please enter the year which you want to find is this a leep or not");
		expectedYear=sc.nextInt();
		firstWayFineLeapYear(expectedYear);
		sc.close();
	}
	
	public static void firstWayFineLeapYear(int expectedYear) {
		//First way to find year   -- Divide that year by 4 if the reminder is 0 then that is year is leap or if not then not leap year
		if(expectedYear %400 ==0) {
			System.out.println(expectedYear+" Leap year");
		}else if(expectedYear %4==0 && expectedYear%100!=0 ) {
			System.out.println(expectedYear+" Leap Year");
		}else {
			System.out.println(expectedYear+"Not a leap year");
		}
		
	}

}

/**
 * @author 			:	 get it rent
 *	DATE       		:	 10-Aug-2022
 *  FILE NAME  		: 	 Generate_RandomNumberByGivenLength.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    6:00:34 pm
 */
package javainterviewquestionspractice;

import java.util.Random;

public class GenerateRandomNumberByGivenLength {
	public static String generateRandomeNumber(int exLength) {
		 Random rnd = new Random();
		 int number = 0;
		 
		 if(exLength==4) {
			 number = rnd.nextInt(9999);
		 }else if(exLength==6) {
			 number = rnd.nextInt(999999);
		 }else if(exLength==8){
			 number = rnd.nextInt(99999999);
		 }
		  return String.format("%0"+exLength+"d", number);
		    
	}
	

}

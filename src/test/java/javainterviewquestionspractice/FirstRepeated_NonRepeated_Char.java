package javainterviewquestionspractice;
import java.util.HashMap;

public class FirstRepeated_NonRepeated_Char {

	public static void firstRepeated_NonRepeatedChar(String str) {
		
		HashMap<Character, Integer> charCount=new HashMap<Character, Integer>();
		
		char[] charArry=str.replaceAll("\\s", "").toCharArray();
		for(Character c:charArry) {
			if(charCount.containsKey(c)) {
				charCount.put(c, charCount.get(c)+1);
			}
			else {
				charCount.put(c, 1);
			}
		}
		
		for(Character ch:charArry) {
			if(charCount.get(ch)==1){
				System.out.println("First non repeated character: "+ch);
				break;
				
			}
		}
		
		for(Character ch:charArry) {
			if(charCount.get(ch)>1){
				System.out.println("First Repeated Character:"+ch);
				break;
				
			}
		}
	}
	
	public static void main(String[] args) {
		firstRepeated_NonRepeatedChar("my name is anurag");

	}

}

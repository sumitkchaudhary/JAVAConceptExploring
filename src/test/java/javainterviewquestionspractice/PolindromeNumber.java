package javainterviewquestionspractice;
public class PolindromeNumber {

	/**
	 * Palindrome
	 * A palindrome is a number, word, phrase, or sequence of characters that reads the same backward as forward. Examples of palindromes include:
	 * Numbers: 121, 555, 78787
	 * Words: racecar, level, rotor
	 * Phrases: "A man, a plan, a canal: Panama", "Madam, I'm Adam"
	 * @param args
	 */
	public static void main(String[] args) {
		int n=151,r,temp,sum=0;
		temp=n;

		while(n>0) {
			r=n%10;
			sum=sum*10+r;
			n=n/10;
		}
		if(temp==sum){
			System.out.println("Number is Palindrome: "+sum);
		}else {
			System.out.println("Number is not Palindrome: "+sum);
		}
	}

}

package javainterviewquestionspractice;

import java.util.Arrays;

public class FindSecondLargestElements{
	public static void firstApproachToFindSecondLargetNumber(int[] intArray){
		Arrays.sort(intArray);
		System.out.println("First largest no from the array list \t:\t\t"+intArray[intArray.length-1]);
		System.out.println("Second largest no from the array list \t:\t\t"+intArray[intArray.length-2]);
		System.out.println("Third largest no from the array list \t:\t\t"+intArray[intArray.length-3]);

	}
	public static int secondApproachtoFindSecondLargest(int[] arr) {
		if (arr.length < 2) {
			return -1;
		}

		int largest = Integer.MIN_VALUE;
		int secondLargest = Integer.MIN_VALUE;

		for (int num : arr) {
			if (num > largest) {
				secondLargest = largest;
				largest = num;
			} else if (num > secondLargest && num != largest) {
				secondLargest = num;
			}
		}

		return secondLargest;
	}
	public static void main(String[] args) {	
		int []a={1,4,15,5,45,12,0,2};
		int lengthOfArray = a.length;
		System.out.println("Your total numbers is \t:\t"+lengthOfArray);
		System.out.println("Second largest no \t:\t"+secondApproachtoFindSecondLargest(a));
		firstApproachToFindSecondLargetNumber(a);
	}

}

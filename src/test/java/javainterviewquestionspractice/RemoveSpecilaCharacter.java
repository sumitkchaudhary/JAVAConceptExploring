package javainterviewquestionspractice;
public class RemoveSpecilaCharacter {

	public static void main(String[] args) {
     //Remove special characters
		String str= "This#string%contains123^special*charac456ters&12354.";   
		//str = str.replaceAll("[^a-zA-Z0-9]", " ");  
		System.out.println("Removed Special characters: "+str); 
		
		//Print only special characters
		String str1=  "This#string%contains123^special*charac456ters&12354.";   
		str1 = str1.replaceAll("[a-zA-Z0-9]", "");  
		System.out.println("Print only Special characters: "+str1);   
		
		//Print Only numbers from string
				String str2=  "This#string%contains123^special*charac456ters&12354.";   
				str2 = str2.replaceAll("[^0-9]", "");  
				System.out.println("Print only Numbers: "+str2);   
	}
}

package javainterviewquestionspractice;
import java.util.HashMap;
import java.util.Stack;

public class PolishNotation {

	public static void main(String[] args) {
	   String[] tokens=new String[] {"2","4","+","8","-","4","*"};
	   System.out.println(polishNotaion(tokens));
	}

	public static int polishNotaion(String[] tokens) {
		int returnValue=0;
		String operators="+-*/";
		Stack<String> stack=new Stack<String>();
		
		for(String t:tokens) {
			if(!operators.contains(t)) {
				stack.push(t);
			}
			else {
				int a=Integer.valueOf(stack.pop());
				int b=Integer.valueOf(stack.pop());
				
				switch(t){
				case "+": 
					stack.push(String.valueOf(a+b));
					break;
				case "-":
					stack.push(String.valueOf(a-b));
					break;
				case "*":
					stack.push(String.valueOf(a*b));
				case "/":
					stack.push(String.valueOf(a/b));
					break;
				} 
			}
		}
		returnValue=Integer.valueOf(stack.pop());
		return returnValue;
		
	}

}

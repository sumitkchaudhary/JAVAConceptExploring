package javainterviewquestionspractice;
public class CapitalizeWord {

	public static void main(String[] args) {
		String str="my name is anurag";
		String[] a=str.split(" ");
		String rev=" ";
		
		for(String w:a) {
			String first=w.substring(0,1);
			String second=w.substring(1);
			rev=rev+first.toUpperCase()+second+" ";
		}	
		System.out.println(" "+rev);
	}

}

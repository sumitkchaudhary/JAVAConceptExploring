package javainterviewquestionspractice;
import java.util.HashMap;
import java.util.Map;

public class print_firstNonRepeated_Character 
{

	public static Character firstNonRepeatedCharacter(String str)
	{
	  HashMap<Character,Integer> map=new HashMap<Character,Integer>();
	  char ar[]=str.toCharArray();
	  char ch = 0;
	  
	  for(int i=0;i<ar.length;i++)
	  {
		  ch=ar[i];
		  if(map.containsKey(ch))
		  {
			  map.put(ch, map.get(ch)+1);
		  }
		  else
		  {
			  map.put(ch, 1);
		  }
	  }
	  for(int  i=0;i<ar.length;i++)
	  {
		  ch=ar[i];
		  
		  if(map.get(ch)==1)
		  return ch;
	  }
	//return ch;
	return null;
	  
	}
	public static void main(String[] args)
	{
		String str1="anurag";
		System.out.println("First non repeated character: "+firstNonRepeatedCharacter(str1));	

	}

}

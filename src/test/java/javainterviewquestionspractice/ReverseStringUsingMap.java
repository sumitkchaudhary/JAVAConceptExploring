package javainterviewquestionspractice;
import java.util.HashMap;
import java.util.Map;

public class ReverseStringUsingMap {

	public static void main(String[] args) {
		String str="my name is anurag yadav";
		String[] str1=str.split(" ");
		
		Map<String, String> map=new HashMap<String, String>();
		
		for(int i=str1.length-1;i>=0;i--) {
			String st=str1.toString();
			if(map.containsKey(st)) {
				map.put(st, map.get(st)+1);
			}
			else {
				map.put(st,map.get(st));
			}						
		}
		System.out.print(map);
	}
}

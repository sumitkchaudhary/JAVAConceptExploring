/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 23-Mar-2022
 *  FILE NAME  		: 	 GetPerticularStringFromArrayOfString.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:14:18 pm
 */
package javainterviewquestionspractice;

import java.util.Arrays;

public class GetPerticularStringFromArrayOfString {
	public static void main(String[] args) {
		String strArr[]= {"Processing","INIT_FAID","Success"};
		for(String s: strArr) {
			if(s.equals("Success")) {
				System.out.println("Equals Condition: "+s);
			}
		}
		
		int s=Arrays.binarySearch(strArr, "Success");
		System.out.println(strArr[s]);
		
	}

}

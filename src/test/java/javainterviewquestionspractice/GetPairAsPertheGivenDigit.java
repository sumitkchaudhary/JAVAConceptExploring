/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 25-Aug-2022
 *  FILE NAME  		: 	 GetPairAsPertheGivenDigit.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    4:31:48 pm
 */
package javainterviewquestionspractice;
public class GetPairAsPertheGivenDigit {
	public static void main(String[] args) {
		int intArray[]= {0,1,2,3,5,7,8,10}; //0,10 2,8, 3,7
		int sum=10;
		
		for(int i=0; i<intArray.length;i++) {
			int a=intArray[i];
			for(int j=0; j<intArray.length;j++) {
				int b=intArray[j];
				if((a+b)==sum) {
					System.out.print(a+","+b+"  ");
				}
			}
		}	
	}

}

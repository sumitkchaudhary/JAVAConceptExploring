/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 05-Aug-2019
 *  FILE NAME  		: 	 PrintTable.java
 *  PROJECT NAME 	:	 ReviseJava
 * 
 */
package javainterviewquestionspractice;

import java.util.Scanner;

public class PrintTable 
{
	public static void main(String[] args) {
		
		int desireNo,calculateNo = 0; 
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter a number to print a table");
		
		desireNo=sc.nextInt();
		
		for(int count=1; count<=10;count++)
		{
			calculateNo=desireNo*count;
			 System.out.println(desireNo+" * "+count+" = "+calculateNo);
			
		}
		
	}

}

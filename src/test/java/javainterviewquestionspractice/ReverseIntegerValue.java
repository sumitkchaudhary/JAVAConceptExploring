/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 08-Jan-2023
 *  FILE NAME  		: 	 ReverseIntegerValue.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:32:18 am
 */
package javainterviewquestionspractice;

import java.util.Scanner;

public class ReverseIntegerValue {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number");
		int num=sc.nextInt();
		int i=num;
		//First Logic
		int rev=0;
		while(num!=0) {
			rev=rev*10+num%10; // 0+12345%10= 5 
			num=num/10; //12345/10 =1234 
		}
		System.out.println("Using algorithm : "+rev);
		
		//Second logic
		System.out.println("Using String Buffer : "+new StringBuffer(String.valueOf(i)).reverse());
		
		//Thired Logic
		System.out.println("Using String Builder : "+new StringBuilder(String.valueOf(i)).reverse());
		
	}

}

package javainterviewquestionspractice;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class NotCommonElement_twoArray {

	public static void main(String[] args) {

		
		Integer []a= { 1,3,4,5,6,6, };
		Integer []b= { 4, 5, 6, 7, 8, 9 };
		
		List<Integer> list1=Arrays.asList(a);
		List<Integer> list2=Arrays.asList(b);
		
		Set<Integer> set1=new HashSet<Integer>(list1);
		//now add the List2 elements in List1
		set1.addAll(list2); //Now Set1 have both list elements
		
		Set<Integer> set2=new HashSet<Integer>(list1);	
		//Set2 Store the Common elements from List1 and List2
		set2.retainAll(list2);//Now Set2 have only common elements
		
		//Now remove set2 elements from set1
		set1.removeAll(set2);
		
		for(Integer n:set1) {
			System.out.println(n);
		}	
		
	}
}

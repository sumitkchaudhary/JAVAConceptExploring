package javainterviewquestionspractice;
public class ArraySortUsingBubble 
{
  
	static void sort(int[] arr)
	{
		int n=arr.length;
		int temp=0;
		
		for(int i=0;i<n;i++)
		{
			for(int j=1;j<(n-1);j++)
			{
				if(arr[j-1]>arr[j])
				{
					temp=arr[j-1];
					arr[j-1]=arr[j];
					arr[j]=temp;
				}
			}
		}
	}
	
	public static void main(String[] args) 
	{
		int arr[]={2,3,12,11,58,6,7,21};
        /*System.out.println("Array befor Sorting");
        
        for(int i=0;i<arr.length;i++)
        {
        	System.out.println(arr[i]);
        }
        System.out.println("");*/           
        sort(arr);
        System.out.println("Array after sorting");
        for(int i=1;i<arr.length;i++)
        {
        	System.out.println(arr[i]+" ");
        }
        
        
	}

}

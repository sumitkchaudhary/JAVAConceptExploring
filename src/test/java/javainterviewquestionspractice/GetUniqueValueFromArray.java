/**
 * @author 			:	 Dell
 *	DATE       		:	 24-Nov-2022
 *  FILE NAME  		: 	 GetUniqueValueFromArray.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    6:19:14 pm
 */
package javainterviewquestionspractice;
public class GetUniqueValueFromArray {
	public static void main(String[] args) {
		int []valueOfArray= {1,2,3,4,6,7,7,6,4,6,7,3,1};
		
		for(int i=0;i<valueOfArray.length; i++) {
			int j;
			for(j=0;j<valueOfArray.length; j++) {
				
				if(i!=j && valueOfArray[i]==valueOfArray[j]) {
					break;
				}
			}
			if(j==valueOfArray.length) {
				System.out.println(valueOfArray[i]);
			}
		}
		
		
	}

}

package javainterviewquestionspractice;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MapMethods {

	public static void main(String[] args) {
		HashMap<Integer,String> map=new HashMap<Integer,String>();
		map.put(5,"Mohit");
		map.put(2,"Rahul");
		map.put(1,"Manoj");
		map.put(4,"Pankaj");
		map.put(3,"Anurag");
		map.put(6,"Anchal");
		
		for(Map.Entry m:map.entrySet()) 
		{
			System.out.println(m.getKey()+":"+m.getValue());
		}
		
//		LinkedList<Entry<Integer, String>> list=new LinkedList<Entry<Integer,String>>(map.entrySet());
//        Collections.sort(list,new Comparator(Entry<Integer,String>)){
//        	
//        }
		
		//		Set set=map.entrySet();
//		Iterator itr=set.iterator();
//		
//		while(itr.hasNext()) {
//			System.out.println(itr.next());
//		}
	}

}

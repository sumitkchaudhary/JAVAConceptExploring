package javainterviewquestionspractice;
public class FilterStringAndAddNumbers {

	public static void main(String[] args) {
		String str= "This#string%contains12^special12*char12acters&112.";
		
		StringBuilder num=new StringBuilder(),
		             chars=new StringBuilder(),
		             specialChars=new StringBuilder();
		for(char ch: str.toCharArray()) {
			if(Character.isDigit(ch)) {
				num.append(ch);
			} else if (Character.isAlphabetic(ch)) {
				chars.append(ch);
			}else {
				specialChars.append(ch);
			}
		}
		
		System.out.println("Number in a string: "+num);
		System.out.println("Character in string: "+chars);
		System.out.println("Special character from string: "+specialChars);
		
		String str1=num.toString();
		int numbers=Integer.parseInt(str1);
		
		int sum=0;
		
		while(numbers>0) {
			sum=sum+numbers%10;
			numbers=numbers/10;
		}
		System.out.println("Sum of Numbers: "+sum);
	}

}

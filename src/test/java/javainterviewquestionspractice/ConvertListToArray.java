/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 11-Jan-2023
 *  FILE NAME  		: 	 ConvertListToArray.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    7:55:03 am
 */
package javainterviewquestionspractice;

import java.util.ArrayList;
import java.util.List;


public class ConvertListToArray {
	//Write a program to convert List to Array. 
	public static void main(String[] args) {
		List<Integer> numbers=new ArrayList<>();
		numbers.add(12);
		numbers.add(23);
		numbers.add(54);
		
		System.out.println("List Data");
		for(int a: numbers) {
			System.out.print(a+", ");
		}
		
		numbers.toArray();
	}

}

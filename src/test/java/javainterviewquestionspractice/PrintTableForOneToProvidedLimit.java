/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 02-Feb-2022
 *  FILE NAME  		: 	 PrintTableForOneToProvidedLimit.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    1:09:22 am
 */
package javainterviewquestionspractice;

import java.util.Scanner;

public class PrintTableForOneToProvidedLimit {
	
	
	public static void main(String[] args) {
		
		int numLimit;
		System.out.println("Enter the desire number then system will print a table for till that numbers");
		Scanner sc=new Scanner(System.in);
		
		numLimit=sc.nextInt();
		
		for(int i=1; i<=numLimit; i++) {
			if(i>0) {
				for(int count=1; count<=10; count++ ) {
					
					int calculateNum=i*count;
					System.out.println(i+" X "+count+" = "+calculateNum);
				}
			}
			System.out.println("");
		}
		sc.close();
	}

}

package javainterviewquestionspractice;

import HackerRangeQuestionSolution.GetTheDayNameOfTheDate;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.Scanner;

public class AgeCalculator {
    public static void calculateAge(int dob_Day, int dob_Month, int dob_Year) {
        //Get the day of the date of birth
        String dob_DayOfWeek =GetTheDayNameOfTheDate.findDay(dob_Month, dob_Day, dob_Year);
        System.out.println("Your birth on:- ");
        System.out.println(dob_DayOfWeek +" : "+dob_Day+"/"+dob_Month+"/"+dob_Year);
        //Get the current year , month, and Day
        int current_Year= LocalDate.now().getYear();
        int current_Month= LocalDate.now().getMonthValue();
        int current_Day= LocalDate.now().getDayOfMonth();
        System.out.println("Today date is :- ");
        //Get the day of the current date
        String current_DayOfWeek=GetTheDayNameOfTheDate.findDay(current_Month, current_Day, current_Year);
        System.out.println(current_DayOfWeek +" : "+current_Day+"/"+current_Month+"/"+current_Year);
        int age_Days;
        int age_Months = 0;
        int age_Years = 0;
        //Check current date is less than or not
        if(current_Day<dob_Day){ // Yes current day is less than date of day
            //Check current month equal 2
            if(current_Month==2){//Yes current month is equal to 2
                //Check current year is leeap or not
                if(current_Year%4==0 && current_Year%400==0 && current_Year%100!=0){ //yes current year is a leap year
                    age_Days=(current_Day+29)-dob_Day;
                }else{ //Current year is not a leap year
                    age_Days=(current_Day+28)-dob_Day;
                }
            //Check current month is lies on (1,3,5,7,8,10,and 12)
            } else if (current_Month==1 || current_Month==3 ||current_Month==5 || current_Month==7 ||
                    current_Month==8 ||current_Month==10 || current_Month==12 ) {//Yes current month is lies on given months
                age_Days=(current_Day+31)-dob_Day;
            } else {//No current month is not lies on given months
                age_Days=(current_Day+30)-dob_Day;
            }
            //Substract 1 from the current month as because current day is less than date of birth day
            current_Month=current_Month-1;
            //Check current month is greater than date of birth month or not
            if(current_Month>=dob_Month){ //yes current month is greater than
                age_Months=current_Month-dob_Month;
                age_Years=current_Year-dob_Year;
            }else {//current month is not greater than
                age_Months=(current_Month+12)-dob_Month;
                age_Years=(current_Year-1)-dob_Year;
            }
        }else { // current day is not less than date of day
            age_Days=current_Day-dob_Day;
            //Check current month is greater than date of birth month or not
            if(current_Month>=dob_Month){ //yes current month is greater than
                age_Months=current_Month-dob_Month;
                age_Years=current_Year-dob_Year;
            }else {//current month is not greater than
                age_Months=(current_Month+12)-dob_Month;
                age_Years=(current_Year-1)-dob_Year;
            }
        }
        System.out.println("Your age is :- ");
        System.out.println(age_Years+ " years old, "+age_Months+" months, "+age_Days+" days");
    }
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter your date of birth/ joining/ aniversory)");
        calculateAge(sc.nextInt(),sc.nextInt(),sc.nextInt());
    }
}



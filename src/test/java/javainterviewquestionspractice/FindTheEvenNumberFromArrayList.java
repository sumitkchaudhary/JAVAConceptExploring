/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 21-Jan-2023
 *  FILE NAME  		: 	 FindTheEvenNumberFromArrayList.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    8:27:53 pm
 */
package javainterviewquestionspractice;
import java.util.Scanner;

public class FindTheEvenNumberFromArrayList {
	public static void main(String args[]) {
		Scanner sc=new Scanner(System.in);
		System.out.println("How many number you want to enter?");
		
		int arraySize=sc.nextInt();
		int []arrayList=new int[arraySize];
		System.out.println("Please enter the numbers.");
		for(int i=0 ;i <arrayList.length; i++) {
			arrayList[i]=sc.nextInt();
		}
		for(int number: arrayList) {
			if(number%2==0) {
				System.out.println("Even number is: "+number);
			}
		}
	}

}

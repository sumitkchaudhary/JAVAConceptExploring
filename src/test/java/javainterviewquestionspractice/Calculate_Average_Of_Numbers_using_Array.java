/**sumitkumar
 Sumit_Calculate_Average_Of_Numbers_using_Array.java
 * 28-Mar-2019
 */
package javainterviewquestionspractice;

public class Calculate_Average_Of_Numbers_using_Array{
	public  static double average(int a[], int n){
		int sum=0;
		for (int i=0; i<n;i++){
			sum = sum+a[i];	
		}
		return sum/n;
	}
	
	public static void main(String[] args) {
		int arr[]= {1,2,3,4,5};
		int x=arr.length;
		System.out.println("out put is : "+average(arr, x));
	}
}

package javainterviewquestionspractice;
public class StarPatternPrograms {
                                           
	public static void main(String[] args) {
		
		int n=5;
			for(int i=0;i<=n;i++) {
			for(int j=1;j<=i;j++) {
				System.out.print(" *");
			}
			System.out.println("");
		}
		//O/P-
//		 *
//		 * *
//		 * * *
//		 * * * *
//		 * * * * *
		
		for(int i=1;i<=n;i++) {
			for(int j=n;j>=i;j--) {
				System.out.print(" *");
			}
			System.out.println("");
		}
//		O/P -
		
//	     * * * * * *
//		 * * * * *
//		 * * * *
//		 * * *
//		 * 
		
		for(int i=1;i<=n;i++) {
			for(int j=1;j<=i;j++) {
				System.out.print(" *");
			}
			System.out.println("");
		}
		for(int i=1;i<=n;i++) {
			for(int j=n-1;j>=i;j--) {
				System.out.print(" *");
			}
			System.out.println("");
		}
		
		//O/P
//		 *
//		 * *
//		 * * *
//		 * * * *
//		 * * * * *
//		 * * * *
//		 * * *
//		 * *
//		 *
		
		for(int i=1;i<=n;i++) {
			for(int j=n-1;j>=i;j--) {
				System.out.print(" ");
			}
			for(int k=1;k<=i;k++) {
				System.out.print("*");
			}
			System.out.println("");
		}
		//O/P
		
//		    *
//		   **
//		  ***
//		 ****
//		*****
		for(int i=1;i<=n;i++) {
			for(int j=2;j<=i;j++) {
				System.out.print(" ");
			}
			for(int k=n;k>=i;k--) {
				System.out.print("*");
			}
			System.out.println("");
		}
		// O/p -
//		*****
//		 ****
//		  ***
//		   **
//		    *
		
		for(int i=1;i<=n;i++) {
			for(int j=n-1;j>=i;j--) {
				System.out.print(" ");
			}
			for(int k=1;k<=i;k++) {
				System.out.print("*");
			}
			System.out.println("");
		}
		for(int i=1;i<=n;i++) {
			for(int j=1;j<=i;j++) {
				System.out.print(" ");
			}
			for(int k=n-1;k>=i;k--) {
				System.out.print("*");
			}
			System.out.println("");
		}
		//O/P-
//		    *
//		   **
//		  ***
//		 ****
//		*****
//		 ****
//		  ***
//		   **
//		    *
		
		
		for(int i=1;i<=n;i++) {
			for(int j=n-1;j>=i;j--) {
				System.out.print(" ");
			}
			for(int k=1;k<=i;k++) {
				System.out.print(" *");
			}
			System.out.println("");
		}
		//O/P
//		     *
//		    * *
//		   * * *
//		  * * * *
//		 * * * * *
		for(int i=1;i<=n;i++) {
			for(int j=2;j<=i;j++) {
				System.out.print(" ");
			}
			for(int k=n;k>=i;k--) {
				System.out.print(" *");
			}
			System.out.println("");
		}
		// O/p -
		
//		 * * * * *
//		  * * * *
//		   * * *
//		    * *
//		     *
		
		for(int i=1;i<=n;i++) {
			for(int j=n-1;j>=i;j--) {
				System.out.print(" ");
			}
			for(int k=1;k<=i;k++) {
				System.out.print(" *");
			}
			System.out.println("");
		}
	
		for(int i=1;i<=n;i++) {
			for(int j=1;j<=i;j++) {
				System.out.print(" ");
			}
			for(int k=n-1;k>=i;k--) {
				System.out.print(" *");
			}
			System.out.println("");
		}
		// O/p -
		
//		     *
//		    * *
//		   * * *
//		  * * * *
//		 * * * * *
//		  * * * *
//		   * * *
//		    * *
//		     *
		
	
		for(int i=1;i<=n;i++) {
			for(int j=n-1;j>=i;j--) {
				System.out.print(" ");
			}
			for(int k=1;k<=i;k++) {
				System.out.print("*");
			}
			for(int k=2;k<=i;k++) {
				System.out.print("*");
			}
			System.out.println("");
		}
		//O/P-
//		    *
//		   ***
//		  *****
//		 *******
//		*********
		
		for(int i=1;i<=n;i++) {
			for(int j=n-1;j>=i;j--) {
				System.out.print(" ");
			}
			for(int k=1;k<=i;k++) {
				if(k==1 || i==n) {
				System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}
			for(int k=2;k<=i;k++) {
				if(k==i || i==n) {
					System.out.print("*");
					} else {
						System.out.print(" ");
					}
			}
			System.out.println("");
		}
		// O/P-
		
//		    *
//		   * *
//		  *   *
//		 *     *
//		*********
		
		for(int i=1;i<=n;i++) {
			for(int j=2;j<=i;j++) {
				System.out.print(" ");
			}
			for(int k=n;k>=i;k--) {
				System.out.print("*");
			}
			for(int k=n-1;k>=i;k--) {
				System.out.print("*");
			}
			System.out.println("");
		}
		
		//O/P-
//		*********
//		 *******
//		  *****
//		   ***
//		    *
		
		for(int i=1;i<=n;i++) {
			for(int j=2;j<=i;j++) {
				System.out.print(" ");
			}
			for(int k=n;k>=i;k--) {
				if(k==n||i==1) {
				System.out.print("*");}
				else {
					System.out.print(" ");
				}
			}
			for(int k=n-1;k>=i;k--) {
				if(k==i || i==1) 
				{
				System.out.print("*");}
				else
				{
				System.out.print(" ");	
				}
			}
				
			System.out.println("");
		}
// O/P-
		
//		*********
//		 *     *
//		  *   *
//		   * *
//		    *
	
		for(int i=1;i<=n;i++) {
			for(int j=1;j<=i;j++) {
				System.out.print(" ");
			}
			for(int k=n;k>=i;k--) {
				System.out.print(" *");
			}
			System.out.println("");
		}
		for(int i=1;i<=n;i++) {
			for(int j=n;j>=i;j--) {
				System.out.print(" ");
			}
			for(int k=1;k<=i;k++) {
				System.out.print(" *");
			}
			System.out.println("");
		}
	
		// O/P
//		  * * * * *
//		   * * * *
//		    * * *
//		     * *
//		      *
//		      *
//		     * *
//		    * * *
//		   * * * *
//		  * * * * *
		
	}
		
}

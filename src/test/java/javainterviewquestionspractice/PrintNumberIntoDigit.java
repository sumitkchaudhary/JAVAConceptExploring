/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 28-Mar-2022
 *  FILE NAME  		: 	 PrintNumberIntoDigit.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    7:16:51 pm
 */
package javainterviewquestionspractice;
public class PrintNumberIntoDigit {
	private static void convertNumberInDigit(char digitValue) {
		
		switch (digitValue) {
		case '0':
			System.out.println("Zero");
			break;
		case '1':
			System.out.println("One");
			break;
		case '2':
			System.out.println("Two");
			break;
		case '3':
			System.out.println("Three");
			break;
		case '4':
			System.out.println("Four");
			break;
		case '5':
			System.out.println("Five");
			break;
		case '6':
			System.out.println("Six");
			break;
		case '7':
			System.out.println("Seven");
			break;
		case '8':
			System.out.println("Eight");
			break;
		case '9':
			System.out.println("Nine");
			break;

		default:
			break;
		}
	}
	
	public static void printInWord(String no) {
		for(int i=0;i<no.length();i++) {
			convertNumberInDigit(no.charAt(i));
		}
	}
	
	public static void main(String[] args) {
			printInWord("9971458090");
		
		
	}

}

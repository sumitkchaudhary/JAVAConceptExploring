package javainterviewquestionspractice;

import java.util.*;
import java.util.stream.Collectors;

public class CommonElementsFromTwoIntArray {
	public static void fistApproachToFindCommonInteger() {
		int[] firstIntArray = {1, 4, 7, 9, 8, 3, 11};
		int[] secondIntArray = {2, 4, 6, 3, 10, 11, 9};
		/*Set<Integer> commonElement = new HashSet<>();
		creates a HashSet to store the common elements. Sets ensure uniqueness, so no duplicates will be added.
		* */
		Set<Integer> commonElement = new HashSet<>();
		for (int number : firstIntArray) {
			for (int i = 0; i < secondIntArray.length; i++) {
				if (secondIntArray[i] == number) {
					commonElement.add(number);
				}
			}
		}

		for (int i : commonElement) {
			System.out.println(i);
		}

	}

	public static void findCommonElements() {
		int[] arr1 = new int[]{1, 2, 3, 4, 5, 6, 7};
		int[] arr2 = new int[]{1, 3, 4, 5, 6, 9, 8};

		HashMap<Integer, Integer> hashMap = new HashMap<>();
		for (int i = 0; i < arr1.length; i++) {
			if (hashMap.containsKey(arr1[i])) {
				hashMap.put(arr1[i],
						hashMap.get(arr1[i]) + 1);
			} else {
				hashMap.put(arr1[i], 1);
			}
		}

		for (int i = 0; i < arr2.length; i++) {
			if (hashMap.containsKey(arr2[i])) {
				hashMap.remove(arr2[i]);
              /* remove common elements from hashmap
              to avoid duplicates*/
				System.out.print(arr2[i] + " ");
			}
		}
	}

	public static void thiredApproachToFindCommon() {
		int[] array1 = {4, 2, 3, 1, 6};
		int[] array2 = {6, 7, 9, 8, 4};
		List<Integer> commonElements = new ArrayList<>();
		for (int i = 0; i < array1.length; i++) {
			for (int j = 0; j < array2.length; j++) {
				if (array1[i] == array2[j]) {
					commonElements.add(array1[i]);
					break;
				}
			}
		}
		System.out.println("Common Elements: " + commonElements);
	}

	public static void fourthApproachToFindCommonElement() {
		Integer[] array1 = {4, 2, 7, 1, 6};
		Integer[] array2 = {6, 7, 9, 8, 4};
		ArrayList<Integer> list1 = new ArrayList<>(Arrays.asList(array1));
		ArrayList<Integer> list2 = new ArrayList<>(Arrays.asList(array2));
		list1.retainAll(list2);
		System.out.println("Common Elements: " + list1);
	}

	public static void fifthApproachToFindCommonElement() {
		Integer[] array1 = {4, 2, 7, 1, 6};
		Integer[] array2 = {6, 7, 9, 8, 4};
		Set<Integer> set1 = new HashSet<>(Arrays.asList(array1));
		Set<Integer> set2 = new HashSet<>(Arrays.asList(array2));
		set1.retainAll(set2);
		System.out.println("Common Elements: " + set1);
	}

	public static void sixthApproachToFindCommon() {
		Integer[] array1 = {4, 2, 7, 1, 6};
		Integer[] array2 = {6, 7, 9, 8, 4};
		Set<Integer> set1 = new HashSet<>(Arrays.asList(array1));
		Set<Integer> set2 = new HashSet<>(Arrays.asList(array2));
		Set<Object> commonelements = set1.stream()
				.filter(set2::contains)
				.collect(Collectors.toSet());
		System.out.println("Common Elements: " + commonelements);

	}

	public static void findCommonFromThreeArray() {
		//findCommonElement fcm=new findCommonElement();
		int[] a = {1, 4, 7, 9, 8, 3};
		int[] b = {2, 4, 6, 3, 10, 11, 9};
		int[] c = {1, 4, 6, 7, 15, 8, 16, 9};
		System.out.println("Common element is -");
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < b.length; j++) {
				for (int k = 0; k < c.length; k++) {
					if (a[i] == b[j] && b[j] == c[k]) {

						System.out.println(a[i]);
					}

				}
			}
		}
	}
}









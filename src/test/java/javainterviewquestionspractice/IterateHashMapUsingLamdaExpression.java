/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 08-Jan-2023
 *  FILE NAME  		: 	 LamdaExpression.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:15:30 pm
 */
package javainterviewquestionspractice;

import java.util.HashMap;
import java.util.Map;

public class IterateHashMapUsingLamdaExpression {
	public static void main(String[] args) {
		Map<String, Integer> items=new HashMap<>();
		items.put("Apple", 90);
		items.put("Banana", 50);
		items.put("Grapes", 80);
		items.put("Papaya", 40);
		items.put("Black berry", 150);
		items.forEach((k,v) -> System.out.println("fruite: "+ k +", price: "+v));
	}
}

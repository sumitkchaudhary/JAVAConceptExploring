package javainterviewquestionspractice;
public class Factorial {

	static int factorial(int n) {
		if(n==1) {
			return 1;
		}else
			return(n*factorial(n-1));
	}
	public static void secondApproachTofindFactorial(int num){
		int i,fact=1;
		for(i=1;i<=num;i++) {
			fact=fact*i;
		}
		System.out.println(fact);
	}

	public static void main(String[] args) {

	}

}

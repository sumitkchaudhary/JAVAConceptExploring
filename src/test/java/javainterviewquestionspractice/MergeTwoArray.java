package javainterviewquestionspractice;
import java.util.Arrays;

public class MergeTwoArray {
	public static void firstApproachMergeArray(){
		int[] arr1={1,15,16,4,12};
		int[] arr2={17,19,21,89,31};
		int[] merge=new int[arr1.length+arr2.length];
		int c=0;
		//Arrays.sort(merge);
		for(int i=0;i<arr1.length;i++) {
			merge[i]=arr1[i];
			c++;
		}
		for(int j=0;j<arr2.length;j++) {
			merge[c++]=arr2[j];
		}
		for(int i=0;i<merge.length;i++) {
			System.out.println(merge[i]);
		}
	}

	public static void secondApproachToMerge(){
		int[] a= {2,4,6,7,8,9,11,12};
		int[] b= {3,5,1,13,17,19,21};
		int[] result=new int[a.length+b.length];

		System.arraycopy(a,0,result,0,a.length);
		System.arraycopy(b,0,result,a.length,b.length);
		System.out.println(Arrays.toString(result));

	}
	public static void main(String[] args) {

	}

}

package javainterviewquestionspractice;
public class EvenOddNumber {

	public static void main(String[] args) {
		int a[]={1,2,3,4,5,6,7,8,9};
		System.out.println("Below are Odd number is: ");

		for(int i=0;i<a.length;i++) {
			if(a[i]%2!=0) {
				System.out.println(a[i]);
			}
		}
      System.out.println("Below are even numbers is: ");
      
      for(int i=0;i<a.length;i++)
      {
    	  if(a[i]%2==0)
  		{
  			System.out.println(a[i]);
  		}
      }
	}

}

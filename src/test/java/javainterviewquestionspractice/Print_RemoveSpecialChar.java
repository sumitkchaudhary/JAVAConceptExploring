package javainterviewquestionspractice;
public class Print_RemoveSpecialChar {
	public static void main(String[] args) {
		String str= "This#string%contains123^special*charac456ters&12354."; 
		//str=str.replaceAll("[^a-z,A-Z,0-9]", " ");
		//same as above
		str=str.replaceAll("\\W", " ");
		System.out.println("Remove Special Character: "+str);
		
		String str1= "This#string%contains123^special*charac456ters&12354."; 
//		str1=str1.replaceAll("[a-z,A-Z,0-9]", " ");
        str1=str1.replaceAll("\\w", "");
	    System.out.println("Print Special Character: "+str1);
//		
		String str2= "This#string%contains123^special*charac456ters&12354."; 
		 str2=str2.replaceAll("[^0-9]", " ");
		System.out.println("Print only numbers: "+str2);

// for Getting addition of Numbers
		int sum=0;		
		char[] c=str.toCharArray();
		
		for(int i=0;i<c.length;i++) {
			if(Character.isDigit(c[i])) {
				int a=Integer.parseInt(String.valueOf(c[i]));
				sum=sum+a;
			}
		}
		System.out.println("Total Number in String: "+c.length);
		System.out.println("Sum of numbers:"+sum);
	}

}

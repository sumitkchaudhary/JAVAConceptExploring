package javainterviewquestionspractice;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddElementToArray {

	public static void main(String[] args) {
		Integer[] a= {2,4,6,7,8,9,11,12};	
		addElement(a,17,2);

	}
	
	public static void addElement(Integer[] arr,int element,int position) {
		List<Integer> list=new ArrayList<Integer>(Arrays.asList(arr));
		list.add(position-1,element);
		arr=list.toArray(arr);
		
		System.out.println("Orginal Arrays:"+Arrays.toString(arr));
		System.out.println("Array with element:"+element+" at postion "+position+" in array"+Arrays.toString(arr));
	}

}

/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 01-Aug-2019
 *  FILE NAME  		: 	 CountTheStringChar.java
 *  PROJECT NAME 	:	 BasicCoceptUnderstading
 * 
 */
package javainterviewquestionspractice;

import java.util.Scanner;

public class CountWordAndCharacter {
	public static void CountWordAfterRemoveSpaceUsingSplit(String str){
		String[] a=str.split(" ");
		String rev="";
		for(int i=0;i<a.length;i++)
		{
			rev=rev+a.length;

		}
		System.out.print("Total word in String :"+rev.length());
	}
	public static void characterAfterRemoveSpaceWithoutUsingSplitMethod(String str){
		int totalChar=0;
		for(char c: str.toCharArray()){
			if(c!=' '){
				totalChar++;
			}
		}
		System.out.println(totalChar);
	}
	public static int stringWordCount(String enterSentences){
		return enterSentences.split(" ").length;
	}

	public static int characterCount(String enterSentences){
		return  enterSentences.toCharArray().length;
	}
	public static void CharacterLengthAfterRemoveSpaceWithoutSplitMethod(String str) {
		String afterRemoveSpace=str.replaceAll("\\s","");
		System.out.println("Orignal String: \t"+str);

		System.out.println("Remove Space: \t"+afterRemoveSpace);
		System.out.println("Orignal String length: \t"+str.length());

		System.out.println("Length after remove Space: \t"+afterRemoveSpace.length());

	}

	public static void main(String[] args) {
		CharacterLengthAfterRemoveSpaceWithoutSplitMethod("SumitKumar");
	}

	

}

/**
 * @author 			:	 get it rent
 *	DATE       		:	 26-Aug-2022
 *  FILE NAME  		: 	 KaprekarConstant.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    5:41:03 pm
 */
package javainterviewquestionspractice;

import java.util.Arrays;

public class KaprekarConstant {
	/*A Kaprekar number is a number whose square when divided into two parts and such that sum 
	 * of parts is equal to the original number and none of the parts has value 0. */
	/*Take any four digit number and do the following:
		Rearrange the string of digits to form the largest and smallest 4-digit numbers possible.
		Take these two numbers and subtract the smaller number from the larger. Count also how many 
		iterations loop gets.
		Suppose we choose the number 8082.
		8820−0288=8532
		8532−2358=6174
		7641−1467=6174 
		It hits 6174 and then stops.
	 * */
	public static int  firstMethodOfKaprekarConstant(int num) {
        int diff = 0, stepCount = 0;
        while (diff != 6174) {
            String s1 = String.valueOf(num);

            char[] ch1 = new char[s1.length()];
            for (int i = 0; i < ch1.length; i++) {
                ch1[i] = s1.charAt(i);
            }
            Arrays.sort(ch1);
            String s2 = new String(ch1);
            String s3 = "";
            for (int j = s2.length() - 1; j >= 0; j--) {
                s3 += s2.charAt(j);
            }

            int a = Integer.parseInt(s2);
            int b = Integer.parseInt(s3);
            if (a > b) {
                diff = a - b;
                System.out.println(a+"-"+b+"="+diff);
            } else if (b > a) {
                diff = b - a;
                System.out.println(b+"-"+a+"="+diff);
            } else {
                break;

            }
            stepCount++;
            num = diff;
        }
        return stepCount;
    }
	
	private static int secondMethodOfKaprekarConstant(int n, int prev){
			if (n == 0)
				return 0;
			prev = n;
			int[] digits=new int[4];
			for (int i=0; i<digits.length; i++){
				digits[i] = n%10;
				n = n/10;
			}
			Arrays.sort(digits);
			int asc = 0;
			for (int i=0; i<digits.length; i++)
			asc = asc*10 + digits[i];

			Arrays.sort(digits);
			int desc = 0;
			for (int i=digits.length-1; i>=0; i--)
			desc = desc*10 + digits[i];

			int diff = Math.abs(asc - desc);

			if (diff == prev)
				return diff;
			return secondMethodOfKaprekarConstant(diff, prev);
		}

		public static int kaprekar(int n){
			int prev = 0;
			return secondMethodOfKaprekarConstant(n, prev);
		}

}

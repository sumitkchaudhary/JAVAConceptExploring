/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 13-Mar-2022
 *  FILE NAME  		: 	 FindGreaterVsLessValueWithoutIfElse.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:27:55 pm
 */
package javainterviewquestionspractice;
public class FindGreaterVsLessValueWithoutIfElse {
	public static void main(String[] args) {
		//Q : Find the greater and smallest value from two digit without using if-else ? 
		int a=10;
		int b=200;
		
		int smallestVal=a<b?a:b;
		System.out.println("Smallest Value is "+smallestVal);
		
		int greaterVal=a>b?a:b;
		System.out.println("Greater Value is "+greaterVal);
	}

}

package javainterviewquestionspractice;

import java.util.HashSet;
import java.util.Set;

public class CommonCharacterFromString {

	public static void main(String[] args) {
		String firstString = "aabbcc";
		String secondString = "abcefg";

		Set<Character> commonCharacter=new HashSet<>();
		for(char character: firstString.toCharArray()){
			if(secondString.indexOf(character)!=-1){
				commonCharacter.add(character);
			}
		}

		for(char c: commonCharacter){
			System.out.println(c);
		}




	}
}

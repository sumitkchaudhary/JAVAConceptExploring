package javainterviewquestionspractice;

public class CountDigitOfInteger {
	public static int countDigit(int num) {
		int countNumber=0;
		while(num!=0) {
			num/=10;
			countNumber++;		
		}
		return countNumber;
	}
	public static void main(String[] args) {
		System.out.println(countDigit(35));
	}
}

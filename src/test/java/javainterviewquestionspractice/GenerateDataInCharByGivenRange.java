/**
 * @author 			:	 get it rent
 *	DATE       		:	 10-Aug-2022
 *  FILE NAME  		: 	 Generate_RandomAlbhabeticByGivenRange.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    6:03:17 pm
 */
package javainterviewquestionspractice;
public class GenerateDataInCharByGivenRange {
	public static String generateRandomeAlbhabetic(int n) {
		
        // chose a Character random from this String
        String alphaString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  
        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);
  
        for (int i = 0; i < n; i++) {
            int index= (int)(alphaString.length()* Math.random());
  
            // add Character one by one in end of sb
            sb.append(alphaString.charAt(index));
        }
  
        return sb.toString();
    }


}

/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 05-Aug-2019
 *  FILE NAME  		: 	 ArmstrongNumber.java
 *  PROJECT NAME 	:	 ReviseJava
 * 
 */
package javainterviewquestionspractice;

import java.util.Scanner;

public class ArmstrongNumber {
	public static void main(String[] args) {
		int expectedNumber, orignalNum, reminder, result=0 ;
		System.out.println("Enter The number to check its Armstrong or not");
		Scanner sc = new Scanner(System.in);
		
		expectedNumber=sc.nextInt();
		sc.close();
		orignalNum=expectedNumber;
		
		while(orignalNum!=0){
			reminder = orignalNum%10;
			result+= reminder*reminder*reminder;
			orignalNum /=10;
		}
		
		if (result == expectedNumber){
			System.out.println("Armstrong");
		}
		else{
			System.out.println("Not armstrong");
		}
	}

}

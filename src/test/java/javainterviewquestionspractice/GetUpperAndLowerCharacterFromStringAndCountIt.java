/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 13-Mar-2022
 *  FILE NAME  		: 	 GetUpperAndLowerCharacterFromStringAndCountIt.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    9:34:44 pm
 */
package javainterviewquestionspractice;
public class GetUpperAndLowerCharacterFromStringAndCountIt {
	//Q: Count and print the lower and small character from the given string
	public static void main(String[] args) {
		String str="My Name is Sumit Kumar Chaudhary";
		firstWayToFind(str);
		secondWayToFind(str);
		
	}
	
	public static void firstWayToFind(String str) {
		String upperChar="";
		String lowerChar="";
		for(int i=0; i<str.length();i++) {
			char c=str.charAt(i);
			if(c!=32) {
				if(c>=65 && c<=90 ) {
					upperChar=upperChar+c;
				}else {
					lowerChar=lowerChar+c;
				}
			}
		}
		System.out.println("Upper Character count is: "+upperChar.length());
		System.out.println("Lower Character count is: "+lowerChar.length());
		System.out.println("Upper Character is: "+upperChar);
		System.out.println("Lower Character is: "+lowerChar);
	}
	public static void secondWayToFind(String str) {
		int upperChar=0;
		int lowerChar=0;
		
		for(int i=0; i<str.length();i++) {
			char c=str.charAt(i);
			if(c!=32) {//32 ASCII value of space
				if(c>'A' && c<'Z' ) {  
					upperChar++;
				}else {
					lowerChar++;
				}
			}
		}
		System.out.println("Upper Character is: "+upperChar);
		System.out.println("Lower Character is: "+lowerChar);
	}

}

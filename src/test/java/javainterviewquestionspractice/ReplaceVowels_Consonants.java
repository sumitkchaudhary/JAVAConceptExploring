package javainterviewquestionspractice;
public class ReplaceVowels_Consonants {

	public static void main(String[] args) {
        String str="My name122 is anurag Yadav and I am automation engineer in bold technology .";
	
		String sts=str.replaceAll("[^aeiouAEIOU]","_");//It will replace consonants to special characters	
		String sts1=str.replaceAll("[aeiouAEIOU]","_");//It will replace all vowels to special character
		//System.out.println("Replace  only character to vowels : "+sts);
		
        String st=str.replaceAll("[^aeiouAEIOU]",""); //It will remove only consonants	
		String st1=str.replaceAll("[aeiouAEIOU,0-9]","");//It will remove only vowels
		
		System.out.println("Consonants are:"+st1);
	}

}

/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 04-Aug-2019
 *  FILE NAME  		: 	 CheckPrimeFromList.java
 *  PROJECT NAME 	:	 ReviseJava
 * 
 */
package javainterviewquestionspractice;

import java.util.Scanner;

public class PrimeNumberFromMultipleNumbers {
	public static void main(String[] args) { 
		int n=0,isPrime = 0;
		System.out.println("How many numbers you want to enter");
		Scanner sc= new Scanner(System.in);
		n=sc.nextInt();
		System.out.println("Enter the numbers for checked is prime or not ");
		int[] arr = new int[n];
		for (int i=0; i<n; i++){
			arr[i]=sc.nextInt();
			int counter =0;
			for(int num =arr[i]; num>=1 ; num--){
				if(i%num==0){
					 counter = counter + 1;
				}
			}
			if (counter == 2){
				isPrime = isPrime + i;
			}			
		}
		sc.close();
		System.out.println("Prime numbers from the list"+isPrime);
	}

}

package javainterviewquestionspractice;

import java.util.Scanner;

public class CheckPrime {

	public static void firstWayToFindPrimeNumber(int n){
		int  m = 0, flag = 0;
		m = n / 2;
		if (n == 0 || n == 1) {
			System.out.println("number is not prime");
		} else {
			for (int i = 2; i <= m; i++) {
				if (n / i == 0) {
					System.out.println("number is not prime.");
					flag = 1;
					break;
				}
			}
		}

		if (flag == 0) {
			System.out.println(n + " is prime number");
		}
	}

	public static void secondApproachToFindPrime(){
		int num;
		boolean isPrime = true;// check the true and false
		System.out.println("Please enter the number for check its prime or not\n ");
		Scanner sc=new Scanner(System.in);
		num=sc.nextInt();
		for (int i=2; i<=num/2; i++){
			if (num%i == 0){
				isPrime=false;
				break;
			}
		}
		if(isPrime){
			System.out.println("Your entered number is prime");
		}
		else{
			System.out.println("Your entered number is not prime");
		}
		sc.close();
	}

	public static void main(String[] args) {


	}

}

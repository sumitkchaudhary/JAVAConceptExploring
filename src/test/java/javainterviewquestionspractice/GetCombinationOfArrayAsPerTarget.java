package javainterviewquestionspractice;

import java.util.Arrays;

public class GetCombinationOfArrayAsPerTarget {
    /**
     * Question : Get the combination of two elements from array thos addition total should be the
     * desire target i.e 5
     * Out condition.
     * 1. The first element must be less then the second element
     * 2. Combination must be print in ascending order
     * 3. Combination of two element total of sum must be the desire target i.e 5
     */
    public static void main(String[] args) {
        int []arrayElement={4,2,-1,3,6,1,-2,7};
        int targetOfSum=5;
        Arrays.sort(arrayElement);
        int firstElement;
        int secondElement;
        for(int ele: arrayElement){
            firstElement=ele;
            for(int i=1; i<=arrayElement.length-1; i++){
                secondElement=arrayElement[i];
                if((firstElement+secondElement)==targetOfSum)
                    // If we comment this condition then we get the all possible combination
                    if(firstElement<secondElement)
                        System.out.println("("+firstElement+","+secondElement+")");
            }
        }
    }
}

package javainterviewquestionspractice;
import java.util.Arrays;

public class AnagramString {


	public static void main(String[] args) {
		findAnagramString("listen","Silent");
	}

	/**
	 An anagram is a word or phrase formed by rearranging the letters of a different word or phrase.
	 In other words, it's a string that contains the same characters as another string, but in a different order.
	 For example, "listen" and "silent" are anagrams of each other. Both words have the same letters ('l', 'i', 's', 't', 'e', 'n') but arranged differently.
	 Here are some other examples of anagrams:
	 "dormitory" and "dirty room"
	 "cinema" and "ice man"
	 Not all words can be anagrams of each other. For instance, "hello" and "world" are not anagrams because "hello" has two 'l's' while "world" has none.
	 There are many ways to check if two strings are anagrams. Here's a simple approach:

	 1. Sort the characters of both strings.
	 2. Compare the sorted strings. If they are identical, the original strings are anagrams.
	 * */
	public static void findAnagramString(String str1, String str2){
		char[] ch1=str1.toLowerCase().toCharArray();
		char[] ch2=str2.toLowerCase().toCharArray();

		Arrays.sort(ch1);
		Arrays.sort(ch2);
		if(Arrays.equals(ch1, ch2)) {
			System.out.println("Both string are anagram.");
		}else {
			System.out.println("Both string are not anagram.");
		}
	}

}

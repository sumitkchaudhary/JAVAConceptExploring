package javainterviewquestionspractice;
public class Largest_SmallestWithoutSortingCollection {

	public static void main(String[] args) {
	 int arr[] = {10, 324, 45, -90, 9808}; 
	 
	 int max=0;//Initialize largest element
	 int min=arr[0];//Initialize smallest element
	 // Traverse array elements from second and
     // compare every element with current max
	 for(int i=1;i<arr.length;i++) {
		 if(arr[i]>max) {
			 max=arr[i];		
			
		 }
		 else if(arr[i]<min) {
			 min=arr[i];				
		 }
	  }
	 System.out.println("Largest element"+max);
	 System.out.println("Smallest element"+min);
	 int max2=max-min;
	 
	 System.out.println("Difference between max and min element"+max2);
	}

}

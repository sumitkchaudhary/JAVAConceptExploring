/**
 * @author 			:	 get it rent
 *	DATE       		:	 05-Sep-2022
 *  FILE NAME  		: 	 RemoveLeadingZeroFromString.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    5:09:09 pm
 */
package javainterviewquestionspractice;
public class RemoveLeadingZeroFromString {

	public static String removeLeadingZeros(String string){
      for(int i=0;i<string.length();i++){
             if(string.charAt(i)!='0'){
                String res = string.substring(i);
                return res;
            }
        }
         return "0";
    }
	
	public static String removeStartingZeroFromString(String string) {
		if(string.startsWith("0")) {
			return string.substring(1);
		}
		return string;
	}
	
	public static void main(String[] args) {
		System.out.println(removeLeadingZeros("0001"));
		System.out.println(removeStartingZeroFromString("0009"));
	}


}

/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 14-Mar-2022
 *  FILE NAME  		: 	 Q2_SortOfStringsUsingCollection.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    12:07:07 am
 */
package javainterviewquestionspractice;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class SortOfStringsUsingCollection {
	public static void main(String[] args) {
		//2- Write code to sort the list of strings using Java collection?
		firstWay();
		System.out.println("**********");
		secondWay();
	}
	public static void firstWay() {
	Set<String> str=new TreeSet<>();
		
		str.add("Amit");
		str.add("Umesh");
		str.add("Ritesh");
		str.add("Bablu");
		str.add("Anuj");
		str.add("Arun");
		//String s[]= {"Amit","Suresh","Arun"};
		//Arrays.sort(s);
		for(String ss:str) {
			System.out.println(ss);
		}
	}
	
	public static void secondWay() {
		String listOfStrings[]= {"Arun","Sunil","Nitin","Kabir","Amitabh","Abhishek","Ajay","Akas","Abhilash"};
		Arrays.sort(listOfStrings);
		for(String sortedList:listOfStrings) {
			System.out.println(sortedList);
		}
	}

}

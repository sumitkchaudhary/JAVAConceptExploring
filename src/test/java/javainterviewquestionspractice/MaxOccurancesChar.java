package javainterviewquestionspractice;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class MaxOccurancesChar {

	public static void main(String[] args) {
		
    String str="my name is anurag yadav";
    HashMap<Character, Integer> charCount=new HashMap<>();   
    char[] charArr=str.replaceAll("\\s", "").toCharArray();
    
    for(Character c:charArr) {
    	if(charCount.containsKey(c)){
    		charCount.put(c, charCount.get(c)+1);
    	}
    	else {
    		charCount.put(c, 1);
    	}    
    }
    Set<Entry<Character,Integer>> entrySet=charCount.entrySet();
    int maxCount=0;
    char maxChar=0;
    for(Entry<Character, Integer> entry:entrySet) {
    	if(entry.getValue()>maxCount) {
    		maxCount=entry.getValue();
    		maxChar=entry.getKey();
    	}
    }
    
    System.out.println("Input string: "+str);
    System.out.println(maxChar+" is maximum repeated character. Repeated count:"+maxCount);
	}

}

package javainterviewquestionspractice;


import java.util.*;
import java.util.stream.Collectors;

public class FindUniqueStringFromArray {
    public static void main(String[] args) {
        String[] strArray ={"aa","abc","bb","cc","abc","cc","bb"};

        for(int i=0; i<strArray.length-1; i++){
            for(int j=i+1; j<strArray.length; j++){
                if(strArray[i].contains(strArray[j])){
                   System.out.println(strArray[i]);
                }
            }
        }
        System.out.println("Second approach using HashSet");
        HashSet<String> strElements=new HashSet<>();
        for(String str: strArray){
            if(!strElements.add(str)){
               System.out.println(str);
            }
        }

    }
}

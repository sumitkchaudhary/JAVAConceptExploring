/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 10-Mar-2022
 *  FILE NAME  		: 	 IncreaseNumberAfterTwo.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    12:26:15 pm
 */
package javainterviewquestionspractice;
public class IncreaseNumberAfterTwo {
	public static void main(String[] args) {
		for(int i=1;i<=20; i++) {
			System.out.println(i);
			i+=1;
		}
	}

}

package javainterviewquestionspractice;
public class FindVowelConsonantsFromString {

    public static boolean stringContainsVowels(String input){
        return input.matches(".*[aeiou].*");
    
    }   
    
      public static void main(String []args){
       System.out.println(stringContainsVowels("hello"));
       System.out.println(stringContainsVowels("anurag"));
      }
}

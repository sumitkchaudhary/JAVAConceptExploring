/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 11-May-2022
 *  FILE NAME  		: 	 PrintCountingWithoutUsingLoop.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    5:20:44 pm
 */
package javainterviewquestionspractice;
public class PrintCountingWithoutUsingLoop {
	
	public static void main(String[] args) {
		printNum(1, 100);
	
	}
	//Method for increase the number from given start number till the given end number
	public static int printNum(int startNum, int endNum ) {
		//Check if the given start number less then given end number
		if(startNum<=endNum) {
			//printing the start number 
			System.out.println(startNum);
			//increment the start number
			startNum++;
			//call the method 
			printNum(startNum, endNum);
			
		}
		return 0;
	}

}

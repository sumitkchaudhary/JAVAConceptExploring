package javainterviewquestionspractice;
import java.util.Arrays;

public class FindTop2Element{

	public static void main(String[] args) 
	{
		int[] arr={4,25,25,12,11,32,14,6,16};
		
		int max1=0;
		int max2=0;
		int max3=0;
		//for(int i1=0;i1<arr.length;i1++)  //this loop print lowest elemnt from array
		for(int i1:arr) {
			if(max1<i1) {
				max3=max2;
				max2=max1;
				max1=i1;
			} else if(max2<i1) {
				max3=max2;
				max2=i1;
			} else if(max3<i1) {
				max3=i1;
			}
		}
		
		System.out.println("First max "+max1);
		System.out.println("Second Max "+max2);
		System.out.println("Third max "+max3);

	}

}

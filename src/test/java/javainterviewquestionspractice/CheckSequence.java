package javainterviewquestionspractice;

public class CheckSequence {
    /** Interview question
     *  If class has one void, non void, constructor, and block then which will run first when we call on main method.
     */
    public void voidMethod(){
        System.out.println("I'm void method");
    }

    public static String staticMethod(){
        return "I'm static method";
    }
    public CheckSequence(){
        System.out.println("I'm constructor ");
    }
    static {
        System.out.println("I'm static block");
    }

    public static void main(String[] args) {
        CheckSequence obj=new CheckSequence();
        obj.voidMethod();
        System.out.println(CheckSequence.staticMethod());
    }
}

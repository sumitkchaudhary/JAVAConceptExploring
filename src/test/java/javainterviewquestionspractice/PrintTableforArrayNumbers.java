/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 02-Feb-2022
 *  FILE NAME  		: 	 PrintTableforArrayNumbers.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    12:44:59 am
 */
package javainterviewquestionspractice;

import java.util.Scanner;

public class PrintTableforArrayNumbers {
	
	public static void main(String[] args) {
		
		int numOfArray[]= {2,3,4,5,6,7,8,9,10};
		
		
		for(int expectedNum :numOfArray) {
			if(expectedNum>0) {
				for(int i=1; i<=10; i++) {
					
					int calculatedNum=expectedNum*i;
					System.out.println(expectedNum+" X "+i+"= "+calculatedNum);
				}
			}
			System.out.println("------------------------------------\n");
		}
		
		
		
	}

}

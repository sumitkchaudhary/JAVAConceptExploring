package javainterviewquestionspractice;
import java.util.*;

public class ListRetainAll {

	public static void main(String[] args) {
		List<String> list=new ArrayList<String>();
		 list.add("Mohan");
	     list.add("Rahul");
	     list.add("Bengal");
	     list.add("Kanpur");
	     list.add("Lohit");
	     list.add("Anurag");
	     
		 List<String> list2=new ArrayList<String>();
		 list2.add("Lohit");
	     list2.add("Anurag");
	     list.retainAll(list2);
	     
	     Iterator itr=list.iterator();
	     
	     while(itr.hasNext()) {
	    	 System.out.println(itr.next());
	     }
	}
	

}

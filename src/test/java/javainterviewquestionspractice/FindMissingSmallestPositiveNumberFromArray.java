package javainterviewquestionspractice;

public class FindMissingSmallestPositiveNumberFromArray {
    public static void main(String[] args) {
        int [] arrayInt={-1,-2,1,4,3,5,8,9,7,9};
        int missingNumber=1;
        for(int i=0; i<arrayInt.length; i++){
            if(arrayInt[i]==missingNumber){
                missingNumber++;
            }
        }
        System.out.println(missingNumber);
    }


}

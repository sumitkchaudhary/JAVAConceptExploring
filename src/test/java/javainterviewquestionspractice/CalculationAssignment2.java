package javainterviewquestionspractice;

public class CalculationAssignment2 {
	
		public static int substraction(int a, int b){
			return a-b;
		}
		public static int sum(int a, int b){
			return a+b;
		}
		public static int divition(int a, int b){
			return a/b;
		}
		public static int multiplication(int a, int b) {
			return a*b;
		}
		public static void main(String[] args){
			
			
			int sub_Result=CalculationAssignment2.substraction(10, 2);
			int sum_Result=CalculationAssignment2.sum(sub_Result, 2);
			int sum_Result2=CalculationAssignment2.sum(sum_Result, 2);// call the sum method second time 
			int div_Result=CalculationAssignment2.divition(sum_Result2, 2);
			int grandTotal=CalculationAssignment2.multiplication(div_Result, 2);
			
			System.out.println("The total sum of (((((10-2)+2)+2)/2)*2) : "+grandTotal);
			//(((((10-2)+2)+2)/2)*2)
			/* notes 
			we can't write any statement after return, for example try to write syso after return in a method it will give a error, 
			because after returning control will go to the method which has called it. For example there is a method sum which is returning 
			a result, from main method we have called the sum method and in sum method we are returning the result so after return statement
			control will go to the main method as we are calling from the main method so no statement will work after return in a method.*/	
			
		}

}

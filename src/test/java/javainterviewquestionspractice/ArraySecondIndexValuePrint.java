/**
 * @author 			:	 dell
 *	DATE       		:	 17-Jan-2022
 *  FILE NAME  		: 	 ArraySecondIndexValuePrint.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:49:37 pm
 */
package javainterviewquestionspractice;
public class ArraySecondIndexValuePrint {
	
	public static void main(String[] args) {
		      
		int [][]array =new int [3][3];
		
		array[0] = new int[]{1, 2, 3};
		array[1] = new int[]{4, 5, 6};
		array[2] = new int[]{7, 8, 9};
		for(int i=0; i<3; i++){
			System.out.print(array[i][1]); //prints 258
		}
	}
	
}

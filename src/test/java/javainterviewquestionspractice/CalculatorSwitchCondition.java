package javainterviewquestionspractice;

import java.util.Scanner;

public class CalculatorSwitchCondition {
	public static void main(String[] args) {
		System.out.println("Enter two Values to perform action");	
		Scanner sc = new Scanner(System.in);
		int a= sc.nextInt();
		int b= sc.nextInt();
		System.out.println("Choose the action which you want perform");
		System.out.println("1. Add\n2. Substraction\n3. Multiplication\n4. division\n5. Modulus");
		int calculateValue = sc.nextInt();
		switch (calculateValue) {
		case 1:
			System.out.println("You have choose Addition and Value is : "+(a+b));
			break;
		case 2:
			System.out.println("You have choose Substraction and Value is : "+(a-b));
			break;
		case 3:
			System.out.println("You have choose Multiplication and Value is : "+(a*b));
			break;
		case 4:
			System.out.println("You have choose Division and Value is : "+(a/b));
			break;
		case 5:
			System.out.println("You have choose Modulus and value is : "+(a%b));

		default:
			System.out.println("You choose wrong option please select from given");
			break;
		}
		sc.close();
		
	}
}

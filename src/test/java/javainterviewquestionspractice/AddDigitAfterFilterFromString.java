package javainterviewquestionspractice;
public class AddDigitAfterFilterFromString {
//Get the sum digits from string
	public static void main(String[] args) {
		String str="This#string%contains5^special5*char5acters&5.";
		StringBuilder temp= new StringBuilder("0");
		int sum=0;
		
		for(char ch: str.toCharArray()) {
			if(Character.isDigit(ch)) {
				temp.append(ch);
			}else {
				sum+=Integer.parseInt(temp.toString());
				temp = new StringBuilder("0");
			}
			
		}
		System.out.println("Sum of digits: "+sum);
	}

}

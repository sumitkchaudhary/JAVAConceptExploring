/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 18-Nov-2019
 *  FILE NAME  		: 	 SecondHighestSalariedEmploye.java
 *  PROJECT NAME 	:	 BasicDatabaseProject
 * 	Class Time		:    8:40:47 pm
 */
package databasework.GetData;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import com.Database.ConnectionBase.DataBaseConnectionCreditials;
import com.Database.SqlQuaryisPropertiesUtilies.SqlQauryLaodMehods;

public class SecondHighestSalariedEmploye 
{
	public static void main(String[] args) throws IOException 
	{
		Properties getQueryfromfile = SqlQauryLaodMehods.getSqlQueryFile();
		
		try {
			Connection connect = DataBaseConnectionCreditials.connectionCreditials();
			
			Statement stmt = connect.createStatement();
			ResultSet restSt = stmt.executeQuery(getQueryfromfile.getProperty("secondHighestSalary"));
			while(restSt.next())
			{
				System.out.println("Employee First Name		\t:\t"+restSt.getString("first_name"));
                System.out.println("Employee Joining		\t:\t"+restSt.getInt("salary"));
			}
			connect.close();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	} 

}

/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 07-Oct-2019
 *  FILE NAME  		: 	 GetEmployeeData.java
 *  PROJECT NAME 	:	 BasicDatabaseProject
 * 	Class Time		:    8:29:51 pm
 */
package databasework.GetData;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import com.Database.ConnectionBase.DataBaseConnectionCreditials;
import com.Database.SqlQuaryisPropertiesUtilies.SqlQauryLaodMehods;

public class GetEmployeeData 
{

	public static void main(String[] args) throws SQLException, IOException
	{
		Properties getSqlQuery=SqlQauryLaodMehods.getSqlQueryFile();
		{
	        try {
	            Connection con= DataBaseConnectionCreditials.connectionCreditials();
	           
	            Statement stmt=con.createStatement();
	            ResultSet rs=stmt.executeQuery(getSqlQuery.getProperty("getAllEmpData"));
	            while(rs.next())
	            {
	                System.out.println("Employee id			\t:\t"+rs.getInt("employee_id"));
	                System.out.println("Employee First Name		\t:\t"+rs.getString("first_name"));
	                System.out.println("Employee Last Name		\t:\t"+rs.getString("last_name"));
	                System.out.println("Employee email			\t:\t"+rs.getString("email"));
	                System.out.println("Employee Phone Number			\t:\t"+rs.getString("phone_number"));
	                System.out.println("Employee salary		\t:\t"+rs.getString("salary"));
	                System.out.println("-----------------------------");
	                   
	            }
	            con.close();
	            
	        } catch (Exception e) 
	        {
	            e.printStackTrace();
	        }
	    }
  
        
	}

}

/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 07-Oct-2019
 *  FILE NAME  		: 	 DataBaseConnectionCreditials.java
 *  PROJECT NAME 	:	 BasicDatabaseProject
 * 	Class Time		:    8:28:42 pm
 */
package databasework.ConnectionBase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseConnectionCreditials 
{
	public static Connection connectionCreditials() throws SQLException, ClassNotFoundException 
	{  
		String url = "jdbc:mysql://sql6.freesqldatabase.com/sql6465225";
		String userName ="sql6465225";
		String passCode = "B3ks3B7tyY";
	//	String className= "com.mysql.cj.jdbc.Driver";
		
		Connection conn=DriverManager.getConnection(url, userName, passCode);
		return conn;
		
	}

}

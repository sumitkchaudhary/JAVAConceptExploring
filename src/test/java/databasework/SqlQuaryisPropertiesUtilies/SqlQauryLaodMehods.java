/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 12-Oct-2019
 *  FILE NAME  		: 	 SqlQauryLaodMehods.java
 *  PROJECT NAME 	:	 BasicDatabaseProject
 * 	Class Time		:    7:22:24 pm
 */
package databasework.SqlQuaryisPropertiesUtilies;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class SqlQauryLaodMehods 
{
	public static Properties getSqlQueryPropertiesFile;
	
	public static Properties getSqlQueryFile() throws IOException 
	{
		
		FileInputStream getSQLFile = new FileInputStream(new File(System.getProperty("user.dir")+File.separator+"SQL_Queries.properties"));
		getSqlQueryPropertiesFile = new Properties();
		
		getSqlQueryPropertiesFile.load(getSQLFile);
		
		return getSqlQueryPropertiesFile;
		
	}
	
}

package HackerRangeQuestionSolution;
import javainterviewquestionspractice.CountDigitOfInteger;

import java.util.*;
public class UseOfPrintF {
    /**
     * Java's System.out.printf function can be used to print formatted output. The purpose of this exercise is to test your understanding of formatting output using printf.
     * <p>
     * Input Format
     * <p>
     * Every line of input will contain a String followed by an integer.
     * Each String will have a maximum of 10 alphabetic characters, and each integer will be in
     * the inclusive range from to 0 to 999
     * <p>
     * Output Format
     * <p>
     * In each line of output, there should be two columns:
     * The first column contains the String and is left justified using exactly
     * characters.
     * The second column contains the integer, expressed in exactly digits; if the original input has less than three digits, you must pad your output's leading digits with zeroes.
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("================================");
        for (int i = 0; i < 3; i++) {
            String s= sc.next();
            int num= sc.nextInt();
            System.out.printf("%-15s",s);

            if(getIntegerValueLength(num)<=2){
                System.out.printf("%03d%n",num);
            }else{
                System.out.printf("%1.3s%n",num);
            }
        }
        System.out.println("================================");

    }
    public static int getIntegerValueLength(int num){
        int lenghtOfNum=0;
        while(num!=0){
            num/=10;
            lenghtOfNum++;
        }
        return lenghtOfNum;
    }
}

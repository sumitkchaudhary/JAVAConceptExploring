package HackerRangeQuestionSolution;

import java.util.Scanner;
public class PrintIntDoubleString {
    public static void main(String[] args) {
        /**
         * This is a common problem, and it happens because the nextInt method doesn't read the newline character of your input, so when you issue the command nextLine, the Scanner finds the newline character and gives you that as a line.
         */

        Scanner sc=new Scanner(System.in);
        int i=sc.nextInt();
        double d=sc.nextDouble();
        sc.nextLine(); // skip the newline character
        String s=sc.nextLine();

        sc.close();
        System.out.println("Int: "+i);
        System.out.println("Double: "+d);
        System.out.println("String: "+s);
    }
}

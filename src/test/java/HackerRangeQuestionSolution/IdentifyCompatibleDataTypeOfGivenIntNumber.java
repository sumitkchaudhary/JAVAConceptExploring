package HackerRangeQuestionSolution;

import java.util.Scanner;

public class IdentifyCompatibleDataTypeOfGivenIntNumber {
    /**
     * <p>
     * byte: The byte data type is an 8-bit signed two's complement integer. It has a minimum value of -128 and a maximum value of 127 (inclusive).
     * </p><p>
     * short: The short data type is a 16-bit signed two's complement integer. It has a minimum value of -32,768 and a maximum value of 32,767 (inclusive).
     *</p><p>
     * int: The int data type is a 32-bit signed two's complement integer. It has a minimum value of -2,147,483,648 and a maximum value of 2,147,483,647 (inclusive).
     * </p><p>
     * long: The long data type is a 64-bit signed two's complement integer. It has a minimum value of -9,223,372,036,854,775,808 and a maximum value of 9,223,372,036,854,775,807 (inclusive).
     * </p>
     *
     * Solution Explanation
     * Multiple if statements are useful when you want to independently check multiple conditions and execute
     * corresponding blocks of code for each true condition. if-else statements are useful when you have two
     * mutually exclusive conditions and want to execute different blocks of code based on whether the condition is true or false
     */

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        for(int i=0; i<=t; i++){
            /*
            try{
                long x=sc.nextLong();
                System.out.println(x+"can be fitted in:");
                if(x>=-128 && x<=127){
                    System.out.println("*byte");
                }else if(x>=-32768 && x<=32767 ){
                    System.out.println("*short");
                }else if(x>=-2147483648 && x<=247483647){
                    System.out.println("*int");
                }else if(x>=-9223372036854775808L && x<= 9223372036854775807L) {
                    System.out.println("*long");
                }
            }catch (Exception e){
                System.out.println(sc.next()+"can't be fitted anywhere.");
            }*/
            try{
                long x=sc.nextLong();
                System.out.println(x+" can be fitted in:");
                if(x>=-128 && x<=127)
                    System.out.println("*byte");
                if(x>=Short.MIN_VALUE && x<=Short.MAX_VALUE )
                    System.out.println("*short");
                if(x>=Integer.MIN_VALUE && x<=Integer.MAX_VALUE)
                    System.out.println("*int");
                if(x>=Long.MIN_VALUE && x<= Long.MAX_VALUE)
                    System.out.println("*long");

            }catch (Exception e){
                System.out.println(sc.next()+"can't be fitted anywhere.");
            }
        }
    }

}

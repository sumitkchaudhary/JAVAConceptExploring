package HackerRangeQuestionSolution;

import java.util.HashSet;

public class IdentifyOccupiedRoom {

    /**
     * Suppose you have an array
     * ["+4e", "+1a", "+3b", "-1b"]
     * + means occupied
     * 4/1/3 means floor of the building
     * a-z means room number
     *
     * Now find out the number of vacant room and occupied room
     */

    public static void main(String[] args) {
        String hotelRooms[] ={"+4e", "+1a", "+3b", "-1a"};
        firstSolutionforAvalabityOfRoom(hotelRooms);
        secondSolutionforAvailablityOfRoom(hotelRooms);
    }
    public static void firstSolutionforAvalabityOfRoom(String hotelRooms[]){
        //countOffupiedRoom variable will store the occupied room
        int countOccupiedRoom=0;
        //countVacantRoom variable will store the available room
        int countVacantRoom=0;
        //iterate the hotelRooms array to fetch all the rooms
        for(String room: hotelRooms){
            //Check the availablity here + means occupied so check occupied room
            if(room.charAt(0)=='+'){
                //increase the occupied count
                countOccupiedRoom++;
            }else {
                //increase the vacent count
                countVacantRoom++;
            }
        }
        //This loop help to identified duplicate room wich have occupied and vacant status here - means vacant.
        for (int i = 0; i < hotelRooms.length; i++) {//get all the rooms from 0th index
            for (int j = i + 1; j < hotelRooms.length; j++) {//get all the room from 1st index
                //compare the both array value to verify the duplicay of rrom
                if (hotelRooms[i].substring(1).equals(hotelRooms[j].substring(1))) {
                    //after that we identify here duplicated room should have both status.
                    if (hotelRooms[i].charAt(0) == '+' && hotelRooms[j].charAt(0) == '-') {
                        countOccupiedRoom--;//decrease the count of occupied
                        countVacantRoom--;//decrease the count of vacant
                    }
                }
            }
        }
        System.out.println("Occupied Room is: \t  "+countOccupiedRoom);
        System.out.println("Vacant Room is: \t "+countVacantRoom);

    }

    public static void secondSolutionforAvailablityOfRoom(String hotelRooms[]){
            HashSet<String> occupiedRooms = new HashSet<>();
            HashSet<String> vacantRooms = new HashSet<>();
            //iterate the array to get all the rooms one by one
            for (String room : hotelRooms) {
                //get the room without status here + means occupied and - means vacant
                String roomNumber = room.substring(1);
                //check the availabity status of the room
                if (room.charAt(0) == '+') {
                    //here checking is vacant room already available or not with
                    if (vacantRooms.contains(roomNumber)) {
                        vacantRooms.remove(roomNumber);//if yes then remove
                    } else {
                        occupiedRooms.add(roomNumber);//if not then add
                    }
                } else {
                    if (occupiedRooms.contains(roomNumber)) {//here checking is occupied room available or not
                        occupiedRooms.remove(roomNumber);//if yes then remove
                    } else {
                        vacantRooms.add(roomNumber);//else add
                    }
                }
            }

            int countOccupiedRoom = occupiedRooms.size();
            int countVacantRoom = vacantRooms.size();

            System.out.println("Occupied Room is: \t" + countOccupiedRoom);
            System.out.println("Vacant Room is: \t" + countVacantRoom);

    }


}

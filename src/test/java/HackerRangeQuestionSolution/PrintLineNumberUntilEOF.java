package HackerRangeQuestionSolution;

import com.ryinsightlearninghub.usefulcontroller.GetFileAbsolutepathByName;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class PrintLineNumberUntilEOF {
    public static void main(String[] args) throws FileNotFoundException {
        // We can read file using scanner class.
        //  Scanner sc=new Scanner(new File(GetFileAbsolutepathByName.getAbsolutePath("Filehandle.txt")));
       Scanner sc=new Scanner(System.in);
        int i=1;
        while(sc.hasNext()){
            System.out.println(i+" "+sc.nextLine());
            i++;
        }
        sc.close();

    }
}

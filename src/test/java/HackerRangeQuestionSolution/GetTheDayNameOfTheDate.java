package HackerRangeQuestionSolution;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class GetTheDayNameOfTheDate {

    public static String findDay(int month, int day, int year) {
        Calendar calendar = Calendar.getInstance();
        String dayOfTheDate="";
        try {
            if(day>=1 && day<=31 && month>=1 && month <=12 && year>=0001 && year<=9999) {

                String inputDateStr = String.format("%s/%s/%s", day, month, year);
                Date inputDate = new SimpleDateFormat("dd/MM/yyyy").parse(inputDateStr);
                calendar.setTime(inputDate);
                dayOfTheDate= calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US).toUpperCase();
            }
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        return dayOfTheDate;
    }
    public static void main(String [] args){
        Scanner sc=new Scanner(System.in);
        int day=sc.nextInt();
        int month=sc.nextInt();
        int year=sc.nextInt();
        String dayOfTheDate=findDay(month, day, year);
        System.out.println(dayOfTheDate);

    }


}

-- show tables;
select * from countries;
select * from departments;
select * from dependents;
select * from employees;
select * from jobs;
select * from locations;
select * from regions;

--Changes
-- get employees data order by first name in accending order
select * from employees order by first_name ;

-- Get employe name who have highest salary
select emp.first_name, max(emp.salary) from employees emp;  

-- get employee name who have second highest salary
select emp.first_name, max(emp.salary) from employees emp where emp.salary not in (select max(salary) from employees);

-- get employee name and salary who have salary in between 3000 to 5000
select emp.first_name, emp.salary from employees emp where emp.salary between 3000 and 5000;
select emp.first_name, emp.salary from employees emp where emp.salary >3000 and emp.salary<5000;
select emp.first_name, emp.salary from employees emp where emp.salary >=3000 and emp.salary<=5000;

select emp.first_name from employees emp where emp.first_name like '%a';
select emp.first_name from employees emp where emp.first_name like 'a%';
select emp.first_name from employees emp where emp.first_name like '%a%';
select emp.first_name from employees emp where emp.first_name like '%a_';
select emp.first_name from employees emp where emp.first_name like '%_a';

-- Get the employee detail whos hiring date between 1997-01-01 to 2000-01-01 
select emp.first_name, emp.hire_date from employees emp where emp.hire_date>'1997-01-01' and emp.hire_date<'2000-01-01' order by emp.hire_date;

-- select * from employees e right join dependents d on e.employee_id =d.employee_id where e.employee_id =; 

/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-Feb-2022
 *  FILE NAME  		: 	 Multilevel_SubClassA.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    3:45:32 pm
 */
package com.ryinsightlearninghub.oops.Inheritance.MultilevelInheritance;
public class Multilevel_SubClassA extends Multilevel_ParentClass{
	
	public static int substraction(int a, int b) {
		return a-b;
	}
}

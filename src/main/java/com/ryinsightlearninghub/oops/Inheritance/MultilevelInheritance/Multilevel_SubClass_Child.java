/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-Feb-2022
 *  FILE NAME  		: 	 Multilevel_SubClass_Child.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    3:46:42 pm
 */
package com.ryinsightlearninghub.oops.Inheritance.MultilevelInheritance;
public class Multilevel_SubClass_Child extends Multilevel_SubClassA{
	
	public static void main(String[] args) {
		System.out.println("Addition total is "+additionMethod(50, 60));
		
		System.out.println("Subsctraction total is "+substraction(60, 50));
	}
}

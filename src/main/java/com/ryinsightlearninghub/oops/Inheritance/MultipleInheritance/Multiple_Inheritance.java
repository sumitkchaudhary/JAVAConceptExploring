/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-Feb-2022
 *  FILE NAME  		: 	 Multiple_Inheritance.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:12:10 pm
 */
package com.ryinsightlearninghub.oops.Inheritance.MultipleInheritance;


interface Backend {
	public void connectServer();
}

class FrontEnd{
	public void responsive(String str) {
		System.out.println(str+" can also be used  FrontEnd");
	}
}


public class Multiple_Inheritance extends FrontEnd implements Backend{
	 String language= "java";
	@Override
	public void connectServer() {
		System.out.println(language+" can be used as backend language");
	}
	
	public static void main(String[] args) {
		Multiple_Inheritance obj=new Multiple_Inheritance();
		
		obj.connectServer();
		obj.responsive(obj.language);

		
	}
	

}

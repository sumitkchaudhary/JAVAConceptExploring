/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-Feb-2022
 *  FILE NAME  		: 	 Multiple_Parent_Father.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:57:56 pm
 */
package com.ryinsightlearninghub.oops.Inheritance.MultipleInheritance;
interface Multiple_Parent_Father {
	
	public void fatherMethod(String s);
	//default void fatherMethod(); //default keyword is not allowed from jdk 1.8 
	
	

}

/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-Feb-2022
 *  FILE NAME  		: 	 Multiple__Parents_Mother.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:57:28 pm
 */
package com.ryinsightlearninghub.oops.Inheritance.MultipleInheritance;
interface Multiple__Parents_Mother {
	public void motherMessage();
}

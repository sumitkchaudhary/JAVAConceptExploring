/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-Feb-2022
 *  FILE NAME  		: 	 Multiple_Child.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:02:49 pm
 */
package com.ryinsightlearninghub.oops.Inheritance.MultipleInheritance;
public class Multiple_Child implements Multiple__Parents_Mother, Multiple_Parent_Father{

	@Override
	public void fatherMethod(String s) {
		System.out.println("I'm father class method "+s);
		
	}

	@Override
	public void motherMessage() {
		System.out.println("I'm mother class method");
		
	}
	
	public static void main(String[] args) {
		Multiple_Child obj=new Multiple_Child();
		
		obj.fatherMethod("Sumit");
		obj.motherMessage();
	}

}

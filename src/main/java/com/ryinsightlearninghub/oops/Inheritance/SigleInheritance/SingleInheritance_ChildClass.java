/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-Feb-2022
 *  FILE NAME  		: 	 SigleInheritance_ParentClass.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    3:20:20 pm
 */
package com.ryinsightlearninghub.oops.Inheritance.SigleInheritance;
public class SingleInheritance_ChildClass extends SingleInheritance_ParentClass{
	
	public static void main(String[] args) {
		displayNonReturnMessage();
		System.out.println(displayReturnMessage());
		
		SingleInheritance_ChildClass obj=new SingleInheritance_ChildClass();
		obj.displayMessage();
	}

}

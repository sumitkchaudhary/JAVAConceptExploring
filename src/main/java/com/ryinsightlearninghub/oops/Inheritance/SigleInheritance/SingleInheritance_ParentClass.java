/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-Feb-2022
 *  FILE NAME  		: 	 SingleInheritance_ParentClass.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    3:11:53 pm
 */
package com.ryinsightlearninghub.oops.Inheritance.SigleInheritance;
public class SingleInheritance_ParentClass {
	
	public void displayMessage() {
		System.out.println("I'm Parent class method");
	}
	public static String displayReturnMessage() {
		return "I'm Parent return type method";
	}
	public static void displayNonReturnMessage() {
		System.out.println("I'm not return type parent method");
	}

}

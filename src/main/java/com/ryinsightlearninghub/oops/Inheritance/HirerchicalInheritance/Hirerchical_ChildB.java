/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-Feb-2022
 *  FILE NAME  		: 	 Hirerchical_ChildB.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    3:53:14 pm
 */
package com.ryinsightlearninghub.oops.Inheritance.HirerchicalInheritance;
public class Hirerchical_ChildB extends Hirerchical_Parents {
	
	public static void main(String[] args) {
		System.out.println(parentMessage("I'm child B class and using parent method"));
	}

}

/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-Feb-2022
 *  FILE NAME  		: 	 Hirerchical_Parents.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    3:49:22 pm
 */
package com.ryinsightlearninghub.oops.Inheritance.HirerchicalInheritance;
public class Hirerchical_Parents {
	
	public static String parentMessage(String msg) {
		return "I'm Parent Class Method and child message is : "+msg;
	}

}

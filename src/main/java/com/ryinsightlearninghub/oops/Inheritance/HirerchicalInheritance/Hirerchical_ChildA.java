/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-Feb-2022
 *  FILE NAME  		: 	 Hirerchical_ChildA.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    3:50:18 pm
 */
package com.ryinsightlearninghub.oops.Inheritance.HirerchicalInheritance;
public class Hirerchical_ChildA extends Hirerchical_Parents{
	
	public static void main(String[] args) {
		System.out.println(parentMessage("I'm a child class B and inherit the method in it."));
	}

}

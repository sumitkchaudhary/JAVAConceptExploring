/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 25-Feb-2022
 *  FILE NAME  		: 	 MethodOverriding.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    9:11:00 pm
 */
package com.ryinsightlearninghub.oops.Inheritance.methodOverriding;

class Animal{
	public void displayInfo() {
		System.out.println("I'm an animal");
	}
	public static int animalAge(int age) {
		System.out.println("I'm parents static method And my age is "+age);
		return age;
	}
	final void finalMethod() {
		System.out.println("I'm super class final message");
	}
	protected void protectedParentMethod() {
		System.out.println("I'm protected parent class method");
	}
	
}

//The MethodOverriding class extends the animal class which means MethodOverring is child and animal is parent
public class MethodOverriding extends Animal{
	@Override
	public void displayInfo() {
		super.displayInfo();//if we want to use the super class method after override
		super.finalMethod();
		super.animalAge(50);
		System.out.println("I'm dog");
	}
	/*
	@Override
	public static int animalAge(int age) { //We cannot override the static method if want to do so remove the @override annotation
		System.out.println("And my age is"+age);
		return age;
	}*/
	public static int animalAge(int age) { //We cannot override the static method if want to do so remove the @override annotation
		//super.animalAge(50); //we can not call the super class static method by use super keyword on same overidded method
		System.out.println("And my age is "+age);
		return age;
	}
	
	/*
	private void protectedParentMethod() { //Cannot reduce the visibility of the inherited method from WildAnimal
		System.out.println("I'm child private method");
	}*/
	@Override
	protected void protectedParentMethod() {
		super.protectedParentMethod();
		System.out.println("I'm child class protected method after override with same access modifier");
	}
	private void protectedParentMethod(int i) { //The method protectedParentMethod(int) from the type MethodOverriding is never used locally
		System.out.println("I'm child class protected method after override with different access modifier "+i);
	}
	
	
	/* @Override annotation is the metadata that we use to provide information to the compiler that the method after this annotation override 
	 * the method of superclass. It is not mandatory to use the annotation when we use this the method should follow all the rules of overrides.
	 * Other wise the compiler will generate an error. 
	 * 
	 * Override Rules 
	 *  # Both the super class and sublass must have the same method name, the same return type and the same parameter list. 
	 *  # We can't override the method declared as final and static 
	 *  # We should always override abstract method of the superclass.
	 *  # We can use those access specifiers/ modifier in subclass that provide larger access than the access modifier of the superclass
	 *    For example:	If superclass method is declared protected , then the same method in subclass can be public or protected, but not private
	 * Question: Can we access the method of the superclass after overriding? 
	 * Answer :	 Yes, for do so we need to use super keyword
	 * */
	
/*
	final void finalMethod() { //We Cannot override the final method from WildAnimal
		System.out.println("I'm sub class final method");
	}*/

	public static void main(String[] args) {
		
		MethodOverriding obj=new MethodOverriding();
		obj.displayInfo();
		MethodOverriding.animalAge(40);
		
		obj.protectedParentMethod();
		obj.protectedParentMethod(10);
		
	}
}

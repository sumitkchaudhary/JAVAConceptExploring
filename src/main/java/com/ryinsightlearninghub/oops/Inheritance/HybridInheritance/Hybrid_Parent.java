/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-Feb-2022
 *  FILE NAME  		: 	 Hybrid_Parent.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    7:31:18 pm
 */
package com.ryinsightlearninghub.oops.Inheritance.HybridInheritance;
public class Hybrid_Parent {
	
	public static String parentMessage(String msg) {
		return "I'm HybirdInheritance Parent class message"+msg;
	}

}

/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-Feb-2022
 *  FILE NAME  		: 	 Hybrid_ChildA.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    7:39:10 pm
 */
package com.ryinsightlearninghub.oops.Inheritance.HybridInheritance;
public class Hybrid_ChildA extends Hybrid_Parent{
	
	public static String childA(String msg) {
		return "I'm Child A method"+msg;
	}

}

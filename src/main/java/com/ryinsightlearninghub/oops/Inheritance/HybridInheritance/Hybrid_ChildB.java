/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-Feb-2022
 *  FILE NAME  		: 	 Hybrid_ChildB.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    7:39:22 pm
 */
package com.ryinsightlearninghub.oops.Inheritance.HybridInheritance;
public class Hybrid_ChildB extends Hybrid_ChildA{
	
	public static String childBMessage(String msg) {
		return "I'm child C Method"+msg;
	}

}

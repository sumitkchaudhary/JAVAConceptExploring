/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-Feb-2022
 *  FILE NAME  		: 	 Hybird_ChildC.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    7:39:39 pm
 */
package com.ryinsightlearninghub.oops.Inheritance.HybridInheritance;
public class Hybrid_ChildC extends Hybrid_ChildB {
	
	public static String displayMessageChildC(String msg) {
		return "I'm method of child c "+msg;
	}
	
	public static void main(String[] args) {
		
		System.out.println(displayMessageChildC("Child c Message"));
		System.out.println(parentMessage("Child C message"));
		System.out.println(childA("message for child A"));
		System.out.println(childBMessage("Message for Child A"));
		
	}

}

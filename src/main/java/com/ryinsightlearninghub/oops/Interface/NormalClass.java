/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 05-Mar-2022
 *  FILE NAME  		: 	 NormalClass.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:50:29 pm
 */
package com.ryinsightlearninghub.oops.Interface;
public class NormalClass implements ParentInterfaceClass, ChildInterfaceClass {

	@Override
	public void childeSubsctractionOFTwoNumber() {
		System.out.println("Subsctraction of two digit 50-10  = "+(50-10));
		
	}

	@Override
	public void parentAddTwoNumber() {
		System.out.println("Addition of two digit 50+10  = "+(50+10));
		
	}

	@Override
	public void parentAddTwoNumber(int a, int b) {
		System.out.println("Addition of two digit "+a+"+"+b+"= "+(a+b));
		
	}

	@Override
	public void display() {
		System.out.println("I'm overrided method which implemented in subclass/child class");
		
	}
	
	public static void main(String[] args) {
		NormalClass obj=new NormalClass();
		obj.childeSubsctractionOFTwoNumber();
		obj.parentAddTwoNumber();
		obj.parentAddTwoNumber(40, 50);
		obj.display();
		
	}

}

/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 05-Mar-2022
 *  FILE NAME  		: 	 ParentInterfaceClass.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:47:45 pm
 */
package com.ryinsightlearninghub.oops.Interface;
public interface ParentInterfaceClass {
	
	public abstract void parentAddTwoNumber();
	
	public abstract void parentAddTwoNumber(int a, int b);
	
	//public static int parentAddTwoNumber(int a, int c, int d);  //In interface class we can't create static/ return type method
	
	public void display();  //In the interface class we can declare method without abstract keyword but without body
	/*
	public void message() {
		in the interface class we can't not create non-abstract/ normal class 
	}*/
	
	/*Advantage of the interface 
	 * 	We can achieve 100% abstraction with the help of interface 
	 * 	we can hide the core implementation/declaration of subclass. And subclass must be implements that method which is define in the interface class
	 * 	we can achieve multiple inheritance with the help of interface
	 * 	we can't create object of interface class
	 * 
	 * */

}

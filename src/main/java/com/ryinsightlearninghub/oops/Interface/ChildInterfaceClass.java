/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 05-Mar-2022
 *  FILE NAME  		: 	 ChildInterfaceClass.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:48:45 pm
 */
package com.ryinsightlearninghub.oops.Interface;
public interface ChildInterfaceClass extends ParentInterfaceClass {
	
	public abstract void childeSubsctractionOFTwoNumber();

}

/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 18-Jul-2019
 *  FILE NAME  		: 	 AbstractClass_Shap.java
 *  PROJECT NAME 	:	 BasicCoceptUnderstading
 * 
 */
package com.ryinsightlearninghub.oops.Absctraction;

abstract class AbstractClass_Shap
{
	String color;
	
	//these are abstract method
	abstract double area();
	
	public abstract String toString();
	
	
	public AbstractClass_Shap(String color)
	{
		System.out.println("AbstractClass_Shap constructor called");
		this.color=color;
		
	}
	
	
	public String getColor()
	{
		return color;
	}
}

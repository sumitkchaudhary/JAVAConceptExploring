/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 18-Jul-2019
 *  FILE NAME  		: 	 AbstractClass_ChildClass_Rectangle.java
 *  PROJECT NAME 	:	 BasicCoceptUnderstading
 * 
 */
package com.ryinsightlearninghub.oops.Absctraction;
class AbstractClass_ChildClass_Rectangle extends AbstractClass_Shap
{
	double length;
	double width;
	
	public AbstractClass_ChildClass_Rectangle (String color, double length, double width)
	{
		super(color);
		System.out.println("AbstractClass_ChildClass_Rectangle cunstructor called");
		this.length=length;
		this.width=width;
	}
	@Override
	double area() {
		return length*width;
	}
	@Override
	public String toString() {
		return "AbstractClass_ChildClass_Rectangle color is" + super.color+"and area is "+area();
	}
}
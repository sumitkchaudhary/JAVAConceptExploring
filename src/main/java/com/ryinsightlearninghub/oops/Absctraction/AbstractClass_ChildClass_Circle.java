/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 18-Jul-2019
 *  FILE NAME  		: 	 AbstractClass_ChildClass_Circle.java
 *  PROJECT NAME 	:	 BasicCoceptUnderstading
 * 
 */
package com.ryinsightlearninghub.oops.Absctraction;
class AbstractClass_ChildClass_Circle extends AbstractClass_Shap
{
	double redius; 
	
	public AbstractClass_ChildClass_Circle(String color, double redius)
	{
		super(color);
		System.out.println("AbstractClass_ChildClass_Circle cunstructor called");
		
		this.redius=redius;
	}

	@Override
	double area() {
		return Math.PI * Math.pow(redius, 2);
	}

	@Override
	public String toString() {
		return "Circule color is "+super.color+"and area is :"+area();
	}
}

/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 18-Jul-2019
 *  FILE NAME  		: 	 AbsctractClass_Initilization.java
 *  PROJECT NAME 	:	 BasicCoceptUnderstading
 * 
 */
package com.ryinsightlearninghub.oops.Absctraction;



public class AbsctractClass_Initilization 
{
		public static void main(String[] args) {
			
			AbstractClass_Shap s1 = new AbstractClass_ChildClass_Circle(" Red ", 2.2);
			
			AbstractClass_Shap s2 = new AbstractClass_ChildClass_Rectangle(" Yello ", 4, 5);
			
			System.out.println(s1.toString());
			System.out.println(s2.toString());
		}
		
	

}

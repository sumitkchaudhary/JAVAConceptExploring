/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 08-Mar-2022
 *  FILE NAME  		: 	 InstanceOfDuringInheritance.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:23:17 pm
 */
package com.ryinsightlearninghub.oops.Keywords;
class Animal {
}

// subclass
class Dog extends Animal {
}

public class InstanceOfDuringInheritance {
	  public static void main(String[] args) {

		    // create an object of the subclass
		    Dog d1 = new Dog();

		    // checks if d1 is an instance of the subclass
		    System.out.println(d1 instanceof Dog);        // prints true

		    // checks if d1 is an instance of the superclass
		    System.out.println(d1 instanceof WildAnimal);     // prints true
		  }

}

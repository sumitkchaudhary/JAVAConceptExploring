/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 08-Mar-2022
 *  FILE NAME  		: 	 InstanceOfInInterface.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:25:12 pm
 */
package com.ryinsightlearninghub.oops.Keywords;
//Java program to check if an object of a class is also
//an instance of the interface implemented by the class
//The instanceof operator is also used to check whether an object of a class is also an instance of the interface implemented by the class. For example,

interface WildAnimal {
}

class WildDog implements WildAnimal {
}
public class InstanceOfInInterface {
	public static void main(String[] args) {
		// create an object of the Dog class
		WildDog d1 = new WildDog();

	    // checks if the object of Dog
	    // is also an instance of Animal
	    System.out.println(d1 instanceof WildAnimal);  // returns true
	  }
}

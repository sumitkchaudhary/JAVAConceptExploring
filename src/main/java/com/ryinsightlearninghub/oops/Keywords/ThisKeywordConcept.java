package com.ryinsightlearninghub.oops.Keywords;

public class ThisKeywordConcept{
	
	public void method(){
		System.out.println("default method");
	}
	public void method1(int a){
		this.method();
		System.out.println("One perametarised method value"+a);
	}
	public void method2(int a, float b){
		this.method1(100);
		System.out.println("Two perametarised method value "+a+" "+"Float"+" "+b);
	}
	public void method3(String str, int b, float f){
		this.method2( 100, 100.9F);
		
		System.out.println("This three different perametrised "+str+" "+"integer Value "+b+" "+"float Value "+f);
	}
	public static void main(String[] args){
		ThisKeywordConcept refVariable=new ThisKeywordConcept();
		refVariable.method3("Sumit", 90, 80f);		
	}
}

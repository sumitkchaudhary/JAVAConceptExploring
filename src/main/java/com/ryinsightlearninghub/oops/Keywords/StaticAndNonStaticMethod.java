/**
 * @author 			:	 DELL
 *	DATE       		:	 19-Jul-2021
 *  FILE NAME  		: 	 StaticAndNonStaticMethod.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    4:41:43 PM
 */
package com.ryinsightlearninghub.oops.Keywords;
public class StaticAndNonStaticMethod {
	String nonStaticVar="I'am a non static variable";
	static String staticVar= "I'am a static variable";
	public void nonStaticMethod() {
		System.out.println("I'm non static default method");
		System.out.println(nonStaticVar);
		System.out.println(staticVar);
	}
	public void nonStaticMethod(String message) {
		System.out.println(message);
		System.out.println(nonStaticVar);
		System.out.println(staticVar);
	}
	public int dataTypenonStaticMethod() {
		System.out.println(nonStaticVar);
		System.out.println(staticVar);
		int a=0;
		return a;
	}
	public int dataTypenonStaticMethod(int a) {
		System.out.println(nonStaticVar);
		System.out.println(staticVar);
		return a;
	}
	public static String staticWithoutPermatriseMethod() {
		String message="I'am a static without perametrize method";
		//System.out.println(nonStaticVar);
		System.out.println(staticVar);
		return message;
	}
	public static String staticWithPermatriseMethod(String message) {
		//System.out.println(nonStaticVar);
		System.out.println(staticVar);
		return message;
	}	
	
	
	public static void main(String[] args) {	
		StaticAndNonStaticMethod referVariable=new StaticAndNonStaticMethod();
		referVariable.nonStaticMethod();
		referVariable.nonStaticMethod("I'am a non static perametrize method");
		System.out.println(referVariable.dataTypenonStaticMethod());
		System.out.println(referVariable.dataTypenonStaticMethod(123));	
	}
}

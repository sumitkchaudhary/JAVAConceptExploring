/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 08-Mar-2022
 *  FILE NAME  		: 	 InstanceOfOperator.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:21:47 pm
 */
package com.ryinsightlearninghub.oops.Keywords;
public class InstanceOfOperator {
	public static void main(String[] args) {
		/*The instanceof operator in Java is used to check whether an object is an instance of a particular class or not.
		 * 
		 * */
	    // create a variable of string type
	    String name = "Programiz";
	    
	    // checks if name is instance of String
	    boolean result1 = name instanceof String;
	    System.out.println("name is an instance of String: " + result1);

	    // create an object of Main
	    InstanceOfOperator obj = new InstanceOfOperator();

	    // checks if obj is an instance of Main
	    boolean result2 = obj instanceof InstanceOfOperator;
	    System.out.println("obj is an instance of Main: " + result2);
	  }

}

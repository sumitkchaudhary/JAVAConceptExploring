/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 08-Mar-2022
 *  FILE NAME  		: 	 FinalKeyword.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:12:04 pm
 */
package com.ryinsightlearninghub.oops.Keywords;
class ParentFinalKeyword{
	final void parentFinalDisplay() {
		System.out.println("Parent Method");
	}
}
public class FinalKeyword extends ParentFinalKeyword{
	
	final void display() {
		System.out.println("Sumit");
	}
	//we can overload the final method
	final void display(String dynamic) {
		super.parentFinalDisplay();//We can access final method with super keyword
		System.out.println(dynamic);
	}
	/*//We can not override the final method
	final void parentFinalDisplay() {
	
	}*/
	public static void main(String[] args) {
		final int i=30;
		System.out.println(i);
		//i=200;//we cannot change the final variable value 
		
		FinalKeyword obj= new FinalKeyword();
		obj.display();
		obj.display("My message");
	}

}

/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 23-Jul-2022
 *  FILE NAME  		: 	 SuperKeywordConcept.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:11:41 pm
 */
package com.ryinsightlearninghub.oops.Keywords;
public class SuperKeywordConcept extends ThisKeywordConcept{
	
	public void skmethod(){
		super.method3("Kumar", 100, 10F);
		System.out.println("SuperKeywordConcept class default method");
	}
	public void skMethod1(int a){
		this.method();
		System.out.println("SuperKeywordConcept class One perametarised method value"+a);
	}
	public void skMethod2(int a, float b){
		this.method1(100);
		System.out.println("SuperKeywordConcept class Two perametarised method value "+a+" "+"Float"+" "+b);
	}
	public void skMethod3(String str, int b, float f){
		this.method2( 100, 100.9F);
		
		System.out.println("SuperKeywordConcept class This three different perametrised "+str+" "+"integer Value "+b+" "+"float Value "+f);
	}
	public static void main(String[] args){
		SuperKeywordConcept refVariable=new SuperKeywordConcept();
		refVariable.skMethod3("Sumit", 90, 80f);		
	}

}

package com.ryinsightlearninghub.oops.Constructor;

public class ParentsConstructor 
{
	public ParentsConstructor(){
		this(3,3);
		System.out.println("Parent default constructor\n\n");
	}
	public ParentsConstructor(int a){	
		this();
		int one=a;
		System.out.println("Parent 1 perameterized constructor\n\n"+" "+one);
	}
	public ParentsConstructor(int a, int b){
		int two=a+b;
		System.out.println("Parent 2 perameterized constructor\n\n"+""+two);
		
	}
	public ParentsConstructor(int a , int b, int c){
		this(100);
		int three= a+b+c;
		System.out.println("Parent 3 perameterized constructor\n"+""+three);
	}

}

package com.ryinsightlearninghub.oops.Constructor;

public class ChildConstructor extends ParentsConstructor
{
	public ChildConstructor(){
		this(50,50,50);
		System.out.println("This child defult constructor");
	}
	public ChildConstructor(int a){
		super(100,100,100);
		System.out.println("This child 1 perameterized constructor\n\n"+""+a);
	}
	public ChildConstructor(int a, int b){
		this();
		int chTow=a+b;
		System.out.println("This child 2 perameterized constructor\n\n"+""+chTow);
	}
	public ChildConstructor(int a, int b, int c){
		this(100);
		int chThree=a+b+c;
		System.out.println("This child 3 perameterized constructor\n\n"+""+chThree);
	}
	public static void main(String[] args) {
		
		ChildConstructor chilCunst = new ChildConstructor(50,50);
	}
	

}

/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 12-Feb-2022
 *  FILE NAME  		: 	 ProtectedAccessModifierChild.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:21:14 pm
 */
package com.ryinsightlearninghub.oops.AccessModifier.AccessModifierChildPackage;

import com.ryinsightlearninghub.oops.AccessModifier.ProtectedAccessModifier;

public class ProtectedAccessModifierChild extends ProtectedAccessModifier {
	public static void main(String[] args) {
		/*
		
		ProtectedAccessModifier obj=new ProtectedAccessModifier();
		obj.messageDisplayProtectedMethod;
			
		//we can not use the protected method and variable by using object creation of the parent class instead of inherit the parent class
	*/
		
		ProtectedAccessModifierChild obj=new ProtectedAccessModifierChild();
		obj.messageDisplayProtectedMethod(); // But protected modifier define method can we access by create child object and inherit it.
		
		
	
	}
	
	
	

}

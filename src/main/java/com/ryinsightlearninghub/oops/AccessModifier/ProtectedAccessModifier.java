/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 12-Feb-2022
 *  FILE NAME  		: 	 ProtectedAccessModifier.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:14:27 pm
 */
package com.ryinsightlearninghub.oops.AccessModifier;
public class ProtectedAccessModifier {
	
	protected static int protectedVariable;
	
	protected void messageDisplayProtectedMethod() {
		System.out.println("I'm protected, I'm available only same package only. Not be accessable for outside of package as well as sub package "
				+ "by using object creation of the class. But I the child class inherit the class then I'm accessable");
	}
	
	public static void main(String[] args) {
		protectedVariable=10;
		System.out.println(protectedVariable);
		
		ProtectedAccessModifier obj=new ProtectedAccessModifier();
		obj.messageDisplayProtectedMethod();
	}

}

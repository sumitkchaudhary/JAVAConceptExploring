/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 12-Feb-2022
 *  FILE NAME  		: 	 ProtectedAccessModifier.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:14:27 pm
 */
package com.ryinsightlearninghub.oops.AccessModifier;
public class ProtectedAccessModifierChildClass {
	
	
	public static void main(String[] args) {
		int i=ProtectedAccessModifier.protectedVariable=100;
		System.out.println(i);
		
		ProtectedAccessModifier obj=new ProtectedAccessModifier();
		obj.messageDisplayProtectedMethod();
	
	}

}

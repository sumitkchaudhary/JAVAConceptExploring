/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 12-Feb-2022
 *  FILE NAME  		: 	 PrivateAccessModifierChildClass.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:45:37 pm
 */
package com.ryinsightlearninghub.oops.AccessModifier;
public class PrivateAccessModifierChildClass {
	public static void main(String[] args) {
		PrivateAccessModifier obj=new PrivateAccessModifier();
		//obj.display();
		
		//We can not access private method in child class in same package by creating by class object. Warnig is method is not visible to that class. 
		//And we run the class then he get 
		/*
		 * Exception in thread "main" java.lang.Error: Unresolved compilation problem: 
		 * The method display() from the type PrivateAccessModifier is not visible
		 * 	at com.Basic.AllOOPSTopics.AccessModifier.PrivateAccessModifierChildClass.main(PrivateAccessModifierChildClass.java:13)

		 * */
		
	}
	

}

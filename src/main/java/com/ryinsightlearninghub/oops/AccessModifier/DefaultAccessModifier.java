/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 12-Feb-2022
 *  FILE NAME  		: 	 DefaultAccessModifier.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:06:45 pm
 */
package com.ryinsightlearninghub.oops.AccessModifier;
class DefaultAccessModifier {
	
	void messageDisplayDefaultAccessModifierVoidMethod() {
		System.out.println("I'm default access modifier method. Bydefult my access modifier is public and I'm available for outside of package");
	}
	
	static String messageDisplayDefaultAccessModifierStaticMethod() {
		
		return "I'm default access modifier static method. I'm available for out package also.";
	}
	
	static void messageDisplayDefaultAccessModifierVoidStaticMethod() {
		System.out.println("I'm default access modifier static non-return method. I'm available for out package also.");
	}
	public static void main(String[] args) {
		System.out.println(DefaultAccessModifier.messageDisplayDefaultAccessModifierStaticMethod());
		DefaultAccessModifier.messageDisplayDefaultAccessModifierVoidStaticMethod();
		DefaultAccessModifier obj=new DefaultAccessModifier();
		obj.messageDisplayDefaultAccessModifierVoidMethod();
	}

}

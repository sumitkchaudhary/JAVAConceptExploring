/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 12-Feb-2022
 *  FILE NAME  		: 	 PrivateAccessModifierObject.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:45:37 pm
 */
package com.ryinsightlearninghub.oops.AccessModifier;
public class PrivateAccessModifierObject{
	//We can create getter and setter for the private variable and use is another class/ out side of package Refer the below class 
	PrivateAccessModifierObjectChildClass obj; //refer this 
	
	private int rollNumber;
	private String studentName;
	private double studentFee;
	/**
	 * @return the rollNumber
	 */
	public int getRollNumber() {
		return rollNumber;
	}
	/**
	 * @param rollNumber the rollNumber to set
	 */
	public void setRollNumber(int rollNumber) {
		this.rollNumber = rollNumber;
	}
	/**
	 * @return the studentName
	 */
	public String getStudentName() {
		return studentName;
	}
	/**
	 * @param studentName the studentName to set
	 */
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	/**
	 * @return the studentFee
	 */
	public double getStudentFee() {
		return studentFee;
	}
	/**
	 * @param studentFee the studentFee to set
	 */
	public void setStudentFee(double studentFee) {
		this.studentFee = studentFee;
	}
	
	
	

}

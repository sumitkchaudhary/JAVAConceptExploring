/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 12-Feb-2022
 *  FILE NAME  		: 	 PrivateAccessModifierObject.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:45:37 pm
 */
package com.ryinsightlearninghub.oops.AccessModifier;
public class PrivateAccessModifierObjectChildClass{

	public static void main(String[] args) {
		 PrivateAccessModifierObject obj=new PrivateAccessModifierObject();
		 
		 obj.setRollNumber(216543);
		 obj.setStudentName("Sumit Kumar");
		 obj.setStudentFee(200000.50);
		 
		 int rollNum=obj.getRollNumber();
		 String stName=obj.getStudentName();
		 double stFee=obj.getStudentFee();
		 
		 System.out.println(rollNum);
		 System.out.println(stName);
		 System.out.println(stFee);
		 
	}
}

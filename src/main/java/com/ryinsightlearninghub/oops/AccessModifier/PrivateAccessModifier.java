/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 12-Feb-2022
 *  FILE NAME  		: 	 PrivateAccessModifier.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:43:34 pm
 */
package com.ryinsightlearninghub.oops.AccessModifier;
public class PrivateAccessModifier {
	
	private void display() {
		System.out.println("I'm private access modifier method");
	}
	/* We can access private method with in the class
	public static void main(String[] args) {
		PrivateAccessModifier obj=new PrivateAccessModifier();
		obj.display();
		
	}*/
	
	/*We can access private method with in the class. But we can not create getter setter for the private method*/

}

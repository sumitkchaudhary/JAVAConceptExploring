/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 05-Mar-2022
 *  FILE NAME  		: 	 Person.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:33:19 pm
 */
package com.ryinsightlearninghub.oops.Encapsulation;
public class Person {
	private String personName;
	private int mobileNumber;
	private String gender;
	private String address;
	private String personIdentiy;
	
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public int getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(int mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPersonIdentiy() {
		return personIdentiy;
	}
	public void setPersonIdentiy(String personIdentiy) {
		this.personIdentiy = personIdentiy;
	}
	
	

}

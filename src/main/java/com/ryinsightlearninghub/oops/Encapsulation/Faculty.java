/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 05-Mar-2022
 *  FILE NAME  		: 	 Faculty.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:40:09 pm
 */
package com.ryinsightlearninghub.oops.Encapsulation;
public class Faculty {
	public static  Person facultyInformation() {
		Person obj= new Person();
		obj.setPersonName("Amit");
		obj.setAddress("Male");
		obj.setGender("Greater Noida");
		obj.setMobileNumber(987654321);
		obj.setPersonIdentiy("Hindi Teacher");
		return obj;
	}

}

/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 05-Mar-2022
 *  FILE NAME  		: 	 SchoolDirectory.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:41:31 pm
 */
package com.ryinsightlearninghub.oops.Encapsulation;
public class SchoolDirectory {
	
	public static void main(String[] args) {
		System.out.println("Student Information is --");
		System.out.println(Student.studentInformation().getPersonName());
		System.out.println(Student.studentInformation().getGender());
		System.out.println(Student.studentInformation().getMobileNumber());
		System.out.println(Student.studentInformation().getAddress());
		System.out.println(Student.studentInformation().getPersonIdentiy());
		
		
		System.out.println("Faculity Information is --");
		System.out.println(Faculty.facultyInformation().getPersonName());
		System.out.println(Faculty.facultyInformation().getGender());
		System.out.println(Faculty.facultyInformation().getMobileNumber());
		System.out.println(Faculty.facultyInformation().getAddress());
		System.out.println(Faculty.facultyInformation().getPersonIdentiy());
		
		
	}

}

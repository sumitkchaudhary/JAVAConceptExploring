/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 05-Mar-2022
 *  FILE NAME  		: 	 Student.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:35:54 pm
 */
package com.ryinsightlearninghub.oops.Encapsulation;
public class Student {
	
	public static  Person studentInformation() {
		Person obj= new Person();
		obj.setPersonName("Sumit");
		obj.setAddress("Male");
		obj.setGender("Noida");
		obj.setMobileNumber(1234567890);
		obj.setPersonIdentiy("Student");
		return obj;
	}

}

/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 06-Mar-2022
 *  FILE NAME  		: 	 CheckedORComplieTimeException.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    6:51:06 pm
 */
package com.ryinsightlearninghub.exceptionhandling.CheckedException;

import java.io.IOException;

public class CheckedORComplieTimeException {
	
	public void notThrowException(){
		//throw new IOException();  //Here the compiler warning us to use try-cath or throw an exception
	}
	
	public void throwException() throws IOException{
		throw new IOException(); 
		//System.out.println("After line"); //This will show an error because we not handling try catch 
	}
	

}

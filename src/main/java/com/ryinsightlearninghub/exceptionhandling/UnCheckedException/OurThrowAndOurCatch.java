/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 06-Mar-2022
 *  FILE NAME  		: 	 OurThrowAndOurCatch.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    5:21:39 pm
 */
package com.ryinsightlearninghub.exceptionhandling.UnCheckedException;

public class OurThrowAndOurCatch {
	public static void main(String[] args) {
		int balance=5000;
		int withdrawAmount=9000;
		
		//What is difference between runtime and compile time exception
		try {
			if(balance<withdrawAmount)
				throw new ArithmeticException("Incufficient Balance");
				balance=balance-withdrawAmount;
				System.out.println("Account Balance is : "+balance);
				
		}catch(ArithmeticException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("Continue ---");//this line will execute 		
	}

}

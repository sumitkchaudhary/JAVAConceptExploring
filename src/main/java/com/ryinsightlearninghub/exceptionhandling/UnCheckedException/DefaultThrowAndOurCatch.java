/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 06-Mar-2022
 *  FILE NAME  		: 	 DefaultThrowAndOurCatch.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    5:21:05 pm
 */
package com.ryinsightlearninghub.exceptionhandling.UnCheckedException;
public class DefaultThrowAndOurCatch {
	public static void main(String[] args) {
		int x=40;
		int y=0;
		
		try {
				System.out.println("Before exception throw line");
				System.out.println(x/y);//As per java divide by zero is inappropriate behavior so this will throw ArithmeticException
				System.out.println("After exception throw line");
			
		} catch (ArithmeticException e) { //SO we handle with using AirthemticException variable 
			
				System.out.println(e.getMessage());
		}finally {  
			y=4;
			System.out.println("Must run :"+x/y);
			
		}
		
		/* finally block will always run in any situation 
		 * 	=> Case 1 - When exception occurred and handling with exception class object which is declare by own under the catch block then will run
		 * 	Case 2 - When exception occurred and handling with default exception which defined by java then will run
		 * 	Case 3 - When exception not occurred then will run 
		 * */
	}

}

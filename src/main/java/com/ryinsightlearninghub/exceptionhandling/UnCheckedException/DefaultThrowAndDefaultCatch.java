/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 06-Mar-2022
 *  FILE NAME  		: 	 DefaultThrowAndDefaultCatch.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    5:20:45 pm
 */
package com.ryinsightlearninghub.exceptionhandling.UnCheckedException;
public class DefaultThrowAndDefaultCatch {
	
	public static void main(String[] args) {
		String s=null;
		System.out.println(s.length());
		
		/*Exception in thread "main" java.lang.NullPointerException: Cannot invoke "String.length()" because "s" is null
			at com.Basic.ExceptionCencept.UnCheckedException.DefaultThrowAndDefaultCatch.main(DefaultThrowAndDefaultCatch.java:13)
*/
	}

}

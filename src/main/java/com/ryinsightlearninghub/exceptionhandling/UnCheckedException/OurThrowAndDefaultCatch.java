/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 06-Mar-2022
 *  FILE NAME  		: 	 OurThrowAndDefaultCatch.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    5:21:25 pm
 */
package com.ryinsightlearninghub.exceptionhandling.UnCheckedException;


public class OurThrowAndDefaultCatch {
	public static void main(String[] args) {
		int balance=5000;
		int withdrawAmount=13000;
		
		if(balance<withdrawAmount)
			throw new ArithmeticException("You have Insufficient Balance");
			
		balance=balance-withdrawAmount;
		System.out.println("Account Balance is : "+balance);
		
		System.out.println("Continue ---");//This line will not run because except will throw before this 
		
		//Here we are not declaring the try catch but defining own message under the AirthmeticException class constructor. This exception
		//will handle by java with defined message
			
	}

}

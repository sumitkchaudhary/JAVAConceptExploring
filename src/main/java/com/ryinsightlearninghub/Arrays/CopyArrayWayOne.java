/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 08-Mar-2022
 *  FILE NAME  		: 	 CopyArrayWayOne.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:37:00 pm
 */
package com.ryinsightlearninghub.Arrays;
public class CopyArrayWayOne {
	
	public void secondExample() {
		int [] numbers = {1, 2, 3, 4, 5, 6};
        int [] positiveNumbers = numbers;    // copying arrays
      
        // change value of first array
        numbers[0] = -1;

        // printing the second array
        for (int number: positiveNumbers) {
            System.out.print(number + ", ");
        }
    
	}
	public void firstExample() {
	       
        int [] numbers = {1, 2, 3, 4, 5, 6};
        int [] positiveNumbers = numbers;    // copying arrays

        for (int number: positiveNumbers) {
            System.out.print(number + ", ");
        }
    }
	public static void main(String[] args) {
		CopyArrayWayOne obj=new CopyArrayWayOne();
		obj.firstExample();
		
	}

}

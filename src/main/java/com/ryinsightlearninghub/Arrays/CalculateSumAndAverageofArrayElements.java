/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 08-Mar-2022
 *  FILE NAME  		: 	 CalculateSumAndAverageofArrayElements.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:43:33 am
 */
package com.ryinsightlearninghub.Arrays;
public class CalculateSumAndAverageofArrayElements {
	public static void main(String[] args) {
		int[] numbers = {2, -9, 0, 5, 12, -25, 22, 9, 8, 12};
		int sumOfPositiveNumbers=0;
		int sumOfAllNumbers=0;
		double averageOfSum;
		
		int length=numbers.length;
		
		for(int num: numbers) {
			sumOfAllNumbers+=num;
			if(num>0) {
				sumOfPositiveNumbers+=num;
			}
		}

		System.out.println("sum of the positive number from array is :"+sumOfPositiveNumbers);
		
		averageOfSum= (double) sumOfPositiveNumbers /length;
		System.out.println("Average of the positive numbers "+averageOfSum);
		
		System.out.println("sum of the All number from array is :"+sumOfAllNumbers);
		
		averageOfSum= (double) sumOfAllNumbers /length;
		System.out.println("Average of the ALL numbers "+averageOfSum);
		
	}

}

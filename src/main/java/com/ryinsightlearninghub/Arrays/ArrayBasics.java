/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 08-Mar-2022
 *  FILE NAME  		: 	 ArrayBasics.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:31:55 am
 */
package com.ryinsightlearninghub.Arrays;
public class ArrayBasics {
	public static void main(String[] args) {
		//An array is a collection of similar types of data.
		//Declaration
		String []countryNames= {"India","Pakistan","England", "Australia","China"};
		System.out.println("Country name");
		//Initialization
		for(int i=0; i<countryNames.length;i++) {
			System.out.println(countryNames[i]);
		}

		String [] cityOfIndia=new String[5];
		cityOfIndia[0]="Utter Pradesh";
		cityOfIndia[1]="Delhi";
		cityOfIndia[2]="Rajasthan";
		cityOfIndia[3]="Bihar";
		cityOfIndia[4]="Asham";

		System.out.println("City of india");
		for(String city: cityOfIndia) {
			System.out.println(city);
		}

		int [] numbers= {1,2,3,4,5,6,7,-34,0};
		System.out.println("Numbers");
		for(int no: numbers) {
			System.out.println(no);
		}

		double [] money= {10.90,34.56,-12.56,0.50};
		System.out.println("Money");
		for(double mo: money) {
			System.out.println(mo);
		}


	}

}

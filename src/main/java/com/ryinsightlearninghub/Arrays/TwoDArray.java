/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 08-Mar-2022
 *  FILE NAME  		: 	 TwoDArray.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:29:32 pm
 */
package com.ryinsightlearninghub.Arrays;
public class TwoDArray {
	public static void main(String[] args) {

        // create a 2d array
        int[][] a = {
            {1, 2, 3}, 
            {4, 5, 6, 9}, 
            {7}};
      
        // calculate the length of each row
        System.out.println("Length of row 1: " + a[0].length);
        System.out.println("Length of row 2: " + a[1].length);
        System.out.println("Length of row 3: " + a[2].length);
        //For Loop 
        System.out.println("For Loop");
        for (int i = 0; i < a.length; ++i) {
            for(int j = 0; j < a[i].length; ++j) {
                System.out.println(a[i][j]);
            }
        }
        
        System.out.println("For each Loop");

        // first for...each loop access the individual array
        // inside the 2d array
        for (int[] innerArray: a) {
            // second for...each loop access each element inside the row
            for(int data: innerArray) {
                System.out.println(data);
            }
        }
    }

}

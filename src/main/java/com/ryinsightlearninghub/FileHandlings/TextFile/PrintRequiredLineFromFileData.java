/**sumitkumar
 PrintRequiredLineFromFileData.java
 * 06-Apr-2019
 */
package com.ryinsightlearninghub.FileHandlings.TextFile;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

import com.ryinsightlearninghub.usefulcontroller.GetFileAbsolutepathByName;

public class PrintRequiredLineFromFileData {
	public static void main(String [] line) throws IOException{
			File filePath = new File(GetFileAbsolutepathByName.getAbsolutePath("ReadData.txt"));
			if (filePath.exists()){
				FileReader readChar = new FileReader(filePath);
				LineNumberReader readLines = new LineNumberReader(readChar);
				String str = null;
				while((readLines.getLineNumber())!=2){
					str=readLines.readLine();
					System.out.println(str);
				}
				readLines.close();
			}
			else {
					System.out.println("File not exist");
			}	
		//return line;
	}
}

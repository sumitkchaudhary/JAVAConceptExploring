package com.ryinsightlearninghub.FileHandlings.TextFile;

import com.ryinsightlearninghub.usefulcontroller.GetFileAbsolutepathByName;

import java.io.File;
import java.io.IOException;

public class DeleteFile {

    public static void main(String [] args) throws IOException {

        File getFilePath=new File(GetFileAbsolutepathByName.getAbsolutePath("targetDeleteFile.txt"));
        if(getFilePath.delete()){
            System.out.println("File successfully deleted");
        }else{
            System.out.println("File not exist");

        }
    }
}

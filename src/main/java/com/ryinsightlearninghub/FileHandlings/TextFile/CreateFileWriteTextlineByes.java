/**sumitkumar
 CreateFileWriteTextlineByes.java
 * 01-Apr-2019
 */
package com.ryinsightlearninghub.FileHandlings.TextFile;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import com.ryinsightlearninghub.usefulcontroller.GetFileAbsolutepathByName;

/**
 * @author sumitkumar
 *
 */
public class CreateFileWriteTextlineByes 
{
	public static void main(String[] args) throws IOException {
		
		
		BufferedWriter buffWrite = new BufferedWriter(new FileWriter(GetFileAbsolutepathByName.getAbsolutePath("FileWriterNewFile line.txt")));
	
		buffWrite.write("My name is sumit ");
		
		buffWrite.newLine();
		
		buffWrite.write("Contact number is : 9971458090");
		
		buffWrite.close();
		
		
	}

}

/**
 * @author 			:	 sumit
 *	DATE       		:	 18-Aug-2019
 *  FILE NAME  		: 	 ReadFileDataByGivenRangeRange.java
 *  PROJECT NAME 	:	 BasicCoceptUnderstading
 * 
 */
package com.ryinsightlearninghub.FileHandlings.TextFile;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.ryinsightlearninghub.usefulcontroller.GetFileAbsolutepathByName;

public class ReadFileDataByGivenRangeRange {

	public static void main(String[] args) throws IOException  {
		File connectFile = new File(GetFileAbsolutepathByName.getAbsolutePath("File1_Copyfrom.txt"));
		FileReader readFileData = new FileReader(connectFile);
		BufferedReader readLines = new BufferedReader(readFileData);
		String fileData=null;
		int r=0;
		while ((fileData=readLines.readLine())!=null){
			++r;
			if((r>=2)&&(r<=3)){
				System.out.println(fileData);
			}
			
		}
		readLines.close();
		System.out.println( fileData);
	}

}

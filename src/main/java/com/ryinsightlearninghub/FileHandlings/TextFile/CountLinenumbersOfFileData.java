/**sumitkumar
 CountLinenumbersOfFileData.java
 * 05-Apr-2019
 */
package com.ryinsightlearninghub.FileHandlings.TextFile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

import com.ryinsightlearninghub.usefulcontroller.GetFileAbsolutepathByName;

/**
 * @author sumitkumar
 *
 */
public class CountLinenumbersOfFileData 
{
	public static void main(String[] args) throws IOException 
	{
			BufferedReader lineNumber = new LineNumberReader(new FileReader(GetFileAbsolutepathByName.getAbsolutePath("RangeDataFile.txt")));
			
			int getLines=0; 
			
			while(lineNumber.readLine()!=null)
			{
				getLines++;

			}
			System.out.println("Total number of the line in RangeDataFile \n"+getLines);
			lineNumber.close();
					
		}
		

}

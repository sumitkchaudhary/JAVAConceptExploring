/**sumitkumar
 ReadRangedData.java
 * 06-Apr-2019
 */
package com.ryinsightlearninghub.FileHandlings.TextFile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.ryinsightlearninghub.usefulcontroller.GetFileAbsolutepathByName;

public class ReadRangedData {
	public static void readData(String fileName, int startLine,int endLine) throws IOException {
		
		BufferedReader readLine = new BufferedReader(new FileReader(fileName));
			String line;
			while (startLine<=endLine)
			{
				line=readLine.readLine();
				if (line==null)
				{	
					return;
				}
				System.out.println(line);
				startLine++;
			}
		//	readLine.close();
	}
	 public static void main(String[] args) throws IOException 
	 {
		 	readData(GetFileAbsolutepathByName.getAbsolutePath("NewFile.txt"), 2, 3);
	}

}

/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 13-Mar-2022
 *  FILE NAME  		: 	 WirteDataOnFile.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    9:02:06 am
 */
package com.ryinsightlearninghub.FileHandlings.TextFile;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.ryinsightlearninghub.usefulcontroller.GetFileAbsolutepathByName;

public class WirteDataOnFile {
	
	public static void main(String[] args) throws IOException {
		
		File filePath=new File(GetFileAbsolutepathByName.getAbsolutePath("WriteDataOnFile.txt"));
		
		FileWriter writeData=new FileWriter(filePath);
		
		writeData.write("My Name Is Sumit");
		
		writeData.write("Ok");
		writeData.close();
	}

}

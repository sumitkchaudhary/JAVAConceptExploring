/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 10-Dec-2022
 *  FILE NAME  		: 	 CreateNewFile.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    3:34:04 pm
 */
package com.ryinsightlearninghub.FileHandlings.TextFile;

import com.ryinsightlearninghub.usefulcontroller.GetFileAbsolutepathByName;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CreateNewFile {
	public static void main(String[] args) throws IOException {
		File fileRead=new File(GetFileAbsolutepathByName.getAbsolutePath("newFileTest.txt"));
		
			FileWriter fw=new FileWriter(fileRead);
			fw.write("My Name is Sumit");
			fw.flush();
			fw.close();
			FileReader fr=new FileReader(fileRead);
			int fileToText;
			while((fileToText=fr.read())!=-1) {
				System.out.print((char)fileToText);	
			}
			fr.close();

	}

}

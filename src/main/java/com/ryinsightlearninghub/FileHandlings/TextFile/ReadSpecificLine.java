/**
 * @author 			:	 sumit
 *	DATE       		:	 17-Aug-2019
 *  FILE NAME  		: 	 ReadSpecificLine.java
 *  PROJECT NAME 	:	 BasicCoceptUnderstading
 * 
 */
package com.ryinsightlearninghub.FileHandlings.TextFile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import com.ryinsightlearninghub.usefulcontroller.GetFileAbsolutepathByName;

public class ReadSpecificLine
{
	public static String readLine(int line){
		FileReader  getFileData;
        String returnStr="Error";
        
		try { 
        	getFileData=new FileReader(GetFileAbsolutepathByName.getAbsolutePath("File1_Copyfrom.txt"));
        	BufferedReader tempBufferedReader = new BufferedReader(getFileData);
        	for(int i = 0; i < line; i++) {
        		returnStr=tempBufferedReader.readLine();
        		
        	}
        
        } catch (Exception e) { 
        	e.getMessage();
        	e.getStackTrace();
        }
        return returnStr;
    }
	public static void main(String[] args) throws IOException 	 {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number");
		int specificNumber = sc.nextInt();
		System.out.println(ReadSpecificLine.readLine(specificNumber));
		sc.close();
	}

}

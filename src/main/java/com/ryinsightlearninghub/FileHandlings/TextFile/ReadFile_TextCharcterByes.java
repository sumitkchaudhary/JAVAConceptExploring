/**sumitkumar
 ReadFile_TextCharcterByes.java
 * 31-Mar-2019
 */
package com.ryinsightlearninghub.FileHandlings.TextFile;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.ryinsightlearninghub.usefulcontroller.GetFileAbsolutepathByName;

public class ReadFile_TextCharcterByes
{
	public static void main(String[] args) throws IOException 
	{
		
		FileReader fileRead = new FileReader(new File(GetFileAbsolutepathByName.getAbsolutePath("Filehandle.txt"))); // File reader read a content from file character by character
			
			
		
			//System.out.println("Single Character "+(char)fileRead.read());// here we convert the integer value to character implicitly 
			int fileText; // file read character value and content to byte code after that store in file text variable
			
			System.out.println("Character by character **");
			while((fileText=fileRead.read())!=-1) {
				System.out.println((char)fileText);// here we convert the integer value to character implicitly 
					
			}
		
	}

}

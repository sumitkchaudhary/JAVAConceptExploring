package com.ryinsightlearninghub.FileHandlings.ExcelFile.JxlLibrary;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import com.ryinsightlearninghub.usefulcontroller.GetFileAbsolutepathByName;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class ReadDataFromGivenCellAddress {
	public void readFileData( int row_No, int column_No) throws BiffException, IOException {

		Workbook readWorkSheet = Workbook.getWorkbook(new File(GetFileAbsolutepathByName.getAbsolutePath("readData.xls")));
		Sheet readSheet = readWorkSheet.getSheet(0);

		Cell getCell=null;
		for (int i=0; i<row_No; i++) {
			for (int j=0; j<column_No; j++) {
				 getCell= readSheet.getCell(j,i);
			}
		}

		System.out.println(getCell.getContents());
	}
	
	public static void main(String[] args) throws BiffException, IOException {
		
		ReadDataFromGivenCellAddress obj = new ReadDataFromGivenCellAddress();
		
		System.out.println("Enter Row no & Column no for read the perticuler ");
		Scanner sc= new Scanner(System.in);
		int a=sc.nextInt(); int b=sc.nextInt();
		
		obj.readFileData(a, b);
	}

}

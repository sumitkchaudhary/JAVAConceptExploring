/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 30-Aug-2019
 *  FILE NAME  		: 	 TransferData.java
 *  PROJECT NAME 	:	 BasicCoceptUnderstading
 * 
 */
package com.ryinsightlearninghub.FileHandlings.ExcelFile.JxlLibrary;

import java.io.File;
import java.io.IOException;

import com.ryinsightlearninghub.usefulcontroller.GetFileAbsolutepathByName;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class TransferDatafromOneFileToAnotherFile {
	public static void main(String[] args) throws BiffException, IOException, WriteException {

		Workbook readExcelFile =Workbook.getWorkbook(new File(GetFileAbsolutepathByName.getAbsolutePath("File4ReadData.xls")));
		Sheet  getSheet = readExcelFile.getSheet(0);
		WritableWorkbook copyReadedData = Workbook.createWorkbook(new File(GetFileAbsolutepathByName.getAbsolutePath("WriteReadedData.xls")));
		WritableSheet writeDataSheet = copyReadedData.createSheet("File4ReadData_Data", 0);
		String getSheetReadedData = null;
		int rawNumber = getSheet.getRows();
		int colNumber = getSheet.getColumns();
		for(int i=0; i<rawNumber;i++){
			for(int j=0; j<colNumber; j++) {
				Cell getCell = getSheet.getCell(j, i);
				getSheetReadedData = getCell.getContents();
				Label addLable = new Label(j,i, getSheetReadedData);
				writeDataSheet.addCell(addLable);
			}
		}
		copyReadedData.write();
		copyReadedData.close();

	}

}

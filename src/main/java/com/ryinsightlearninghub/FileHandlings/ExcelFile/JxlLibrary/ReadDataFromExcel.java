/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 25-Aug-2019
 *  FILE NAME  		: 	 ReadDataFromExcel.java
 *  PROJECT NAME 	:	 BasicCoceptUnderstading
 * 
 */
package com.ryinsightlearninghub.FileHandlings.ExcelFile.JxlLibrary;

import java.io.File;
import java.io.IOException;

import com.ryinsightlearninghub.usefulcontroller.GetFileAbsolutepathByName;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class ReadDataFromExcel{
	public static void main(String[] args) throws BiffException, IOException {

		Workbook getworkbook =Workbook.getWorkbook(new File(GetFileAbsolutepathByName.getAbsolutePath("readData.xls")));
		Sheet getSheet =getworkbook.getSheet(0);
		for(int i=0; i<getSheet.getRows(); i++){
			for(int j=0; j<getSheet.getColumns(); j++){
				Cell getCell = getSheet.getCell(j,i);
				System.out.println(getCell.getContents());
			}
		}	
	}
	

}

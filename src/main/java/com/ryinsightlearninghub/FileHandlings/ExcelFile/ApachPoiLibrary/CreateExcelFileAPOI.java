/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 30-Aug-2019
 *  FILE NAME  		: 	 CreateExcelFileAPOI.java
 *  PROJECT NAME 	:	 BasicCoceptUnderstading
 * 
 */
package com.ryinsightlearninghub.FileHandlings.ExcelFile.ApachPoiLibrary;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ryinsightlearninghub.usefulcontroller.GetFileAbsolutepathByName;

public class CreateExcelFileAPOI {
	
	public static void main(String[] args) throws IOException {
		FileOutputStream createExcelFile = new FileOutputStream(new File(GetFileAbsolutepathByName.getAbsolutePath("XLSXFile_create.xlsx")));
		
		//using excel object create workbook in targeted excel file
		XSSFWorkbook createExcel = new XSSFWorkbook();
		//create sheet into excel workbook
		XSSFSheet createSheet = createExcel.createSheet("WriteSheet1");
		//using scanner for taking input from console. 
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the Data which you want to save in Excel file. ");
		String enteredData; 
		//for loop to get row 
		for (int i=0; i<3; i++) {
			XSSFRow createRow = createSheet.createRow(i);
			//for loop to get column
			for(int j=0; j<3; j++) {
				XSSFCell createCell = createRow.createCell(j);
				//strong entered data into string variable
				enteredData=sc.nextLine();
				//passing in excel cell
				createCell.setCellValue(enteredData);
			}
		}
		sc.close();
		createExcel.write(createExcelFile);
		createExcelFile.flush();
		createExcel.close();
		
		System.out.println("Thanks to you, Data is successfully saved in File");

	}

}

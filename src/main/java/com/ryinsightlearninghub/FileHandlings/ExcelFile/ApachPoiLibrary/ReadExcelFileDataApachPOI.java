/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 30-Aug-2019
 *  FILE NAME  		: 	 ReadExcelFileData.java
 *  PROJECT NAME 	:	 BasicCoceptUnderstading
 * 
 */
package com.ryinsightlearninghub.FileHandlings.ExcelFile.ApachPoiLibrary;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ryinsightlearninghub.usefulcontroller.GetFileAbsolutepathByName;

public class ReadExcelFileDataApachPOI {
	public static void main(String[] args) throws InvalidFormatException, IOException {
		XSSFWorkbook  readExcelFile= new XSSFWorkbook(new File(GetFileAbsolutepathByName.getAbsolutePath("XLSXFile_READ.xlsx")));
		XSSFSheet getSheet = readExcelFile.getSheetAt(0);
		int rowNumber = getSheet.getPhysicalNumberOfRows();
		for(int i=0; i<rowNumber; i++) {
			XSSFRow getRow = getSheet.getRow(i);
			int sheetCell = getRow.getPhysicalNumberOfCells();
			for(int j=0; j<sheetCell; j++) {
				XSSFCell getCell = getRow.getCell(j);
				switch (getCell.getCellType()) {
				case STRING:System.out.println(getCell.getStringCellValue());
					break;
				case NUMERIC:System.out.println(getCell.getNumericCellValue());
				break;
				case FORMULA:System.out.println(getCell.getCellFormula());
				break;
				default:
					System.out.println(getCell.getStringCellValue());
					break;
				}
			}
		}
	}

}

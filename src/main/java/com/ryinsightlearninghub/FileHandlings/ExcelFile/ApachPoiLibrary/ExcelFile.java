package com.ryinsightlearninghub.FileHandlings.ExcelFile.ApachPoiLibrary;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.ryinsightlearninghub.usefulcontroller.GetFileAbsolutepathByName;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelFile {

	public void readExcel(String filePath,String fileName,String sheetName) throws IOException{
	   //TODO Resolve the File path issues

	    File file =    new File(filePath+"\\"+fileName);
	    FileInputStream inputStream = new FileInputStream(file);
	    Workbook workbook = null;
	    String fileExtensionName = fileName.substring(fileName.indexOf("."));
	    if(fileExtensionName.equals(".xlsx")){
	    	workbook = new XSSFWorkbook(inputStream);
	    }else if(fileExtensionName.equals(".xls")){
			workbook = new HSSFWorkbook(inputStream);
	    }
	    Sheet workbookSheet = workbook.getSheet(sheetName);

	    int rowCount = workbookSheet.getLastRowNum()- workbookSheet.getFirstRowNum();
	    for (int i = 0; i < rowCount+1; i++) {
	        Row row = workbookSheet.getRow(i);
	        for (int j = 0; j < row.getLastCellNum(); j++) {
	            System.out.print(row.getCell(j).getStringCellValue()+"|| ");
	        }
	        System.out.println();
	    } 
	}
    public static void main(String[] args) throws IOException{
		ExcelFile objExcelFile = new ExcelFile();
	    String filePath = GetFileAbsolutepathByName.getAbsolutePath("");
		objExcelFile.readExcel(filePath,"JavaBooks.xlsx","data");
    }
}
	


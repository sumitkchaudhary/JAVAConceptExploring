/**
 * @author 			:	 DELL
 *	DATE       		:	 22-Jun-2021
 *  FILE NAME  		: 	 GetFileAbsolutepathByName.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    1:10:50 PM
 */
package com.ryinsightlearninghub.usefulcontroller;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Collection;
import java.util.Iterator;

public class GetFileAbsolutepathByName {
    public static String getAbsolutePath(String fileName) {
        String fileAbsolutePath = "";

        try {
            Collection<File> files= FileUtils.listFiles(new File(System.getProperty("user.dir")), null, true);
            for(Iterator iterator= files.iterator(); iterator.hasNext();){
                File file = (File) iterator.next();
                if (file.getName().equals(fileName))
                    fileAbsolutePath=file.getAbsolutePath();
            }

        } catch (Exception e) {
            System.err.println("An unexpected error occurred: " + e.getMessage());
        }

        return fileAbsolutePath;
    }
}

/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 12-Oct-2019
 *  FILE NAME  		: 	 PropertyFileReader.java
 *  PROJECT NAME 	:	 BasicDatabaseProject
 * 	Class Time		:    7:22:24 pm
 */
package com.ryinsightlearninghub.usefulcontroller;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class PropertyFileReader {

	public static Properties getPropertyFile(String fileName) {
		Properties getSqlQueryPropertiesFile= new Properties();
		try {
			FileInputStream getSQLFile = new FileInputStream(new File(GetFileAbsolutepathByName.getAbsolutePath(fileName)));

			getSqlQueryPropertiesFile.load(getSQLFile);

		}catch(Exception e){
			System.err.println(e);
			e.printStackTrace();
		}
		return getSqlQueryPropertiesFile;

	}

}

/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 07-Oct-2019
 *  FILE NAME  		: 	 GetEmployeeData.java
 *  PROJECT NAME 	:	 BasicDatabaseProject
 * 	Class Time		:    8:29:51 pm
 */
package com.ryinsightlearninghub.JDBC.GetData;

import com.ryinsightlearninghub.JDBC.ConnectionBase.DataBaseConnectionCredentials;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class GetEmployeeData extends  DataBaseConnectionCredentials{

	public static void main(String[] args) throws SQLException, IOException {

	        try {
	            Connection con= DataBaseConnectionCredentials.connectionCredentials();
	           
	            Statement stmt=con.createStatement();
	            ResultSet rs=stmt.executeQuery(getPropertyFileData.getProperty("getAllEmpData"));
	            while(rs.next()) {
	                System.out.println("Employee id			\t:\t"+rs.getInt("emp_no"));
	                System.out.println("Employee First Name		\t:\t"+rs.getString("first_name"));
	                System.out.println("Employee Last Name		\t:\t"+rs.getString("last_name"));
	                System.out.println("Employee DOB			\t:\t"+rs.getDate("birth_date"));
	                System.out.println("Employee Gander			\t:\t"+rs.getString("gender"));
	                System.out.println("Employee Joining		\t:\t"+rs.getDate("hire_date"));
	                System.out.println("-----------------------------");
	                   
	            }
	            con.close();
	            
	        } catch (Exception e) 
	        {
	            e.printStackTrace();
	        }
	    }
  
        
	}



/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 18-Nov-2019
 *  FILE NAME  		: 	 SecondHighestSalariedEmploye.java
 *  PROJECT NAME 	:	 BasicDatabaseProject
 * 	Class Time		:    8:40:47 pm
 */
package com.ryinsightlearninghub.JDBC.GetData;

import com.ryinsightlearninghub.JDBC.ConnectionBase.DataBaseConnectionCredentials;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class SecondHighestSalariedEmployee extends DataBaseConnectionCredentials {
	public static void main(String[] args) throws IOException {
		try {
			Connection connect = DataBaseConnectionCredentials.connectionCredentials();
			
			Statement stmt = connect.createStatement();
			ResultSet restSt = stmt.executeQuery(getPropertyFileData.getProperty("secondHighestSalary"));
			while(restSt.next())
			{
				System.out.println("Employee First Name		\t:\t"+restSt.getString("first_name"));
                System.out.println("Employee Joining		\t:\t"+restSt.getInt("salary"));
			}
			connect.close();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	} 

}

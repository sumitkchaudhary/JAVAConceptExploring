/**
 * @author 			:	 sumitkumar
 *	DATE       		:	 07-Oct-2019
 *  FILE NAME  		: 	 DataBaseConnectionCredentials.java
 *  PROJECT NAME 	:	 BasicDatabaseProject
 * 	Class Time		:    8:28:42 pm
 */
package com.ryinsightlearninghub.JDBC.ConnectionBase;

import com.ryinsightlearninghub.usefulcontroller.PropertyFileReader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DataBaseConnectionCredentials {
	public static Properties getPropertyFileData= PropertyFileReader.getPropertyFile("SQL_Queries.properties.properties");
	public static Connection connectionCredentials() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.cj.jdbc.Driver");

        return DriverManager.getConnection("jdbc:mysql://127.0.0.1/employee_management", "root", "sumitdb");
	}

}

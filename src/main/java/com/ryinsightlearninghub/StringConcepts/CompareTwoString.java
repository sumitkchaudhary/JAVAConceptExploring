/**
 * @author 			:	 dell
 *	DATE       		:	 20-Jan-2022
 *  FILE NAME  		: 	 CompareTwoString.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    6:34:18 pm
 */
package com.ryinsightlearninghub.StringConcepts;
public class CompareTwoString {
	
	public static void main(String[] args) {
		String s="Sumit Kumar";
		
		if(s.contains("Kumar")) {  //Returns true if and only if this string contains the specified sequence of char values 
			System.out.println("true");
		}else {
			System.out.println("False");
		}
		
		if(s=="Kumar") {  //Return true if and only if this string contains the exact value
			System.out.println("true");
		}else {
			System.out.println("false");
		}
		//Compares this string to the specified object. The result is true if and only if the argument is not null and is a String object that represents the same sequence of characters as this object.
		if(s.equals("Kumar")) {  
			System.out.println("true");
		}else {
			System.out.println("false");
		}
		
		if(s.equalsIgnoreCase("Sumit kumar")) {
			System.out.println("true");
		}else {
			System.out.println("false");
		}
	}

}

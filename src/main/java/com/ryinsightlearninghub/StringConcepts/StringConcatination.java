/**
 * @author 			:	 Sumit Kumar Chaudhary
 *	DATE       		:	 05-Jan-2022
 *  FILE NAME  		: 	 StringConcatination.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    11:08:46 pm
 */
package com.ryinsightlearninghub.StringConcepts;
public class StringConcatination {
	
	public static void main(String[] args) {
		int a=10;
		int b=20;
		
		double d1=10.33;
		double d2=20.33;
		
		float f1=20.50f;
		float f2=20.50f;
		
		String s1="Sumit";
		String s2="Kumar";
		
		System.out.println(a+b); 					//30
		System.out.println(s1+s2);					//SumitKumar
		System.out.println(a+b+s1+s2);				//30SumitKumar
		System.out.println(s1+s2+a+b);				//SumitKumar1020
		System.out.println(s1+s2+(a+b));			//SumitKumar30
		System.out.println(a+s1+b+s2);				//10Sumit20Kumar
		System.out.println(a+s1+b+s2+a+b);			//10Sumit20Kumar1020
		System.out.println(d1+d2);					//30.659999999999997
		System.out.println(d1+d2+s1+s2);			//30.659999999999997SumitKumar
		System.out.println(s1+s2+d1+d2);			//SumitKumar10.3320.33
		System.out.println(s1+s2+(d1+d2));			//SumitKumar30.659999999999997
		System.out.println(d1+s1+d2+s2);			//10.33Sumit20.33Kumar
		System.out.println(d1+s1+d2+s2+d1+d2);		//10.33Sumit20.33Kumar10.3320.33
		System.out.println(f1+f2);					//41.0
		System.out.println(f1+f2+s1+s2);			//41.0SumitKumar
		System.out.println(s1+s2+f1+f2);			//SumitKumar20.520.5
		System.out.println(s1+s2+(f1+f2));			//SumitKumar41.0
		System.out.println(f1+s1+f2+s2);			//20.5Sumit20.5Kumar
		System.out.println(f1+s1+f2+s2+f1+f2);		//20.5Sumit20.5Kumar20.520.5
		
	}

}

/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 19-Feb-2022
 *  FILE NAME  		: 	 RecursionConcept.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:04:02 pm
 */
package com.ryinsightlearninghub;

import java.util.Scanner;

public class RecursionConcept {
	//In Java, a method that calls itself is known as a recursive method. And, this process is known as recursion.
	public static int factorial(int n) {
		if(n!=0) 
			return n * factorial(n-1);//factorial method call its self 
		else
			return 1;
	}
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter you number");
		int number=sc.nextInt();
		
		System.out.println(number+" Factorial ="+factorial(number));
		sc.close();
		
	}
	/*
	 * Advantages and Disadvantages of Recursion 
	 * When a recursive call is made, new storage locations for variables are allocated on the stack. 
	 * As, each recursive call returns, the old variables and parameters are removed from the stack. 
	 * Hence, recursion generally uses more memory and is generally slow.
	 * On the other hand, a recursive solution is much simpler and takes less time to write, debug and maintain.
	 * */

}

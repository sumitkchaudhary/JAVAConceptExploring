/**sumitkumar
 TreeSet_Concept.java
 * 05-May-2019
 */
package com.ryinsightlearninghub.CollectionInterface.Set;

import java.util.TreeSet;

public class TreeSet_Concept {
	public static void main(String[] args) {
		/*Java TreeSet class implements the Set interface that uses a tree for storage. Like HashSet, TreeSet also contains unique elements. 
		 * However, the access and retrieval time of TreeSet is quite fast. 
		 * The elements in TreeSet stored in ascending order.*/
		TreeSet<Integer> tSet = new TreeSet<Integer>();
		// TreeSet it will give you in shorting order ascending
		tSet.add(1);
		tSet.add(3);
		tSet.add(4);
		tSet.add(2);
		tSet.add(5);

		for (int a : tSet) {
			System.out.println(a);
		}

	}

}

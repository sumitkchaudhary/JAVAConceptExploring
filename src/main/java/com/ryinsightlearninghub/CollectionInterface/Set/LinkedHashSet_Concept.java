/**sumitkumar
 LinkedHashSet_Concept.java
 * 05-May-2019
 */
package com.ryinsightlearninghub.CollectionInterface.Set;

import java.util.LinkedHashSet;

public class LinkedHashSet_Concept {
	public static void main(String[] args) {
		
		/* LinkedHashSet class represents the LinkedListBasics implementation of Set Interface.
		 * It extends the HashSet class and implements Set interface. Like HashSet, It also contains unique elements. 
		 * It maintains the insertion order and permits null elements.*/

		LinkedHashSet<Integer> lHs = new LinkedHashSet<Integer>();
		// LinkedHasSet it will maintain the insertion order

		lHs.add(1);
		lHs.add(4);
		lHs.add(5);
		lHs.add(2);
		lHs.add(3);

		for (Integer a : lHs) {
			System.out.println(a);
		}
		LinkedHashSet<String> strValue = new LinkedHashSet<String>();
		// LinkedHasSet it will maintain the insertion order

		strValue.add("A");
		strValue.add("C");
		strValue.add("E");
		strValue.add("B");
		strValue.add("D");

		for (String b : strValue) {

			System.out.println(b);
		}

	}

}

/**sumitkumar
 HashSet.java
 * 05-May-2019
 */
package com.ryinsightlearninghub.CollectionInterface.Set;

import java.util.HashSet;

public class HashSetConcept {
	public static void main(String[] args) {
		/*HashSet class implements Set Interface. It represents the collection that uses a hash table for storage. 
		 * Hashing is used to store the elements in the HashSet. It contains unique items.*/
		HashSet<Integer> hs = new HashSet<Integer>(); // <integer > means we can store only integer value.
		// HashSet will not maintain the variable insertion order

		hs.add(5);
		hs.add(2);
		hs.add(1);
		hs.add(4);
		hs.add(3);
		hs.add(4);

		for (Integer a : hs) // : means each its help to store all variable in a from hs one by one
		{
			System.out.println(a);
		}

		HashSet<String> strHas = new HashSet<String>();
		strHas.add("Sumit");
		strHas.add("Sachin");
		strHas.add("Saurav");
		strHas.add("Amit");
		strHas.add("Vivek");
		strHas.add("Amit");

		for (String name : strHas) {
			System.out.println(name);
		}
	}

}

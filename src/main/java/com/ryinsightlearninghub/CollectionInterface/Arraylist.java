package com.ryinsightlearninghub.CollectionInterface;
import java.util.*;

public class Arraylist {

	public static void main(String[] args) {
		 ArrayList<String> al=new ArrayList<String>();  
		  al.add("Mango");  
		  al.add("Apple");  
		  al.add("Banana");  
		  al.add("Grapes");  
		  
		  //accessing the 2 index element 
		  System.out.println("Returning element: "+al.get(1));//it will return the 2nd element, because index starts from 0  
		  //changing the element  
		  al.set(1,"Dates");  
		  //short the element
		  Collections.sort(al);
		  //Traversing list using foreach loop  
		  
		  for(String fruit:al)    
		    System.out.println(fruit);  

	}

}

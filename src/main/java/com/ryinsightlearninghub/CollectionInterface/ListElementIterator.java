package com.ryinsightlearninghub.CollectionInterface;
import java.util.ArrayList;
import java.util.*;

public class ListElementIterator {

	public static void main(String[] args) {
		List<String> list=new ArrayList<String>();
		 list.add("Mango");  
		 list.add("Apple");  
		 list.add("Banana");  
		 list.add("Grapes");  
		
        //Iterate element usng listIterator		 
		 System.out.println("Iterate using list iterator: ");
		 ListIterator<String> itr=list.listIterator(list.size());		
		 while(itr.hasPrevious()){
		  String str=itr.previous();
		  System.out.println(str);
		 }
		 
		 //Iterate using for loop
		 System.out.println("Iterate using for Loop: ");
		 for(int i=0;i<list.size();i++) {
			 String str=list.get(i);
			 System.out.println(str);
		 }
		 
		 System.out.println("Iterate using for each loop");
		 for(String str:list) {
			 System.out.println(str);
		 }
		 
		 //using for each  loop Lambda Expression
		 System.out.println("Iterate using for each loop lambda Expression");
		/* list.forEach(l-> {
			System.out.println(l);
		});*/
		 
		 System.out.println("Iterate using iterator");
		 Iterator<String> itrr=list.iterator();
		 
		 while(itrr.hasNext()) {
			 String str=itrr.next();
			 System.out.println(str);
		 }
	 
//		 itrr.forEachRemaining(l->{
//			 System.out.println(l);
//			 
//		 });
	}

}

/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 12-Mar-2022
 *  FILE NAME  		: 	 List_Vector.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    8:06:08 pm
 */
package com.ryinsightlearninghub.CollectionInterface.ListInterface;

import java.util.List;
import java.util.Vector;

public class List_Vector {
	
	public static void main(String[] args) {
		List<String> mammals=new Vector<>();
		
		mammals.add("Elements");
		mammals.add("Fox");
		mammals.add("Lion");
		mammals.add("Elements");
		for(String s: mammals) {
			System.out.println(s);
		}
		/*Vector uses a dynamic array to store the data elements. It is similar to ArrayList.
		 *  However, It is synchronized and contains many methods that are not the part of Collection framework.*/
		
	}

}

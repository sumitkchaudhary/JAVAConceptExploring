/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 12-Mar-2022
 *  FILE NAME  		: 	 List_Stack.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    8:13:46 pm
 */
package com.ryinsightlearninghub.CollectionInterface.ListInterface;

import java.util.Iterator;
import java.util.Stack;

public class List_Stack {
	
	public static void main(String[] args) {
		Stack<String> stack = new Stack<String>();  
		stack.push("Ayush");  
		stack.push("Garvit");  
		stack.push("Amit");  
		stack.push("Ashish");  
		stack.push("Garima");  
		stack.pop();  //Remove the object which is in the top in the list. 
		Iterator<String> itr=stack.iterator();  
		while(itr.hasNext()){  
		System.out.println(itr.next());  
		}  
		
		Stack<String> animals=new Stack<String>();
		
		animals.push("Lion");
		animals.push("Fox");
		animals.push("Elephant");
		animals.push("Monkey");
		
		
		for(String animal: animals) {
			System.out.println(animal);
		}
		
		System.out.println("Remove the Top of the animal in the list is: "+animals.pop());
		System.out.println("Get the top of the element without removing: "+animals.peek());
		
		/*The stack is the subclass of Vector. It implements the last-in-first-out data structure, i.e., Stack. 
		 * The stack contains all of the methods of Vector class and also provides its methods like boolean push(),
		 * boolean peek(), boolean push(object o), which defines its properties.*/
		
	}

}

/**sumitkumar
 List_ArrayList.java
 * 05-May-2019
 */
package com.ryinsightlearninghub.CollectionInterface.ListInterface;

import java.util.ArrayList;
import java.util.List;

public class List_ArrayList {
	public static void main(String[] args) {
		List<Integer> arrList = new ArrayList<Integer>();

		arrList.add(1);
		arrList.add(4);
		arrList.add(3);
		arrList.add(2);

		for (int a : arrList) {
			System.out.println(a);
		}

		ArrayList<String> strArr = new ArrayList<String>();
		strArr.add("Sumit");
		strArr.add("Sachin");
		strArr.add("Saurav");
		strArr.add("Sumit");//Array List can store duplicate value
		
		strArr.remove("Sumit");//Remove the top element
		

		for (String b : strArr) {
			System.out.println(b);
		}
		/*The ArrayList class implements the List interface. It uses a dynamic array to store the duplicate element of different data types. 
		 * The ArrayList class maintains the insertion order and is non-synchronized. 
		 * The elements stored in the ArrayList class can be randomly accessed. Consider the following example.*/

	}

}

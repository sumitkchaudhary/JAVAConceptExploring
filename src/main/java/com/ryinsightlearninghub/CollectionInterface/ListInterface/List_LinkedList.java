/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 12-Mar-2022
 *  FILE NAME  		: 	 List_LinkedList.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    7:51:06 pm
 */
package com.ryinsightlearninghub.CollectionInterface.ListInterface;

import java.util.LinkedList;
import java.util.List;

public class List_LinkedList {
	public static void main(String[] args) {
		List<String> animals=new LinkedList<>();
		
		animals.add("Dog");
		animals.add("Buffalo");
		animals.add("Dear");
		animals.add("Elephant");
		System.out.println("Get all Element in array: "+animals);
		System.out.println("Get the element by index : 2nd Index value is : "+animals.get(2));
	
		for(String animal: animals) {
			System.out.println(animal);
		}
		
		/*LinkedListBasics implements the Collection interface. It uses a doubly linked list internally to store the elements. 
		 * It can store the duplicate elements. It maintains the insertion order and is not synchronized. 
		 * In LinkedListBasics, the manipulation is fast because no shifting is required.*/
	}

}

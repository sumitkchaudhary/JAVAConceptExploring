package com.ryinsightlearninghub.CollectionInterface;
import java.util.*;  
public class LinkedListMethods {

	public static void main(String[] args) {
		
	LinkedList<String> ll=new LinkedList<String>();	
	ll.add("Ramesh");
	ll.add("Vijay");
	ll.add("Anurag");
		
		Iterator<String> itr=ll.descendingIterator();
		
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}

	}

}

/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 13-Mar-2022
 *  FILE NAME  		: 	 Deque_ArrayDeque.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    8:59:57 pm
 */
package com.ryinsightlearninghub.CollectionInterface.Queues.Deque;

import java.util.ArrayDeque;

public class Deque_ArrayDeque {
	public static void main(String[] args) {
		 ArrayDeque<String> animals= new ArrayDeque<>();

	        // Using add()
	        animals.add("Dog");

	        // Using addFirst()
	        animals.addFirst("Cat");

	        // Using addLast()
	        animals.addLast("Horse");
	        System.out.println("ArrayDeque: " + animals);
	}

}

/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 12-Mar-2022
 *  FILE NAME  		: 	 Queue_ArrayDeque.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:37:18 pm
 */
package com.ryinsightlearninghub.CollectionInterface.Queues;

import java.util.ArrayDeque;
import java.util.Deque;

public class Queue_ArrayDeque {
	public static void main(String[] args) {
		/*ArrayDeque class implements the Deque interface. It facilitates us to use the Deque. Unlike queue, 
		 * we can add or delete the elements from both the ends.
		 * ArrayDeque is faster than ArrayList and Stack and has no capacity restrictions.*/
		Deque<String> deque = new ArrayDeque<String>();  
		deque.add("Gautam");  
		deque.add("Karan");  
		deque.add("Ajay");  
		//Traversing elements  
		for (String str : deque) {  
		System.out.println(str);  
		}  
	}

}

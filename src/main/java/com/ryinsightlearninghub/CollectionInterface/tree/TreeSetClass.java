package com.ryinsightlearninghub.CollectionInterface.tree;
import java.util.Iterator;
import java.util.TreeSet;

public class TreeSetClass {

	public static void main(String[] args) {
		
		TreeSet<String> tree=new TreeSet<String>();
        tree.add("Mohit");
        tree.add("Rahul");
        tree.add("Mohit");
        tree.add("Anurag");
        tree.add("Monit");
        tree.add("Pankaj");
        tree.add("Polit");
        tree.add("Anuj");
        System.out.println("Initial set: "+tree);
        System.out.println("Descending tree set: "+tree.descendingSet());
        System.out.println("Head set of Pankaj :"+tree.headSet("Pankaj",true));
        System.out.println("Tail Set of Monit: "+tree.tailSet("Monit",false));
//        System.out.println();
//        System.out.println();
        System.out.println("Remove lowest value: "+tree.pollLast());
        System.out.println("Remove highest value: "+tree.pollFirst());
        Iterator itr=tree.iterator();      
        while(itr.hasNext()) {
        	System.out.println(itr.next());
        }
	}

}

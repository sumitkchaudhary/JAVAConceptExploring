/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 21-Feb-2022
 *  FILE NAME  		: 	 IOConcept.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    9:19:42 am
 */
package com.ryinsightlearninghub.basicpillers;

import java.util.Scanner;

public class IOConcept {
	
	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		int i=sc.nextInt();
		String s=sc.nextLine();
		double d=sc.nextDouble();
		float f=sc.nextFloat();
		//char c=sc.
		
		//Out put
		System.out.print("One Line Print put");// It prints string inside the quotes.
		System.out.println("New Line Print out put");//It prints string inside the quotes similar like print() method. Then the cursor moves to the beginning of the next line.
		System.out.printf("");//It provides string formatting (similar to printf in C/C++ programming).
		
		
		
	}

}

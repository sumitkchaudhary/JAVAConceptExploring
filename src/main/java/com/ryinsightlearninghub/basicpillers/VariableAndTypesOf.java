package com.ryinsightlearninghub.basicpillers;

public class VariableAndTypesOf 
{
	int instanceGlobalVariable = 50;				//Global instance variable --Can not access inside the static method. 
	static int globalStaticVariable=100; 			//Global static variable -- It can access static and non-static/ void method 
	
	public static void main(String[] args) {
		int localVariable = 28;//local variable --Can access only inside the method only 
	
		System.out.println("Print the Value from local variable\n\n"+localVariable);
		
		VariableAndTypesOf referenceVariable=new VariableAndTypesOf();//With the help of new keyword we can create a instanc of the class
		
		referenceVariable.nonStaticExample();  //with the help of reference variable we can access the method and global variable
		
		System.out.println(VariableAndTypesOf.staticMethodexmple()); //For static variable no need to create a reference variaable 
		
	}
	
	public void nonStaticExample() {
		
		instanceGlobalVariable=300;
		globalStaticVariable=500;
		System.out.println(instanceGlobalVariable);
		System.out.println(globalStaticVariable);
		
	}
	public static int staticMethodexmple() {
		globalStaticVariable=2;
		
		//instanceGlobalVariable=50;
		
		return globalStaticVariable;
	}
	
	

}

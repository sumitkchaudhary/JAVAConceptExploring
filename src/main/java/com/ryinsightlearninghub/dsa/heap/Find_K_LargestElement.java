/**
 * @author 			:	 Sumit-Chaudhary
 *	DATE       		:	 28-Dec-2023
 *  FILE NAME  		: 	 K_LargestElement.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    9:54:40 pm
 */
package com.ryinsightlearninghub.dsa.heap;
import java.util.PriorityQueue;

public class Find_K_LargestElement {
	public static void main(String[] args) {
		int arr[]= {10,1,4,7,2,13,3};
		int k=4;
		K_LargetElement(arr, k);
	}
	public static void K_LargetElement(int arr[], int k) {
		PriorityQueue<Integer> pq=new PriorityQueue<Integer>();
		int i=0;
		while(i<k) {
			pq.add(arr[i]);
			i++;
		}
		while(i<arr.length) {
			if(arr[i]>pq.peek()) {
				pq.remove();
				pq.add(arr[i]);
			}
			i++;
		}
		while(pq.size()>0) {
			System.out.println(pq.remove());
		}
	}
}

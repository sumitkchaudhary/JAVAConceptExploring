/**
 * @author 			:	 Sumit-Chaudhary
 *	DATE       		:	 28-Dec-2023
 *  FILE NAME  		: 	 PriorityQueueBasic.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    9:34:54 pm
 */
package com.ryinsightlearninghub.dsa.heap;

import java.util.PriorityQueue;

public class PriorityQueueBasic {
	public static void main(String[] args) {
		PriorityQueue<Integer> pq=new PriorityQueue<Integer>();
		pq.add(10);
		pq.add(20);
		pq.add(30);
		pq.add(40);
		pq.add(50);
		pq.add(60);
		pq.add(9);
		pq.add(9);
		pq.add(8);
		
		System.out.println(pq);
		
		System.out.println("Return lowest element of the list: "+pq.peek());
		
		System.out.println("Remove lowest element of the list: "+pq.poll());
		
		System.out.println("Return size of list or count of element: "+pq.size());
		

		
		
	}

}

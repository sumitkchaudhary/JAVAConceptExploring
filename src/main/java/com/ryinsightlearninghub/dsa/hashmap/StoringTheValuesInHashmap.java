/**
 * @author 			:	 Sumit-Chaudhary
 *	DATE       		:	 28-Dec-2023
 *  FILE NAME  		: 	 StoringTheValuesInHashmap.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:13:01 pm
 */
package com.ryinsightlearninghub.dsa.hashmap;

import java.util.HashMap;
public class StoringTheValuesInHashmap {
	public static void main(String[] args) {
		HashMap<String, Integer> hm=new HashMap<>();
		hm.put("India", 138);
		hm.put("Pakistan", 22);
		hm.put("Nepal", 2);
		hm.put("Australia", 150);
		hm.put("America", 560);
		hm.put("Afganistan", 50);
		//Print all values
		System.out.println(hm);
		//Print the value as per key
		System.out.println(hm.get("India"));
		//Is key available? --if Yes then return true if not then return false
		System.out.println(hm.containsKey("India"));
		//How to run loop --traversing
		for(String countryName: hm.keySet()) {
			System.out.println(countryName);
		}
		System.out.println("Size of hashmap : "+hm.size());
		System.out.println("Remove the value from hashmap : "+hm.remove("Afganistan"));
		for(String countryName: hm.keySet()) {
			System.out.println("Country: "+ countryName+ " Population : "+hm.get(countryName));
		}
	}

}

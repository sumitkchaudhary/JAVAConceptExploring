/**sumitkumar
 Sumit_Loops.java
 * 02-Apr-2019
 */
package com.ryinsightlearninghub.ControlStatements;

public class ForLoops {
	public static void main(String[] args) 
	{
		// print the 1 to 10 number 
		int i;
		for ( i=1; i<=10; i++)  // for loops 
		{
			System.out.println(i);
		}
		
		
		
	}

}

/**sumitkumar
 SingleIf.java
 * 01-Apr-2019
 */
package com.ryinsightlearninghub.ControlStatements;

import java.util.Scanner;

/**
 * @author sumitkumar
 * only if
 */
public class SingleIf
{
	public static void main(String[] args) {
		
		int a; 
		System.out.println("Please enter the value for check odd or even");
		
		Scanner sc = new Scanner(System.in);
		
		a=sc.nextInt();
		
		if(a%2==0)
		{
			System.out.println(a+" is Even number");
		}
	}

}

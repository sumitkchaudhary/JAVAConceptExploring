/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 07-Mar-2022
 *  FILE NAME  		: 	 ContinueWithNestedLoop.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:07:50 pm
 */
package com.ryinsightlearninghub.ControlStatements;
public class ContinueWithNestedLoop {
	public static void main(String[] args) {

	    int i = 1, j = 1;

	    // outer loop
	    while (i <= 5) {

	      System.out.println("Outer Loop: " + i);

	      // inner loop
	      while(j <= 5) {

	        if(j == 2) {
	          j++;
	          continue;
	        }

	        System.out.println("Inner Loop: " + j);
	        j++;
	      }
	      i++;
	    }
	  }

}

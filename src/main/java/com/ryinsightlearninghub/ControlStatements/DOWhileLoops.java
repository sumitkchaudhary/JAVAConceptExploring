/**
 * @author 			:	 DELL
 *	DATE       		:	 02-Jun-2021
 *  FILE NAME  		: 	 DOWhileLoops.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:37:30 PM
 */
package com.ryinsightlearninghub.ControlStatements;
public class DOWhileLoops {
	public static void main(String[] args) {
		int a=0;
		
		do 
		{
			System.out.println(a);  // first print the a value is 0 because condition check after the 
									//the syso 
			a=a+1;
		}
		while(a<=10);  // if take o>10 then loops run once 
	}

}

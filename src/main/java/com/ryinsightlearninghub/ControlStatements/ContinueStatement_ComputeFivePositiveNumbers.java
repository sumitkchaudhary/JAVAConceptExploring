/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 07-Mar-2022
 *  FILE NAME  		: 	 ContinueStatement_ComputeFivePositiveNumbers.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    10:03:45 pm
 */
package com.ryinsightlearninghub.ControlStatements;

import java.util.Scanner;

public class ContinueStatement_ComputeFivePositiveNumbers {
	public static void main(String[] args) {
		Double number, sum = 0.0;
	    // create an object of Scanner
	    Scanner input = new Scanner(System.in);

	    for (int i = 1; i < 6; ++i) {
	      System.out.print("Enter number " + i + " : ");
	      // takes input from the user
	      number = input.nextDouble();

	      // if number is negative
	      // continue statement is executed
	      if (number <= 0.0) {
	        continue;
	      }

	      sum += number;
	    }
	    System.out.println("Sum = " + sum);
	    input.close();
	}

}

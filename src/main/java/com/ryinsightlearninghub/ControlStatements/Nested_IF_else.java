/**sumitkumar
 Nested_IF_else.java
 * 02-Apr-2019
 */
package com.ryinsightlearninghub.ControlStatements;

import java.util.Scanner;

/**
 * @author sumitkumar
 *
 */
public class Nested_IF_else
{
	public static void main(String[] args) {
		
		System.out.println("Please enter the number to check odd and even");
		
		int a ;
		
		Scanner sc =new Scanner(System.in);
		
		a=sc.nextInt();
		
		if (a>=0)  // First Condition check entered number must be greater than 0: if this condition is true then check even on odd 
		{
			if (a%2==0)// Condition check after  first -- true -- even
			{
				System.out.println(a+"is Even number");
			}
			else
			{
				System.out.println(a+"is odd number");
			}
		}
		else
		{
			System.out.println("Please enter positive number");
		}
	}

}

/**sumitkumar
 SwitchCaseCondition.java
 * 02-Apr-2019
 */
package com.ryinsightlearninghub.ControlStatements;

import java.util.Scanner;

/**
 * @author sumitkumar
 *
 */
public class SwitchCaseCondition
{
	public static void main(String[] args) {
		
		int a=0;
		int b=0;
		int c=0;
		System.out.println("Please enter two number for perform calculation");
		
		
		Scanner sc =new Scanner(System.in);
		
		a=sc.nextInt();
		b=sc.nextInt();
		
		System.out.println("Please enter desire operator symbol to perform\n"
				+ "Example \r\n1. +\r\n 2.-\r\n 3. *\r\n 4. /\n");
		char operator =sc.next().charAt(0);// when we want to take operator input from user
		System.out.println("Your enter operator is :"+operator);
		
		
		switch(operator)  
		{
		case '+':  // match the operator variable value and run the define calculation
			c=a+b;
			break; // break is used for exit from the case
		case '-':
			c=a-b;
			break;
		case '*':
			c=a*b;
			break;
		case '/':
			c=a/b;
			break;
		default: 
		System.out.println("Please enter correct operator ");
		
		}
		System.out.println("Value after calculation is as per airthmetic operator"+c);
		
		
	}

}

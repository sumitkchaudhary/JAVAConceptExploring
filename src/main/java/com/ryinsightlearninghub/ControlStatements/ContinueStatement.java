/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 07-Mar-2022
 *  FILE NAME  		: 	 ContinueStatement.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    9:52:50 pm
 */
package com.ryinsightlearninghub.ControlStatements;
public class ContinueStatement {
	
	public static void main(String[] args) {
		/*While working with loops, sometimes you might want to skip some statements or terminate the loop. 
		 * In such cases, break and continue statements are used.
		 * After the continue statement, the program moves to the end of the loop. 
		 * And, test expression is evaluated (update statement is evaluated in case of the for loop).
		 * */
		 // for loop
	    for (int i = 1; i <= 10; ++i) {

	      // if value of i is between 4 and 9
	      // continue is executed
	      if (i > 4 && i < 9) {
	        continue;
	      }
	      System.out.println(i);
	    }
	}

}

/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 06-Mar-2022
 *  FILE NAME  		: 	 ForEachLoop.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    7:10:27 pm
 */
package com.ryinsightlearninghub.ControlStatements;
public class ForEachLoop {
	
	public static void main(String[] args) {
		String friendsName[]= {"Sachin","Sourav", "Ajay","Sonu"};
		
		for(String friend:friendsName ) {
			System.out.println(friend);
		}
		int numbers[]= {12,34,56,78,675,6546,-1,000};
		
		for(int no:numbers ) {
			System.out.println(no);
		}
		
	}
}

/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 06-Mar-2022
 *  FILE NAME  		: 	 BreakStatement.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    7:14:08 pm
 */
package com.ryinsightlearninghub.ControlStatements;

public class BreakStatement {
	 public static void main(String[] args) {
	      BreakStatement obj=new BreakStatement();
	      obj.exampleOne();
	      obj.example2();
	 }
	 public void  example2() {
		 double numbers[]= {3.2, 5.0, 2.3, 0.0, -4.5};
		 double sum = 0.0;
		 
		 for(double no: numbers) {
			 while(no<0.0) {
				 break;
			 }
			 sum+=no;
			 
		 }
		 
		 
    	 
		 System.out.println(sum);
	 }
	 public void exampleOne() {
		    // for loop
	        for (int i = 1; i <= 10; ++i) {

	            // if the value of i is 5 the loop terminates  
	            if (i == 5) {
	                break;
	            }      
	            System.out.println(i);
	        }   

	 }
}

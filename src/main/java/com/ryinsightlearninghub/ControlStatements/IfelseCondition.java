/**sumitkumar
 IfelseCondition.java
 * 01-Apr-2019
 */
package com.ryinsightlearninghub.ControlStatements;

import java.util.Scanner;

/**
 * @author sumitkumar
 * if else condition
 */
public class IfelseCondition 
{
	public static void main(String[] args) 
	{
		System.out.println("Please inter two value for compare");
		
		int a, b;
		
		Scanner sc = new Scanner(System.in);
		
		a=sc.nextInt();
		b=sc.nextInt();
		
		if(a<b)
		{
			System.out.println(a +" "+"is smallest than"+" "+b);
		}
		else
		{
			System.out.println(a +" "+"is smaller than"+" "+b);
		}
		
	}

}
